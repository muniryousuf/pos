package com.application.ordermanagement.interfaces;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}
