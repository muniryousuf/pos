package com.application.ordermanagement.interfaces;

public interface DataLoadedListener {
    void onDataLoaded();
}
