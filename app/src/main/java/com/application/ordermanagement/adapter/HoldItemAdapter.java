package com.application.ordermanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HoldItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<OrderPlaceModel> categoriesEntList;
    MainActivity mainActivity;
    onClickHold onClickHold;
    public HoldItemAdapter(MainActivity mainActivity,onClickHold onClickHold) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.onClickHold = onClickHold;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hold, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final OrderPlaceModel datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.lblProduct.setText(datum.getName());
            categoryViewHolder.lblPrice.setText("£ " + datum.getTotalAmountWithFee());
            categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.clickHold(datum);

                }
            });

        }
    }

    public interface onClickHold {
     void clickHold(OrderPlaceModel orderDetail);
    }

    public void addAll(List<OrderPlaceModel> categoriesEntList) {

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblProduct)
        TextView lblProduct;


        @BindView(R.id.lblPrice)
        TextView lblPrice;
        @BindView(R.id.tvupdateprice)
        TextView tvupdateprice;


        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}