package com.application.ordermanagement.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.models.OrdersResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<OrdersResponse.Detail> items;
    private MainActivity context;
    //OrderHistoryDetailInterface orderHistoryDetailInterface;
    boolean status = true;

    public OrderHistoryProductAdapter(final MainActivity context) {
        this.context = context;
        this.status = status;
        this.items = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail, parent, false);
        OrderHistoryProductAdapter.OrderHistoryDetailViewHolder productViewHolder = new OrderHistoryProductAdapter.OrderHistoryDetailViewHolder(v);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final OrdersResponse.Detail orderDetailResponse = items.get(position);

        if (holder instanceof OrderHistoryProductAdapter.OrderHistoryDetailViewHolder) {

            ((OrderHistoryDetailViewHolder) holder).lblProduct.setText(orderDetailResponse.getProductName());
            ((OrderHistoryDetailViewHolder) holder).tvSpecialInstruction.setText(orderDetailResponse.getSpecialInstructions());
            ((OrderHistoryDetailViewHolder) holder).lblQuantity.setText("x" + orderDetailResponse.getQuantity());
            ((OrderHistoryDetailViewHolder) holder).lblPrice.setText("" + orderDetailResponse.getPrice() + " £");
            ((OrderHistoryDetailViewHolder) holder).lblTotalPrice.setText(orderDetailResponse.getPrice() * orderDetailResponse.getQuantity() + " £");

            try {
                JSONArray jsonArray = new JSONArray(orderDetailResponse.getExtras());
                String choice, group_name, price = "";
                Spanny spanny = new Spanny();
                ((OrderHistoryDetailViewHolder) holder).llEta.setVisibility(jsonArray.length()>0?View.VISIBLE:View.GONE);
                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject currentObject = jsonArray.getJSONObject(k);
                    choice = currentObject.getString("choice");
                    group_name = currentObject.getString("group_name");
                    price = currentObject.getString("price");

                    spanny.append(!group_name.equals("")?group_name:"").append(" \n").append(!choice.equals("")?choice:"").append("\n").append(!price.equals("")?"Price :"+price+" £":"").append("\n\n");

                }
                ((OrderHistoryDetailViewHolder) holder).eta.setText(spanny.toString());

            } catch (Exception e) {

            }

        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public class OrderHistoryDetailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llEta)
        protected LinearLayout llEta;
        @BindView(R.id.tvEta)
        protected TextView eta;
        @BindView(R.id.lblProduct)
        protected TextView lblProduct;
        @BindView(R.id.lblPrice)
        protected TextView lblPrice;
        @BindView(R.id.lblQuantity)
        protected TextView lblQuantity;
        @BindView(R.id.lblTotalPrice)
        protected TextView lblTotalPrice;
        //        @BindView(R.id.ivOrderItem)
//        protected ImageView ivOrderItem;
//
//        @BindView(R.id.tvProductrefrence)
//        protected TextView tvProductrefrence;
//
//        @BindView(R.id.tvDownloadinvoice)
//        protected TextView tvDownloadinvoice;
//
//        @BindView(R.id.tvDownloadTax)
//        protected TextView tvDownloadTax;
//
//
//        @BindView(R.id.flInprogress)
//        protected FrameLayout flInprogress;
//        @BindView(R.id.flShipped)
//        protected FrameLayout flShipped;
//        @BindView(R.id.flDelivered)
//        protected FrameLayout flDelivered;
//
//
        @BindView(R.id.tvSpecialInstruction)
        protected TextView tvSpecialInstruction;
//        @BindView(R.id.llSoldBy)
//        protected LinearLayout llSoldBy;
//
//        @BindView(R.id.llEta)
//        protected LinearLayout llEta;
//        @BindView(R.id.tvEta)
//        protected TextView tvEta;
//
//
//        @BindView(R.id.tvInprocess)
//        protected TextView tvInprocess;
//        @BindView(R.id.tvShipped)
//        protected TextView tvShipped;
//        @BindView(R.id.tvDelivered)
//        protected TextView tvDelivered;
//
//        @BindView(R.id.tvMultipleshipping)
//        protected RelativeLayout tvMultipleshipping;
//        @BindView(R.id.lltimeline)
//        protected LinearLayout lltimeline;


        public OrderHistoryDetailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void showToastShortTime(String title, String message) {
        if (context != null) {
            Toast.makeText(context,
                    message,
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void addAll(List<OrdersResponse.Detail> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();

    }


}


