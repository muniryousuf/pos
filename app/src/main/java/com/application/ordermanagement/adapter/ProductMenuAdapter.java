package com.application.ordermanagement.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.fragments.AddAdonFragment;
import com.application.ordermanagement.fragments.EditPriceFragment;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.daimajia.androidanimations.library.Techniques;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

public class ProductMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MenuResponse.Product> items;
    ProductMenuAdapter.ISubCategoryProductAdapter ISubCategoryProductAdapter;
    private MainActivity context;
    private static final int LOADING = 1;
    private static final int ITEM = 0;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    boolean isGridLayout;
    private int lastPosition = -1;
    private boolean pos = false;
    private long mLastClickTime = 0;
    int camp = 0;
    String title;
    ArrayList<String> suggestions;

    public ProductMenuAdapter(ProductMenuAdapter.ISubCategoryProductAdapter iProductAdapter, final MainActivity context, PaginationAdapterCallback mCallback, boolean isGridLayout, boolean pos) {
        this.items = new ArrayList<>();
        this.ISubCategoryProductAdapter = iProductAdapter;
        this.mCallback = mCallback;
        this.context = context;
        this.isGridLayout = isGridLayout;
        this.pos = pos;
        suggestions = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                if (isGridLayout) {
                    View productView = layoutInflater.inflate(R.layout.product_item, parent, false);
                    viewHolder = new ProductMenuAdapter.ProductViewHolder(productView);
                }

                break;

            case LOADING:
                View viewLoading = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new ProductMenuAdapter.LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MenuResponse.Product datum = items.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                if (holder instanceof ProductMenuAdapter.ProductViewHolder) {
                    final ProductMenuAdapter.ProductViewHolder bannerViewHolder = (ProductViewHolder) holder;

                    Spanny spanny = new Spanny();
                    bannerViewHolder.lblMenu.setText(datum.getName());
                    if (datum.getSizes().size() > 0) {
                        for (int i = 0; i < datum.getSizes().size(); i++) {
                            spanny.append(datum.getSizes().get(i).getSize() + " :£" + datum.getSizes().get(i).getPrice() + "\n");
                        }
                        bannerViewHolder.tvMainPrice.setText(spanny);
                    } else {
                        bannerViewHolder.tvMainPrice.setText("£ " + datum.getPrice());
                    }
                    bannerViewHolder.swAvailibity.setChecked(datum.getStatus() == 1 ? true : false);


                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 5, 5, 5);


                    if (datum.getmGroup() != null && datum.getmGroup().size() > 0) {
                        bannerViewHolder.chips_box_layout.removeAllViews();
                        for (int i = 0; i < datum.getmGroup().size(); i++) {
                            TextView t = new TextView(context);
                            t.setText(datum.getmGroup().get(i).getName());
                            t.setLayoutParams(params);
                            t.setPadding(5, 5, 5, 5);
                            t.setTextColor(Color.WHITE);
                            t.setBackgroundColor(context.getResources().getColor(R.color.blue_login_color));
                            bannerViewHolder.chips_box_layout.addView(t);
                        }
                        bannerViewHolder.chips_box_layout.setVisibility(View.VISIBLE);
                    } else {
                        bannerViewHolder.chips_box_layout.setVisibility(View.GONE);
                    }


//                    bannerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//
//                            ISubCategoryProductAdapter.clickmenu(datum.getProducts());
//                        }
//                    });
                    bannerViewHolder.swAvailibity.setOnCheckedChangeListener((buttonView, isChecked) -> {

                        if (datum.getStatus() == 1) {
                            ISubCategoryProductAdapter.updateAvailibity(Integer.valueOf(String.valueOf(datum.getId())), datum.getPrice(), 0);
                            items.get(position).setStatus(Long.valueOf(0));
                            // notifyDataSetChanged();
                            //  bannerViewHolder.swAvailibity.setChecked(false);
                            // notifyItemChanged(position);
                        } else {
                            ISubCategoryProductAdapter.updateAvailibity(Integer.valueOf(String.valueOf(datum.getId())), datum.getPrice(), 1);
                            items.get(position).setStatus(Long.valueOf(1));
                            //  notifyDataSetChanged();
                            //   bannerViewHolder.swAvailibity.setChecked(true);
                            // notifyItemChanged(position);


                        }


                    });


                        bannerViewHolder.tvupdateprice.setOnClickListener(v -> {
                        if (datum.getSizes() != null && datum.getSizes().size() > 0) {
                            EditPriceFragment editPriceFragment = new EditPriceFragment();
                            editPriceFragment.setdata(Integer.parseInt(String.valueOf(datum.getId())), datum.getSizes());
                            context.replaceFragment(editPriceFragment, true, true);


                        } else {

                            showDialog(context, Integer.parseInt(String.valueOf(datum.getId())), bannerViewHolder, datum.getPrice());
                        }

                    });

                    bannerViewHolder.tvAddAdone.setOnClickListener(v -> {

                        AddAdonFragment addAdonFragment = new AddAdonFragment();
                        addAdonFragment.setIsShow(true);
                        addAdonFragment.id(String.valueOf(datum.getId()), datum.getmGroup());
                        context.replaceFragment(addAdonFragment, true, true);

                    });
                    bannerViewHolder.tvAddtoCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showAdonDialog(context);

                        }
                    });
                    bannerViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ISubCategoryProductAdapter.deleteSubMenu(datum.getId());

                        }
                    });bannerViewHolder.edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ISubCategoryProductAdapter.edit(datum);

                        }
                    });
                    bannerViewHolder.editAdon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ISubCategoryProductAdapter.editAdon(datum);

                        }
                    });

                    UIHelper.animation(Techniques.ZoomIn, 200, 0, holder.itemView);
                    if (pos) {

                        bannerViewHolder.tvupdateprice.setVisibility(View.GONE);
                        bannerViewHolder.llAvailable.setVisibility(View.GONE);
                        bannerViewHolder.tvAddAdone.setVisibility(View.GONE);
                        bannerViewHolder.tvAddtoCart.setVisibility(View.VISIBLE);
                    } else {
                        bannerViewHolder.tvupdateprice.setVisibility(View.VISIBLE);
                        bannerViewHolder.llAvailable.setVisibility(View.VISIBLE);
                        bannerViewHolder.tvAddAdone.setVisibility(View.VISIBLE);
                        bannerViewHolder.tvAddtoCart.setVisibility(View.GONE);
                    }
                    if (datum.getSizes() != null && datum.getSizes().size() > 0) {
                        bannerViewHolder.tvupdateprice.setVisibility(View.GONE);
                    }else{
                        bannerViewHolder.tvupdateprice.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case LOADING:
                ProductMenuAdapter.LoadingVH loadingVH = (ProductMenuAdapter.LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);


                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }

    public void showDialog(Activity activity, int id, ProductMenuAdapter.ProductViewHolder productViewHolder, String price) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.updatepricedialog);

        EditText text = (EditText) dialog.findViewById(R.id.etPrice);
        text.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        text.setText(price);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.update);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ISubCategoryProductAdapter.updatePrice(id, text.getText().toString());
                productViewHolder.tvMainPrice.setText("£  " + text.getText().toString());
                UIHelper.animation(Techniques.Pulse, 500, 1, productViewHolder.itemView);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showAdonDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_adon_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        CheckoutAddonAdapter checkoutAddonAdapter = new CheckoutAddonAdapter(context);
        CheckoutAddonAdapter checkoutAddonAdaptertwo = new CheckoutAddonAdapter(context);

        RecyclerView rvAdones = (RecyclerView) dialog.findViewById(R.id.rvAdones);
        RecyclerView rvAdonestwo = (RecyclerView) dialog.findViewById(R.id.rvAdonesTwo);

        rvAdones.setLayoutManager(new StaggeredGridLayoutManager(3, VERTICAL));
        rvAdonestwo.setLayoutManager(new StaggeredGridLayoutManager(3, VERTICAL));


        rvAdones.setAdapter(checkoutAddonAdapter);
        rvAdonestwo.setAdapter(checkoutAddonAdaptertwo);

        checkoutAddonAdapter.notifyDataSetChanged();
        checkoutAddonAdaptertwo.notifyDataSetChanged();


        TextView dialogButton = (TextView) dialog.findViewById(R.id.addItem);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ISubCategoryProductAdapter.addtocart();
                UIHelper.showAlertDialog(context, "Success", "Product Added to Cart");

//                ISubCategoryProductAdapter.updatePrice(id, text.getText().toString());
//                productViewHolder.tvMainPrice.setText("£  " + text.getText().toString());
//                UIHelper.animation(Techniques.Pulse, 500, 1, productViewHolder.itemView);
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lblMenu)
        protected TextView lblMenu;
        @BindView(R.id.tvMainPrice)
        protected TextView tvMainPrice;
        @BindView(R.id.swAvailibity)
        Switch swAvailibity;

        @BindView(R.id.tvupdateprice)
        TextView tvupdateprice;
        @BindView(R.id.tvAddAdone)
        TextView tvAddAdone;
        @BindView(R.id.chips_box_layout)
        FlowLayout chips_box_layout;
        @BindView(R.id.llAvailable)
        LinearLayout llAvailable;
        @BindView(R.id.tvAddtoCart)
        ImageView tvAddtoCart;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.edit)
        ImageView edit;
        @BindView(R.id.editAdon)
        TextView editAdon;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }


    public interface ISubCategoryProductAdapter {
        void updatePrice(int id, String price);
        void deleteSubMenu(Long id);
        void edit(MenuResponse.Product datum);
        void editAdon(MenuResponse.Product datum);

        void clickmenu(MenuResponse.Product datum);

        void updateAvailibity(int id, String price, int status);

        void addtocart();
    }

    public void updateFavouriteData(int position, int isWished) {
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == items.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(MenuResponse.Product r) {
        items.add(r);
        notifyItemInserted(items.size() - 1);
    }


    public void remove(MenuResponse r) {
        int position = items.indexOf(r);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addAll(List<MenuResponse.Product> productDataResponses) {
        items.clear();
        for (MenuResponse.Product productDataResponse : productDataResponses) {
            add(productDataResponse);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;

    }


    public MenuResponse.Product getItem(int position) {
        return items.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(items.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


}