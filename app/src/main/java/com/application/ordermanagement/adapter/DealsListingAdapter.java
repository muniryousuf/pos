package com.application.ordermanagement.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.DealsResponse;
import com.application.ordermanagement.models.ProductDataResponse;
import com.daimajia.androidanimations.library.Techniques;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DealsListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DealsResponse.Datum> items;
    private DealsListingAdapter.ISubCategoryProductAdapter ISubCategoryProductAdapter;
    private MainActivity context;
    private static final int LOADING = 1;
    private static final int ITEM = 0;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    boolean isGridLayout;
    private int lastPosition = -1;
    private long mLastClickTime = 0;
    int camp = 0;
    String title;

    public DealsListingAdapter(DealsListingAdapter.ISubCategoryProductAdapter iProductAdapter, final MainActivity context, PaginationAdapterCallback mCallback, boolean isGridLayout) {
        this.items = new ArrayList<>();
        this.ISubCategoryProductAdapter = iProductAdapter;
        this.mCallback = mCallback;
        this.context = context;
        this.isGridLayout = isGridLayout;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                if (isGridLayout) {
                    View productView = layoutInflater.inflate(R.layout.deals_item_category, parent, false);
                    viewHolder = new ProductViewHolder(productView);
                }
//                else {
//                    View productView = layoutInflater.inflate(R.layout.product_item_list, parent, false);
//                    viewHolder = new ProductViewHolder(productView);
//                }
                break;

            case LOADING:
                View viewLoading = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final DealsResponse.Datum productsResponse = items.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                if (holder instanceof DealsListingAdapter.ProductViewHolder) {
                    final ProductViewHolder bannerViewHolder = (DealsListingAdapter.ProductViewHolder) holder;
                    /// setAnimation(holder.itemView, position);
                    UIHelper.setImagewithGlide(context, bannerViewHolder.ivProductDescriptionItem, productsResponse.getImageUrl());


                    bannerViewHolder.lblDescription.setText(productsResponse.getDescription());
                    bannerViewHolder.lblProduct.setText(productsResponse.getName());
                    bannerViewHolder.lblPrice.setText("£ " + productsResponse.getPrice());
                   // bannerViewHolder.swAvailibity.setChecked(productsResponse.getStatus()==1?true:false);

                    bannerViewHolder.itemView.setOnClickListener(v -> {
//                            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
//                                return;
//                            }
//                            mLastClickTime = SystemClock.elapsedRealtime();
//                            ISubCategoryProductAdapter.onProductItemClick(productsResponse);
                    });
                    bannerViewHolder.tvupdateprice.setOnClickListener(v -> {


                        showDialog(context, productsResponse.getId(), bannerViewHolder);

                    });



                    UIHelper.animation(Techniques.ZoomIn, 200, 0, holder.itemView);

                }
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);


                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivProductDescriptionItem)
        protected ImageView ivProductDescriptionItem;

        @BindView(R.id.lblProduct)
        protected TextView lblProduct;
        @BindView(R.id.lblPrice)
        protected TextView lblPrice;


        @BindView(R.id.lblDiscount)
        protected TextView lblDiscount;

        @BindView(R.id.discountBox)
        RelativeLayout discountBox;


        @BindView(R.id.lblDescription)
        TextView lblDescription;
        @BindView(R.id.tvupdateprice)
        TextView tvupdateprice;
        @BindView(R.id.swAvailibity)
        Switch swAvailibity;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // lblDiscountedPrice.setPaintFlags(lblDiscountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

    }


    public void showDialog(Activity activity, Long id, ProductViewHolder productViewHolder) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.updatepricedialog);

        EditText text = (EditText) dialog.findViewById(R.id.etPrice);
        text.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        //text.setText(msg);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.update);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ISubCategoryProductAdapter.updatePrice(id, text.getText().toString());
                productViewHolder.lblPrice.setText("£  "+text.getText().toString());
                UIHelper.animation(Techniques.Pulse, 500, 1, productViewHolder.itemView);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }


    public interface ISubCategoryProductAdapter {
        void updatePrice(Long id, String price);
        void updateAvailibity(int id, String price,int status);
    }

    public void updateFavouriteData(int position, int isWished) {
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == items.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(DealsResponse.Datum r) {
        items.add(r);
        notifyItemInserted(items.size() - 1);
    }


    public void remove(ProductDataResponse r) {
        int position = items.indexOf(r);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addAll(List<DealsResponse.Datum> productDataResponses) {
        items.clear();
        for (DealsResponse.Datum productDataResponse : productDataResponses) {
            add(productDataResponse);
        }
    }

//    public void clear() {
//        isLoadingAdded = false;
//        while (getItemCount() > 0) {
//            remove(getItem(0));
//        }
//
//    }

    public boolean isEmpty() {
        return getItemCount() == 0;

    }


    public DealsResponse.Datum getItem(int position) {
        return items.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(items.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

//    private void setAnimation(View viewToAnimate, int position) {
//        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition) {
//            Animation animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
//    }


}