package com.application.ordermanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.adapter.POS.SubCheckoutAdonAdapter;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckoutAddonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<MenuResponse.Group> categoriesEntList;
    MainActivity mainActivity;
    int dataPosition;

    public CheckoutAddonAdapter(MainActivity mainActivity) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adon_selection, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    SubCheckoutAdonAdapter subCheckoutAdonAdapter;

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MenuResponse.Group datum = categoriesEntList.get(position);
        dataPosition = position;
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;

            categoryViewHolder.title.setText(datum.getName());
            subCheckoutAdonAdapter = new SubCheckoutAdonAdapter(mainActivity);
            categoryViewHolder.recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL));
            categoryViewHolder.recyclerView.setAdapter(subCheckoutAdonAdapter);
            subCheckoutAdonAdapter.addAll(categoriesEntList.get(position).getChoices());

        }
    }

    public ArrayList<MenuResponse.Group> getAdons() {

        return categoriesEntList;
    }
//    public void setubCheckoutAdonAdapterPosition(int position){
//        subCheckoutAdonAdapter.setposition(position);
//    }

    public void addAll(List<MenuResponse.Group> categoriesEntList) {

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }


    public int getItemCount() {
        return categoriesEntList.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rvAdonesTwo)
        RecyclerView recyclerView;
        @BindView(R.id.title)
        TextView title;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}