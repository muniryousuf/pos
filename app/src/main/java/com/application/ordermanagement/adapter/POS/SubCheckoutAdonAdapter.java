package com.application.ordermanagement.adapter.POS;

import android.annotation.SuppressLint;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.ResponseForSeperateModels.SingleProductResponse;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;
import com.application.ordermanagement.models.CategoryResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCheckoutAdonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<AddOnsResponse.Choice> categoriesEntList;
    MainActivity mainActivity;
    int lastCheckedPosition =-1;
    public SubCheckoutAdonAdapter(MainActivity mainActivity) {
        this.categoriesEntList =new ArrayList<>();
        this.mainActivity =mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_adon_selection, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
         final AddOnsResponse.Choice datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            if(categoriesEntList.get(position).isIs_checked()){
                lastCheckedPosition =position;
            }
            categoryViewHolder.radiobtn.setChecked(position == lastCheckedPosition);
//            if(position==lastCheckedPosition){
//                categoriesEntList.get(position).setIs_checked(true);
//
//            } else{
//                categoriesEntList.get(position).setIs_checked(false);
//            }
            logicforselectandunselect(categoryViewHolder,categoriesEntList.get(position).isIs_checked());
            categoryViewHolder.itemView.setOnClickListener(v -> {
                if(categoriesEntList.get(position).isIs_checked()){
                   // categoriesEntList.get(position).setIs_checked(false);
                    categoriesEntList.get(position).setIs_checked(false);
                    notifyItemChanged(position);


                }else{
                    if(lastCheckedPosition==-1){

                        categoriesEntList.get(position).setIs_checked(true);
                        lastCheckedPosition =position;
                        notifyItemChanged(lastCheckedPosition);
                    }else{
                        categoriesEntList.get(lastCheckedPosition).setIs_checked(false);
                        categoriesEntList.get(position).setIs_checked(true);
                        notifyItemChanged(position);
                        notifyItemChanged(lastCheckedPosition);
                        lastCheckedPosition =position;

                    }


//                    if(position !=lastCheckedPosition){
//                        categoriesEntList.get(lastCheckedPosition).setIs_checked(false);
//                        categoriesEntList.get(position).setIs_checked(true);
//                        lastCheckedPosition =position;
//                        notifyDataSetChanged();
//
//                    }
                  //  categoriesEntList.get(position).setIs_checked(true);
                   // notifyDataSetChanged();

                }
//                if (position == lastCheckedPosition) {
//                    ((CategoryViewHolder) holder).radiobtn.setChecked(false);
//                    lastCheckedPosition = -1;
//                } else {
//                    lastCheckedPosition = position;
//                    notifyDataSetChanged();
//                }
            });
            categoryViewHolder.tvSubAdoNCheckout.setText(datum.getName());
            categoryViewHolder.price.setText("£ "+datum.getPrice());
            categoryViewHolder.radiobtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                }
            });


        }
    }
    private void logicforselectandunselect(SubCheckoutAdonAdapter.CategoryViewHolder categoryViewHolder, boolean is_checked) {

        if(is_checked){
            categoryViewHolder.llbox.setBackground(mainActivity.getResources().getDrawable(R.drawable.drawable_selected));
            categoryViewHolder.price.setTextColor(mainActivity.getResources().getColor(R.color.white));
            categoryViewHolder.tvSubAdoNCheckout.setTextColor(mainActivity.getResources().getColor(R.color.white));
        }else{
            categoryViewHolder.llbox.setBackground(mainActivity.getResources().getDrawable(R.drawable.drawable_unselected));
            categoryViewHolder.price.setTextColor(mainActivity.getResources().getColor(R.color.mediumBlue));
            categoryViewHolder.tvSubAdoNCheckout.setTextColor(mainActivity.getResources().getColor(R.color.mediumBlue));
        }
    }

    public void addAll(List<AddOnsResponse.Choice> categoriesEntList){

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }

    public int getItemCount() {
       return  categoriesEntList.size();

    }

    public void setposition(int position) {
        this.lastCheckedPosition =position;
        notifyDataSetChanged();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvSubAdoNCheckout)
        TextView tvSubAdoNCheckout;
        @BindView(R.id.radiobtn)
        RadioButton radiobtn;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.llbox)
        LinearLayout llbox;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}