package com.application.ordermanagement.adapter.POS;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.adapter.CheckoutAddonAdapter;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.daimajia.androidanimations.library.Techniques;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

public class PosProductMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MenuResponse.Product> items;
    PosProductMenuAdapter.ISubCategoryProductAdapter ISubCategoryProductAdapter;
    private MainActivity context;
    private static final int LOADING = 1;
    private static final int ITEM = 0;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    boolean isGridLayout;
    private int lastPosition = -1;
    private boolean pos = false;
    private long mLastClickTime = 0;
    int camp = 0;
    String title;
    ArrayList<String> suggestions;
    int[] array;
    public PosProductMenuAdapter(PosProductMenuAdapter.ISubCategoryProductAdapter iProductAdapter, final MainActivity context, PaginationAdapterCallback mCallback, boolean isGridLayout, boolean pos) {
        this.items = new ArrayList<>();
        this.ISubCategoryProductAdapter = iProductAdapter;
        this.mCallback = mCallback;
        this.context = context;
        this.isGridLayout = isGridLayout;
        this.pos = pos;
        suggestions = new ArrayList<>();
        array = new int[]{R.drawable.chicken_perfection, R.drawable.chickn_chines, R.drawable.cajan_chicken, R.drawable.cajan_supreme, R.drawable.california, R.drawable.bbq_chicken};

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                if (isGridLayout) {
                    View productView = layoutInflater.inflate(R.layout.item_products, parent, false);
                    viewHolder = new PosProductMenuAdapter.ProductViewHolder(productView);
                }

                break;

            case LOADING:
                View viewLoading = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new PosProductMenuAdapter.LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MenuResponse.Product datum = items.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                if (holder instanceof PosProductMenuAdapter.ProductViewHolder) {
                    final PosProductMenuAdapter.ProductViewHolder bannerViewHolder = (ProductViewHolder) holder;
                    int randomStr = array[new Random().nextInt(array.length)];
                    bannerViewHolder.foodicon.setImageDrawable(context.getResources().getDrawable(randomStr));
                    Spanny spanny = new Spanny();
                    bannerViewHolder.lblMenu.setText(datum.getName());
                    if (datum.getSizes().size() > 0) {
                        bannerViewHolder.tvMainPrice.setVisibility(View.VISIBLE);
                        bannerViewHolder.tvMainPrice.setText("£ " + datum.getSizes().get(0).getPrice());

//                        for (int i = 0; i < datum.getSizes().size(); i++) {
//                            spanny.append(datum.getSizes().get(i).getSize() + " :£" + datum.getSizes().get(i).getPrice() + " ");
//                        }
//                        bannerViewHolder.tvMainPrice.setText(spanny);
                    } else {
                        bannerViewHolder.tvMainPrice.setVisibility(View.VISIBLE);
                        bannerViewHolder.tvMainPrice.setText("£ " + datum.getPrice());
                    }


//                    bannerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//
//                            ISubCategoryProductAdapter.clickmenu(datum.getProducts());
//                        }
//                    });


                    bannerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ISubCategoryProductAdapter.clickmenu(datum);


                        }
                    });

                    UIHelper.animation(Techniques.ZoomIn, 200, 0, holder.itemView);


                }
                break;
            case LOADING:
                PosProductMenuAdapter.LoadingVH loadingVH = (PosProductMenuAdapter.LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);


                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }

    public void showDialog(Activity activity, int id, PosProductMenuAdapter.ProductViewHolder productViewHolder, String price) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.updatepricedialog);

        EditText text = (EditText) dialog.findViewById(R.id.etPrice);
        text.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        text.setText(price);

        TextView dialogButton = (TextView) dialog.findViewById(R.id.update);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ISubCategoryProductAdapter.updatePrice(id, text.getText().toString());
                //    productViewHolder.tvMainPrice.setText("£  " + text.getText().toString());
                UIHelper.animation(Techniques.Pulse, 500, 1, productViewHolder.itemView);
                dialog.dismiss();
            }
        });

        dialog.show();

    }



    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lblMenu)
        protected TextView lblMenu;
        @BindView(R.id.tvMainPrice)
        protected TextView tvMainPrice;
        @BindView(R.id.foodicon)
        protected ImageView foodicon;


        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }


    public interface ISubCategoryProductAdapter {
        void updatePrice(int id, String price);

        void clickmenu(MenuResponse.Product datum);

        void updateAvailibity(int id, String price, int status);

        void addtocart();
    }

    public void updateFavouriteData(int position, int isWished) {
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {

        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == items.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(MenuResponse.Product r) {
        items.add(r);
        notifyItemInserted(items.size() - 1);
    }


    public void remove(MenuResponse r) {
        int position = items.indexOf(r);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addAll(List<MenuResponse.Product> productDataResponses) {
        items.clear();
        for (MenuResponse.Product productDataResponse : productDataResponses) {
            add(productDataResponse);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;

    }


    public MenuResponse.Product getItem(int position) {
        return items.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(items.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


}