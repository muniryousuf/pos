package com.application.ordermanagement.adapter;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.CategoryResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckoutCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<CategoryResponse.Datum> categoriesEntList;
    MainActivity mainActivity;
    public CheckoutCartAdapter(MainActivity mainActivity) {
        // this.categoriesEntList =new ArrayList<>();
        this.mainActivity =mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_layout, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // final CategoryResponse.Datum datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;

        }
    }

    public void addAll(List<CategoryResponse.Datum> categoriesEntList){

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }
    @Override
    // public int getItemCount() {
//        return categoriesEntList.size();
//    }
    public int getItemCount() {
        return 2;
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.lblnumofproducts)
//        TextView lblnumofproducts;
//        @BindView(R.id.ivCategoryImage)
//        ImageView ivCategoryImage;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}