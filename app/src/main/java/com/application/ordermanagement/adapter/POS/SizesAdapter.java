package com.application.ordermanagement.adapter.POS;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.model.ResponseForSeperateModels.SingleProductResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SizesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<MenuResponse.Size> categoriesEntList;
    MainActivity mainActivity;

    public SizesAdapter(MainActivity mainActivity) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_size, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    SubCheckoutAdonAdapter subCheckoutAdonAdapter;
   public int lastCheckedPosition =  0;

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MenuResponse.Size datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.radiobtn.setChecked(position == lastCheckedPosition);
            if (position == lastCheckedPosition) {
                categoriesEntList.get(position).setIs_checked(true);

            } else {
                categoriesEntList.get(position).setIs_checked(false);
            }

            logicforselectandunselect(categoryViewHolder,categoriesEntList.get(position).isIs_checked());


            categoryViewHolder.itemView.setOnClickListener(v -> {
                if (position == lastCheckedPosition) {
                    categoryViewHolder.radiobtn.setChecked(false);
                    lastCheckedPosition = -1;
                } else {
                    lastCheckedPosition = position;
                    notifyDataSetChanged();
                }
            });
            categoryViewHolder.price.setText("£ " + datum.getPrice());
            categoryViewHolder.tvSubAdoNCheckout.setText(datum.getSize());

        }
    }

    private void logicforselectandunselect(CategoryViewHolder categoryViewHolder, boolean is_checked) {

        if(is_checked){
            categoryViewHolder.llbox.setBackground(mainActivity.getResources().getDrawable(R.drawable.drawable_selected));
            categoryViewHolder.price.setTextColor(mainActivity.getResources().getColor(R.color.white));
            categoryViewHolder.tvSubAdoNCheckout.setTextColor(mainActivity.getResources().getColor(R.color.white));
        }else{
            categoryViewHolder.llbox.setBackground(mainActivity.getResources().getDrawable(R.drawable.drawable_unselected));
            categoryViewHolder.price.setTextColor(mainActivity.getResources().getColor(R.color.blue));
            categoryViewHolder.tvSubAdoNCheckout.setTextColor(mainActivity.getResources().getColor(R.color.blue));
        }
    }

    public void addAll(List<MenuResponse.Size> categoriesEntList) {

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }

    public int getItemCount() {
        return categoriesEntList.size();
    }

    public ArrayList<MenuResponse.Size> getSizes() {
        return categoriesEntList;
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvSubAdoNCheckout)
        TextView tvSubAdoNCheckout;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.radiobtn)
        RadioButton radiobtn;
        @BindView(R.id.llbox)
        LinearLayout llbox;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}