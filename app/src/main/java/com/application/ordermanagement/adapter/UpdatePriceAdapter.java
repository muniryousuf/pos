package com.application.ordermanagement.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.daimajia.androidanimations.library.Techniques;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdatePriceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MenuResponse.Size> items;
    UpdatePriceAdapter.ISubCategoryProductAdapter ISubCategoryProductAdapter;
    private MainActivity context;
    private static final int LOADING = 1;
    private static final int ITEM = 0;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    boolean isGridLayout;
    private int lastPosition = -1;
    private long mLastClickTime = 0;
    int camp = 0;
    String title;

    public UpdatePriceAdapter(UpdatePriceAdapter.ISubCategoryProductAdapter iProductAdapter, final MainActivity context, PaginationAdapterCallback mCallback, boolean isGridLayout) {
        this.items = new ArrayList<>();
        this.ISubCategoryProductAdapter = iProductAdapter;
        this.mCallback = mCallback;
        this.context = context;
        this.isGridLayout = isGridLayout;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                if (isGridLayout) {
                    View productView = layoutInflater.inflate(R.layout.item_sizes, parent, false);
                    viewHolder = new UpdatePriceAdapter.ProductViewHolder(productView);
                }

                break;

            case LOADING:
                View viewLoading = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new UpdatePriceAdapter.LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MenuResponse.Size datum = items.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                if (holder instanceof UpdatePriceAdapter.ProductViewHolder) {
                    final UpdatePriceAdapter.ProductViewHolder bannerViewHolder = (UpdatePriceAdapter.ProductViewHolder) holder;
                    /// setAnimation(holder.itemView, position);
                    bannerViewHolder.etPrice.setText(datum.getPrice());
                    bannerViewHolder.tvsizename.setText(datum.getSize());

                    bannerViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ISubCategoryProductAdapter.delete(datum,position);
                        }
                    });
                    bannerViewHolder.etPrice.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (bannerViewHolder.etPrice.getText().length() < 1) {
                                Toast.makeText(context, "Price cannot be zero", Toast.LENGTH_SHORT).show();
                            } else {
                                ISubCategoryProductAdapter.onPriceUpdate(bannerViewHolder.etPrice.getText().toString(),position);
                                //items.get(position).setPrice(bannerViewHolder.etPrice.getText().toString());
                            }
                            // TODO Auto-generated method stub
                        }
                    });

                    UIHelper.animation(Techniques.ZoomIn, 200, 0, holder.itemView);

                }
                break;
            case LOADING:
                UpdatePriceAdapter.LoadingVH loadingVH = (UpdatePriceAdapter.LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);


                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }

    public void getSizeArray() {

        ISubCategoryProductAdapter.size(items);

    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvsizename)
        protected TextView tvsizename;
        @BindView(R.id.etPrice)
        protected EditText etPrice;
        @BindView(R.id.ivDelete)
        protected ImageView ivDelete;


        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }


    public interface ISubCategoryProductAdapter {
        void size(ArrayList<MenuResponse.Size> datum);
        void delete(MenuResponse.Size datum,int position);
        void onPriceUpdate(String price,int position);
    }


    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == items.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(MenuResponse.Size r) {
        items.add(r);
        notifyItemInserted(items.size() - 1);
    }


    public void remove(MenuResponse r) {
        int position = items.indexOf(r);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeitem(int remove){
        items.remove(remove);
        notifyDataSetChanged();
    }
    public void addAll(List<MenuResponse.Size> productDataResponses) {
        items.clear();
        for (MenuResponse.Size productDataResponse : productDataResponses) {
            add(productDataResponse);
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;

    }


    public MenuResponse.Size getItem(int position) {
        return items.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(items.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


}