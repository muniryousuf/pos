package com.application.ordermanagement.adapter;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.model.TableResponseModel.TablesResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<TablesResponse.Datum> categoriesEntList;
    MainActivity mainActivity;
    onClickHold onClickHold;

    public TableAdapter(MainActivity mainActivity, onClickHold onClickHold) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.onClickHold = onClickHold;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_layout, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final TablesResponse.Datum datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.tvTabel.setText(datum.getName());
            categoryViewHolder.tvStatus.setText(datum.getIs_reserve() == 1 ? "Reserved" : "Vacant");
            categoryViewHolder.tvStatus.setTextColor(datum.getIs_reserve() == 1 ? mainActivity.getResources().getColor(R.color.red_color) : mainActivity.getResources().getColor(R.color.active_color));
            if (datum.getIs_reserve() == 1) {
              categoryViewHolder.swtich.setChecked(true);
                categoryViewHolder.ivStatus.getBackground().setColorFilter(mainActivity.getResources().getColor(R.color.red_color),
                        PorterDuff.Mode.SRC_ATOP);

            } else {
                categoryViewHolder.swtich.setChecked(false);
                categoryViewHolder.ivStatus.getBackground().setColorFilter(mainActivity.getResources().getColor(R.color.active_color),
                        PorterDuff.Mode.SRC_ATOP);
            }
            // categoryViewHolder.ivStatus.setText("£ " + datum.getTotalAmountWithFee());
            categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onCickTable(position);

                }
            });
            categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onCickTable(position);

                }
            });

            categoryViewHolder.swtich.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onEdit(datum);

                }
            });

        }
    }

    public void addAll(ArrayList<TablesResponse.Datum> data) {
        categoriesEntList.clear();
        categoriesEntList = data;
        notifyDataSetChanged();
    }

    public interface onClickHold {
        void onCickTable(int position);

        void onEdit(TablesResponse.Datum datum);
    }

//    public void addAll(List<OrderPlaceModel> categoriesEntList) {
//
//        this.categoriesEntList.clear();
//        this.categoriesEntList.addAll(categoriesEntList);
//        notifyDataSetChanged();
//    }

    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTabel)
        TextView tvTabel;


        @BindView(R.id.tvStatus)
        TextView tvStatus;

        @BindView(R.id.ivStatus)
        ImageView ivStatus;
        @BindView(R.id.swtich)
        Switch swtich;


        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}