package com.application.ordermanagement.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.model.Printers.PrintersResponse;
import com.application.ordermanagement.model.TableResponseModel.TablesResponse;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrintersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<PrinterResponseList.Datum> categoriesEntList;
    MainActivity mainActivity;
    onClickPrinter onClickHold;

    public PrintersAdapter(MainActivity mainActivity, onClickPrinter onClickHold) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.onClickHold = onClickHold;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_printers, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final PrinterResponseList.Datum datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.tvprinterIp.setText(datum.getIp());
            categoryViewHolder.tvPrinterName.setText(datum.getName());
            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 5, 5, 5);
            if (datum.getCategories() != null && datum.getCategories().size() > 0) {
                categoryViewHolder.chips_box_layout.removeAllViews();
                for (int i = 0; i < datum.getCategories().size(); i++) {
                    TextView t = new TextView(mainActivity);
                    t.setText(datum.getCategories().get(i).getCategory().getName());
                    t.setLayoutParams(params);
                    t.setPadding(5, 5, 5, 5);
                    t.setTextColor(Color.WHITE);
                    t.setBackgroundColor(mainActivity.getResources().getColor(R.color.active_color));
                    categoryViewHolder.chips_box_layout.addView(t);
                }
                categoryViewHolder.chips_box_layout.setVisibility(View.VISIBLE);
            } else {
                categoryViewHolder.chips_box_layout.setVisibility(View.GONE);
            }
            categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onClickPrinter(datum);

                }
            });

            categoryViewHolder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onEditPrinters(datum);

                }
            });    categoryViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickHold.onClickPrinterDelete(datum);

                }
            });

        }
    }

    public void addAll(ArrayList<PrinterResponseList.Datum> data) {
        categoriesEntList.clear();
        categoriesEntList = data;
        notifyDataSetChanged();
    }

    public interface onClickPrinter {
        void onClickPrinter(PrinterResponseList.Datum datum);
        void onClickPrinterDelete(PrinterResponseList.Datum datum);

        void onEditPrinters(PrinterResponseList.Datum datum);
    }


    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvPrinterName)
        TextView tvPrinterName;


        @BindView(R.id.tvprinterIp)
        TextView tvprinterIp;
        @BindView(R.id.chips_box_layout)
        FlowLayout chips_box_layout;

        @BindView(R.id.ivEdit)
        ImageView ivEdit;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;



        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}