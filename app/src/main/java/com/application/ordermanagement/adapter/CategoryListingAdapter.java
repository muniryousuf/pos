package com.application.ordermanagement.adapter;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.CategoryResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<CategoryResponse.Datum> categoriesEntList;
    MainActivity mainActivity;
    public CategoryListingAdapter(MainActivity mainActivity) {
        this.categoriesEntList =new ArrayList<>();
        this.mainActivity =mainActivity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder,  int position) {
          final CategoryResponse.Datum datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;


             categoryViewHolder.lblCategory.setText(datum.getName());
             categoryViewHolder.lblDescription.setText(datum.getDescription());
             categoryViewHolder.lblnumofproducts.setText(""+datum.getProducts().size());
             if(datum.isIs_selected()){

                 categoryViewHolder.ivCategoryImage.setVisibility(View.VISIBLE);
             }else{

                 categoryViewHolder.ivCategoryImage.setVisibility(View.INVISIBLE);
             }


             categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     if(datum.isIs_selected()){
                         datum.setIs_selected(false);
                     }else{
                         datum.setIs_selected(true);

                     }
                     notifyItemChanged(position);
                 }
             });

        }
    }

    public  ArrayList<Long> getSelectedIds(){
        ArrayList<Long> items =new ArrayList<>();
        for(int i =0; i<categoriesEntList.size();i++){
            if(categoriesEntList.get(i).isIs_selected()){
                items.add(categoriesEntList.get(i).getId());
            }

        }
        return items;

    }
    public void addAll(List<CategoryResponse.Datum> categoriesEntList){

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblCategory)
        TextView lblCategory;
        @BindView(R.id.lblDescription)
        TextView lblDescription;
        @BindView(R.id.lblnumofproducts)
        TextView lblnumofproducts;
        @BindView(R.id.ivCategoryImage)
        ImageView ivCategoryImage;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}