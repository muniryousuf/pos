package com.application.ordermanagement.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.application.ordermanagement.models.ProductDataResponse;
import com.daimajia.androidanimations.library.Techniques;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChoiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MenuResponse.Product> items;
    private ChoiceAdapter.ISubCategoryProductAdapter ISubCategoryProductAdapter;
    private MainActivity context;
    private static final int LOADING = 1;
    private static final int ITEM = 0;
    private boolean isLoadingAdded = false;
    private String errorMsg;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    boolean isGridLayout;
    private int lastPosition = -1;
    private long mLastClickTime = 0;
    int camp = 0;
    String title;

    public ChoiceAdapter(ChoiceAdapter.ISubCategoryProductAdapter iProductAdapter, final MainActivity context, boolean isGridLayout) {
        this.items = new ArrayList<>();
        this.ISubCategoryProductAdapter = iProductAdapter;
        this.context = context;
        this.isGridLayout = isGridLayout;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM:
                if (isGridLayout) {
                    View productView = layoutInflater.inflate(R.layout.item_choices, parent, false);
                    viewHolder = new ProductViewHolder(productView);
                }
//                else {
//                    View productView = layoutInflater.inflate(R.layout.product_item_list, parent, false);
//                    viewHolder = new ProductViewHolder(productView);
//                }
                break;

            case LOADING:
                View viewLoading = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;

        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MenuResponse.Product productsResponse = items.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                if (holder instanceof ChoiceAdapter.ProductViewHolder) {
                    final ProductViewHolder bannerViewHolder = (ChoiceAdapter.ProductViewHolder) holder;
                    /// setAnimation(holder.itemView, position);


                    bannerViewHolder.tvaddone.setText(productsResponse.getName());
                    bannerViewHolder.price.setText("£ "+productsResponse.getPrice()+"/=");


                    bannerViewHolder.itemView.setOnClickListener(v -> {
                        ISubCategoryProductAdapter.onClickAddMenu(items.get(position));
                    });
                    bannerViewHolder.ivDelete.setOnClickListener(v -> {
                        items.remove(position);
                        notifyDataSetChanged();
                    });


                    UIHelper.animation(Techniques.ZoomIn, 200, 0, holder.itemView);

                }
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);


                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }


    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvaddone)
        protected TextView tvaddone;
        @BindView(R.id.price)
        protected TextView price;
        @BindView(R.id.ivDelete)
        protected ImageView ivDelete;


        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            // lblDiscountedPrice.setPaintFlags(lblDiscountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }


    public interface ISubCategoryProductAdapter {
        void onClickAddMenu(MenuResponse.Product position);
    }

    public void updateFavouriteData(int position, int isWished) {
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM;
        } else {
            return (position == items.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        }
    }

    public void add(MenuResponse.Product r) {
        items.add(r);
        notifyItemInserted(items.size() - 1);
    }

    public void remove(ProductDataResponse r) {
        int position = items.indexOf(r);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void addAll(List<MenuResponse.Product> productDataResponses) {
        items.clear();
        for (MenuResponse.Product productDataResponse : productDataResponses) {
            add(productDataResponse);
        }
    }
    public boolean isEmpty() {
        return getItemCount() == 0;

    }
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(items.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }




}