package com.application.ordermanagement.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.models.OrdersResponse;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<OrdersResponse.Datum> categoriesEntList;
    MainActivity mainActivity;
    IOrderUpdateAdapter iOrderUpdateAdapter;

    public OrderListingAdapter(MainActivity mainActivity, IOrderUpdateAdapter iOrderUpdateAdapter) {
        this.iOrderUpdateAdapter = iOrderUpdateAdapter;
        this.mainActivity = mainActivity;
        categoriesEntList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final OrdersResponse.Datum datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;

            if (datum.getStatus().equals("Order Placed")) {
                categoryViewHolder.llacceptreject.setVisibility(View.VISIBLE);


                categoryViewHolder.tvstatus.setVisibility(View.GONE);
                categoryViewHolder.tvAccept.setOnClickListener(v -> {

                    iOrderUpdateAdapter.updateOrder(datum.getId(), "Accepted");

                });
                categoryViewHolder.tvReject.setOnClickListener(v -> {
                    iOrderUpdateAdapter.updateOrder(datum.getId(), "Rejected");
                });
            } else {
                categoryViewHolder.llacceptreject.setVisibility(View.GONE);

                switch (datum.getStatus()) {
                    case "Accepted":
                        categoryViewHolder.tvstatus.setBackgroundColor(mainActivity.getResources().getColor(R.color.green_color));
                        break;
                    case "Rejected":
                        categoryViewHolder.tvstatus.setBackgroundColor(mainActivity.getResources().getColor(R.color.red_color));
                        break;
                    case "In Progress":
                        categoryViewHolder.tvstatus.setBackgroundColor(mainActivity.getResources().getColor(R.color.orange_color));
                        break;
                    case "Delivered":
                        categoryViewHolder.tvstatus.setBackgroundColor(mainActivity.getResources().getColor(R.color.blue_login_color));
                        break;
                    case "Order Placed":
                        categoryViewHolder.tvstatus.setBackgroundColor(mainActivity.getResources().getColor(R.color.green_gradient_color_3));
                        break;


                }

                categoryViewHolder.tvstatus.setVisibility(View.VISIBLE);
                categoryViewHolder.tvstatus.setText(datum.getStatus());
            }

            categoryViewHolder.lblOrder.setText("Order Number : " + datum.getId());
            categoryViewHolder.lblDeliveryAddress.setText("Delivery Address : " + datum.getDeliveryAddress());
            categoryViewHolder.lblrefeence.setText("Order Refrence : " + datum.getReference());
            categoryViewHolder.lblPaymentMethid.setText(datum.getPayment());
            categoryViewHolder.lblPrice.setText("£ " + datum.getTotalAmountWithFee());
            categoryViewHolder.lblDeliveryFee.setText("Delivery Fee : " + (datum.getDeliveryFees() > 0 ? "£" + datum.getDeliveryFees() : "Free"));
            //UIHelper.setImagewithGlide(mainActivity,categoryViewHolder.ivOrderImage,datum.getDetails().get(0).);
            categoryViewHolder.itemView.setOnClickListener(v -> {

                iOrderUpdateAdapter.onClickitem(datum);
            });
            try {
                setSomeData(categoryViewHolder,datum);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public interface IOrderUpdateAdapter {
        void updateOrder(int id, String status);

        void onClickitem(OrdersResponse.Datum datum);
    }

    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    public void addAll(List<OrdersResponse.Datum> data) {
        categoriesEntList.clear();
        this.categoriesEntList.addAll(data);
        notifyDataSetChanged();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblOrder)
        TextView lblOrder;
        @BindView(R.id.lblDeliveryAddress)
        TextView lblDeliveryAddress;
        @BindView(R.id.lblDeliveryFee)
        TextView lblDeliveryFee;
        @BindView(R.id.lblPaymentMethid)
        TextView lblPaymentMethid;
        @BindView(R.id.lblrefeence)
        TextView lblrefeence;
        @BindView(R.id.ivOrderImage)
        ImageView ivOrderImage;
        @BindView(R.id.lblPrice)
        TextView lblPrice;
        @BindView(R.id.tvAccept)
        TextView tvAccept;
        @BindView(R.id.tvReject)
        TextView tvReject;
        @BindView(R.id.tvstatus)
        TextView tvstatus;

        @BindView(R.id.llacceptreject)
        LinearLayout llacceptreject;

        @BindView(R.id.lblShippingBy)
        TextView lblShippingBy;
        @BindView(R.id.lblPayment)
        TextView lblPayment;@BindView(R.id.lblNumber)
        TextView lblNumber;

//
//        @BindView(R.id.lblShippingPrice)
//        TextView lblShippingPrice;

        @BindView(R.id.lblsubitemPrice)
        TextView lblsubitemPrice;

        @BindView(R.id.lblDiscountPrice)
        TextView lblDiscountPrice;


        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void setSomeData(CategoryViewHolder categoryViewHolder, OrdersResponse.Datum datum) throws JSONException {

        categoryViewHolder.lblShippingBy.setText("Product Name : "+datum.getDetails().get(0).getProductName());
        categoryViewHolder.lblPayment.setText("Payment :"+(datum.getPayment().equals("cod") ? "Cash On Delivery" : "Credit Card"));
        categoryViewHolder.lblsubitemPrice.setText("Sub Total : " + (datum.getTotalAmountWithFee() +(datum.getDiscounted_amount()!=null?datum.getDiscounted_amount():0) - datum.getDeliveryFees()) + " £");
        if (datum.getDiscounted_amount() !=null && datum.getDiscounted_amount() > 0) {
            categoryViewHolder.lblDiscountPrice.setText("Discount : "+"-" + datum.getDiscounted_amount() + " £");
        }
        categoryViewHolder.lblNumber.setText("Contact Number : "+datum.getPhone_number());
    }


}