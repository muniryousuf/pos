package com.application.ordermanagement.adapter.POS;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class POSCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public ArrayList<OrderPlaceModel.OrderDetail> categoriesEntList;
    MainActivity mainActivity;
    onClickOptions onClickOptions;
    public boolean is_pos = true;

    public POSCartAdapter(MainActivity mainActivity, onClickOptions onClickOptions, boolean is_pos) {
        this.categoriesEntList = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.is_pos = is_pos;
        this.onClickOptions = onClickOptions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(v);
        return categoryViewHolder;
    }

    Spanny spanny, priceSpanny;

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final OrderPlaceModel.OrderDetail datum = categoriesEntList.get(position);
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;

            if (categoriesEntList.get(position).getExtras().size() > 0) {
                categoryViewHolder.ivEdit.setVisibility(View.VISIBLE);
                categoryViewHolder.ivEdit.setClickable(true);
                categoryViewHolder.ivEdit.setEnabled(true);
            } else {
                categoryViewHolder.ivEdit.setAlpha(0.5f);
                categoryViewHolder.ivEdit.setClickable(false);
                categoryViewHolder.ivEdit.setEnabled(false);
                //  categoryViewHolder.ivEdit.setVisibility(View.INVISIBLE);
            }
//            if (is_open_edit) {
//                categoryViewHolder.ivEdit.setVisibility(View.VISIBLE);
//
//            } else {
//                categoryViewHolder.ivEdit.setVisibility(View.GONE);
//
//            }

//            if (is_delete_btn) {
//                categoryViewHolder.ivDelete.setVisibility(View.VISIBLE);
//            } else {
//                categoryViewHolder.ivDelete.setVisibility(View.GONE);
//
//            }
            if (position % 2 == 0) {
                categoryViewHolder.lyBackbround.setBackgroundColor(mainActivity.getResources().getColor(R.color.white));
            } else {
                categoryViewHolder.lyBackbround.setBackgroundColor(mainActivity.getResources().getColor(R.color.bg_gray));

            }
            categoryViewHolder.lblItem.setText(datum.getProductName());
            categoryViewHolder.lblQty.setText("" + datum.getQuantity());
            Float price = 0.0f;
            spanny = new Spanny();
            priceSpanny = new Spanny();
            if (datum.getExtras() != null && datum.getExtras().size() > 0) {
                for (int i = 0; i < datum.getExtras().size(); i++) {
                    //spanny = new Spanny();
                    spanny = spanny.append("", mainActivity.getResources().getDrawable(R.drawable.tick_mark)).append(" \n" + datum.getExtras().get(i).getChoice());
                    // spanny = spanny.append("\n" + datum.getExtras().get(i).getChoice());
                    // priceSpanny = new Spanny();
                    priceSpanny = priceSpanny.append("\n" + "£ " + datum.getExtras().get(i).getPrice());
                    //  if (i == 0) {
                    price = price + Float.valueOf(datum.getExtras().get(i).getPrice());
                    // }

                }
                categoryViewHolder.lblItemAdon.setVisibility(View.VISIBLE);
                categoryViewHolder.lblItemAdonPrice.setVisibility(View.VISIBLE);
                categoryViewHolder.lblItemAdon.setText(spanny);
                categoryViewHolder.lblItemAdonPrice.setText(priceSpanny);
                spanny.clear();
                priceSpanny.clear();


                categoryViewHolder.lblAmount.setText("£ " + setdecimal((float) ((float) datum.getQuantity() * (price > 0 ? (price +datum.getPrice()): datum.getPrice()))));


            } else {
                categoryViewHolder.lblItemAdon.setVisibility(View.GONE);
                categoryViewHolder.lblItemAdonPrice.setVisibility(View.GONE);
                categoryViewHolder.lblAmount.setText("£ " + (datum.getSingleProductTotalAmount() == null ? 0 : setdecimal((float) (datum.getSingleProductTotalAmount() * ((float) datum.getQuantity())))));

            }
            if(is_pos){

                categoryViewHolder.increase.setVisibility(View.VISIBLE);
                categoryViewHolder.decrease.setVisibility(View.VISIBLE);
                categoryViewHolder.ivDelete.setVisibility(View.VISIBLE);
                categoryViewHolder.ivEdit.setVisibility(View.VISIBLE);
                categoryViewHolder.viewOne.setVisibility(View.VISIBLE);
                categoryViewHolder.viewtwo.setVisibility(View.VISIBLE);
                categoryViewHolder.viewthree.setVisibility(View.VISIBLE);
            }else{
              //  categoryViewHolder.increase.setVisibility(View.GONE);
              //  categoryViewHolder.decrease.setVisibility(View.GONE);
              //  categoryViewHolder.ivDelete.setVisibility(View.GONE);
                categoryViewHolder.ivEdit.setVisibility(View.GONE);
                categoryViewHolder.viewOne.setVisibility(View.GONE);
            //    categoryViewHolder.viewtwo.setVisibility(View.GONE);
              //  categoryViewHolder.viewthree.setVisibility(View.GONE);

            }
            categoryViewHolder.increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoriesEntList.get(position).setQuantity(categoriesEntList.get(position).getQuantity() + 1);
                    notifyItemChanged(position);
                    onClickOptions.onIncrese();

                }
            });
            categoryViewHolder.decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (categoriesEntList.get(position).getQuantity() == 1) {
                        // notifyItemRemoved(position);
                        categoriesEntList.remove(position);
                        notifyDataSetChanged();
                    } else {
                        categoriesEntList.get(position).setQuantity(datum.getQuantity() - 1);
                        notifyItemChanged(position);
                    }
                    onClickOptions.onDecrese();
                }
            });
            if (is_pos) {

            } else {

                categoryViewHolder.lblQty.setClickable(false);
                categoryViewHolder.ivEdit.setVisibility(View.GONE);
            }

            categoryViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoriesEntList.remove(position);
                    is_delete_btn = false;
                    notifyDataSetChanged();
                    onClickOptions.onDeleteOption(position);
                    //  notifyDataSetChanged();
                }
            });
            categoryViewHolder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //categoriesEntList.remove(position);
                    if (categoriesEntList.get(position).getExtras().size() > 0) {
                        onClickOptions.onClickOption(categoriesEntList.get(position), position);
                        is_open_edit = false;
                        notifyDataSetChanged();
                    }

                }
            });


//            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    openDeletepopup(mainActivity, position);
//
//                    return false;
//                }
//            });


        }
    }

    public String setdecimal(float a) {
        NumberFormat formatter = new DecimalFormat("##.##");
        formatter.setRoundingMode(RoundingMode.CEILING);
        return formatter.format(a);
    }

    public void addAll(List<OrderPlaceModel.OrderDetail> categoriesEntList) {

        this.categoriesEntList.clear();
        this.categoriesEntList.addAll(categoriesEntList);
        notifyDataSetChanged();
    }

    public void add(OrderPlaceModel.OrderDetail categoriesEntList) {
        this.categoriesEntList.add(categoriesEntList);
        notifyDataSetChanged();
    }

    public ArrayList<OrderPlaceModel.OrderDetail> getCart() {
        return categoriesEntList;

    }

    @Override
    public int getItemCount() {
        return categoriesEntList.size();
    }

    //  public int getItemCount() {
    //  return 10;
    ////  }
    public boolean is_delete_btn = false;

    public void viewAllDelete(boolean is_delete_btn) {
        this.is_delete_btn = is_delete_btn;
        notifyDataSetChanged();

    }

    public void remove(int deletedposition) {
        categoriesEntList.remove(deletedposition);
        notifyDataSetChanged();
    }

    public double getCureentCartAmount() {
        double total_amount = 0;
        double price = 0;



        for(int i =0 ;i <categoriesEntList.size();i++){



            if (categoriesEntList.get(i).getExtras() != null && categoriesEntList.get(i).getExtras().size() > 0) {
                for (int j = 0; j < categoriesEntList.get(i).getExtras().size(); j++) {

                    //  if (i == 0) {
                    price = price + Float.parseFloat(categoriesEntList.get(i).getExtras().get(j).getPrice());
                    // }

                }
                total_amount= (total_amount) + (categoriesEntList.get(i).getQuantity() * (price > 0 ? (price +categoriesEntList.get(i).getPrice()): categoriesEntList.get(i).getPrice()));


            }else{
                total_amount =total_amount +(categoriesEntList.get(i).getSingleProductTotalAmount() * ((float) categoriesEntList.get(i).getQuantity()));
            }



        }
        return total_amount;
    }

     class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblItem)
        TextView lblItem;

        @BindView(R.id.lblItemAdonPrice)
        TextView lblItemAdonPrice;
        @BindView(R.id.lblItemAdon)
        TextView lblItemAdon;
        @BindView(R.id.lblAmount)
        TextView lblAmount;
        @BindView(R.id.lblQty)
        TextView lblQty;
        @BindView(R.id.lyBackbround)
        LinearLayout lyBackbround;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;
        @BindView(R.id.increase)
        ImageView increase;
        @BindView(R.id.decrease)
        ImageView decrease;

        @BindView(R.id.viewOne)
        View viewOne;
        @BindView(R.id.viewtwo)
        View viewtwo;
        @BindView(R.id.viewthree)
        View viewthree;


        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClickOptions {
        void onDeleteOption(int pos);
        void onIncrese();
        void onDecrese();

        void onClickOption(OrderPlaceModel.OrderDetail orderDetail, int deletedposition);

    }

    public boolean is_open_edit = true;

    public void openPOSEditOption(boolean is_open_edit) {

        this.is_open_edit = is_open_edit;
        notifyDataSetChanged();
    }

    public void openDeletepopup(final Activity activity, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setMessage("Remove from cart");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                activity.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        categoriesEntList.remove(position);
                        notifyDataSetChanged();
                        //activity.finish();
                    }
                });

        builder1.setNegativeButton(
                activity.getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}