package com.application.ordermanagement.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.BuildConfig;
import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.HomeFragment;
import com.application.ordermanagement.fragments.LoginFragment;
import com.application.ordermanagement.fragments.OrderListingFragment;
import com.application.ordermanagement.fragments.SideMenuFragment;
import com.application.ordermanagement.global.SideMenuChooser;
import com.application.ordermanagement.global.SideMenuDirection;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.GooglePlaceHelper;
import com.application.ordermanagement.helper.PrinterCommands;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.DataLoadedListener;
import com.application.ordermanagement.interfaces.MediaTypePicker;
import com.application.ordermanagement.interfaces.OnActivityResultInterface;
import com.application.ordermanagement.models.FCMPayload;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Calendar;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.utils.Orientation;
import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MainActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, DataLoadedListener {
    public RadioButton rbHome, rbSearch, rbNotification, rbAccount;
    public boolean isNavigationGravityRight = false;
    public FrameLayout sideMneuFragmentContainer;
    //  ImageView imgLoader;
    FrameLayout imgLoader;
    Titlebar titlebar;
    public FrameLayout mainFrame;

    RadioGroup rgTabStart, rgTabEnd;
    DrawerLayout drawerLayout;
    private boolean isLoading;
    private String sideMenuType;
    private String sideMenuDirection;
    private View contentView;
    public SideMenuFragment sideMenuFragment;
    ImageView btnAddProduct;
    ConstraintLayout bottombar;
    //public RadioButton rbHome, rbCart, rbOrder, rbAccount;
    // private boolean isLoading;
    OnActivityResultInterface onActivityResultInterface;
    ArrayList<String> photoPaths;
    MediaTypePicker mediaPickerListener;
    // private Category categories;

    int minWindowWidth, minWindowHeight, aspectRatioX, aspectRatioY;

    Bundle bundle;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    BroadcastReceiver receiver;
    IntentFilter filter;
//    public void show(int id) {
//         Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.dialog);
//
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        TextView view_Detail = (TextView) dialog.findViewById(R.id.view_detail);
//        text.setText("Order Number : 000" + id);
//        view_Detail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                clearBackStack();
//                replaceFragment(new HomeFragment(), true, true);
//                replaceFragment(new OrderListingFragment(), true, true);
//
//            }
//        });
//
//        dialog.show();
//
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String id) {
       // show(Integer.valueOf(id));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);

        if (getIntent().getExtras() != null) {
            bundle = new Bundle();
            bundle = getIntent().getExtras();
        }



        init();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String id = intent.getExtras().getString("id");
                String total_amount = intent.getExtras().getString("total_amount");
                String reference = intent.getExtras().getString("reference");
                String order_typo = intent.getExtras().getString("order_typo");
                String paymento = intent.getExtras().getString("paymento");

                show(id, total_amount, reference, order_typo, paymento);


            }
        };

//
//        sideMenuType = SideMenuChooser.DRAWER.getValue();
//        sideMenuDirection = SideMenuDirection.LEFT.getValue();
//        settingSideMenu(sideMenuType, sideMenuDirection);
//        titlebar.setMenuOnclickListener(view -> {
//            if (sideMenuDirection.equals(SideMenuDirection.LEFT.getValue())) {
//                drawerLayout.openDrawer(Gravity.LEFT);
//            } else {
//                drawerLayout.openDrawer(Gravity.RIGHT);
//            }
//        });

    }

    public void showLoader() {
        //   imgLoader.show();
        imgLoader.setVisibility(View.VISIBLE);
        //   isLoading = true;
    }

    public void hideLoader() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imgLoader.setVisibility(View.GONE);
            }
        }, 1500);

        // isLoading = false;
    }

    private void init() {
        //   imgLoader = findViewById(R.id.imgLoader);
        imgLoader = findViewById(R.id.avi);
        //showAnimation(imgLoader);
        mainFrame = findViewById(R.id.mainFrame);
        bottombar = findViewById(R.id.bottomBar);
        btnAddProduct = findViewById(R.id.btnAddproduct);

        setFrame(R.id.mainFrame);
        titlebar = findViewById(R.id.title);

        rbHome = findViewById(R.id.rbHome);
        rbSearch = findViewById(R.id.rbSearch);
        rbNotification = findViewById(R.id.rbNotification);
        rbAccount = findViewById(R.id.rbprofile);

        rbHome.setOnCheckedChangeListener(this);
        rbSearch.setOnCheckedChangeListener(this);
        rbNotification.setOnCheckedChangeListener(this);
        rbAccount.setOnCheckedChangeListener(this);


        rgTabStart = findViewById(R.id.rgTabsStart);
        rgTabEnd = findViewById(R.id.rgTabsEnd);

        sideMneuFragmentContainer = findViewById(R.id.sideMneuFragmentContainer);
        contentView = findViewById(R.id.contentView);

        addFragment(prefHelper.getLoginStatus() ? new HomeFragment() : new LoginFragment(), true, true);
//        btnAddProduct.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (prefHelper != null && prefHelper.getUser() != null) {
//                    ProductCreation.getInstance().reset();
//                    replaceFragment(new AddProductFragment(), true, false);
//                } else {
//
//                    UIHelper.showToast(MainActivity.this, getString(R.string.please_login_addprodcuts));
//                }
//            }
//        });


    }

    public void hideBottombar() {

        bottombar.setVisibility(View.GONE);
    }

    public void showBottombar() {

        bottombar.setVisibility(View.VISIBLE);
    }

    public RadioGroup getTab() {
        if (rbHome.isChecked() || rbSearch.isChecked())
            return rgTabStart;
        else return rgTabEnd;
    }

    @Override
    public void onDataLoaded() {
        if (bundle != null) {
            FCMPayload fcmPayload = (FCMPayload) bundle.getSerializable(AppConstant.FcmHelper.FCM_DATA_PAYLOAD);
            if (fcmPayload != null) {
                if (fcmPayload.getAction_type().equals(AppConstant.FcmHelper.ACTION_TYPE_JOB) || fcmPayload.getAction_type().equals(AppConstant.FcmHelper.COMPLETED)) {
//                    CartFragment cartFragment = new CartFragment();
//                    cartFragment.setFromNotification(true);
//                    replaceFragment(cartFragment, true, true);
//                    bundle = null;
                } else if (fcmPayload.getAction_type().equals(AppConstant.FcmHelper.ACCEPTED) || fcmPayload.getAction_type().equals(AppConstant.FcmHelper.CANCELLED)) {
//                    OrdersHistoryFragment ordersHistoryFragment = new OrdersHistoryFragment();
////                    ordersHistoryFragment.setFromNotification(true);
////                    ordersHistoryFragment.setActionId(fcmPayload.getAction_id());
////                    replaceFragment(ordersHistoryFragment, true, false);
                    bundle = null;
                }
            }
        }
    }

    private void settingSideMenu(String type, String direction) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (type.equals(SideMenuChooser.DRAWER.getValue())) {

            DisplayMetrics matrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(matrics);
            Long longwidth = Math.round(matrics.widthPixels * 0.75);
            int drawerwidth = longwidth.intValue();
            DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams(drawerwidth, (int) DrawerLayout.LayoutParams.MATCH_PARENT);


            if (direction.equals(SideMenuDirection.LEFT.getValue())) {
                params.gravity = Gravity.LEFT;
                sideMneuFragmentContainer.setLayoutParams(params);
            } else {
                params.gravity = Gravity.RIGHT;
                sideMneuFragmentContainer.setLayoutParams(params);
            }

            sideMenuFragment = SideMenuFragment.newInstance();

//            HomeFragment homeFragment = new HomeFragment();
//            homeFragment.setDataLoadedListener(this);
//            replaceFragment(homeFragment, true, false);
/*
            if (prefHelper != null && prefHelper.getLoginStatus()) {
//                sideMenuFragment.setListner(prefHelper);
                replaceFragment(new HomeFragment(), true, false);

            } else {

                replaceFragment(new HomeFragment(), true, false);
            }
*/
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            transaction.replace(R.id.sideMneuFragmentContainer, sideMenuFragment).commit();
            setDrawerListeners();
            drawerLayout.closeDrawers();
        }
    }

    public void lockDrawer() {
        try {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDrawerListeners() {
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (!isNavigationGravityRight) {
                    contentView.setTranslationX(slideOffset * drawerView.getWidth());
                    drawerLayout.bringChildToFront(drawerView);
                    drawerLayout.requestLayout();
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                isNavigationGravityRight = false;
                drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (!isNavigationGravityRight) {
                    if (newState == DrawerLayout.STATE_SETTLING) {
                        if (!drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            /*Blurry.with(MainActivity.this)
                                    .radius(15)
                                    .sampling(3)
                                    .async()
                                    .animate(500)
                                    .onto(contentView);*/
                            // setBlurBackground();
                        } else {
                            // Blurry.delete(contentView);
                            // removeBlurImage();
                        }
                    }
                }
            }
        });
    }

    public Titlebar getTitleBar() {
        return titlebar;
    }

    @Override
    public void onBackPressed() {

        UIHelper.hideSoftKeyboards(this);

        if (getSupportFragmentManager().getBackStackEntryCount() > 1 && !isLoading) {
            getSupportFragmentManager().popBackStack();
        } else {
            UIHelper.openExitPopUp(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        switch (compoundButton.getId()) {
            case R.id.rbHome:
                rgTabEnd.clearCheck();
                if (rbHome.isChecked())
                    //    replaceFragmentWithClearBackStack(new HomeFragment(), true, false);
                    break;
            case R.id.rbSearch:
                rgTabEnd.clearCheck();
                if (rbSearch.isChecked()) {
//                    willbeimplementedinfuture();
                    //    replaceFragmentWithClearBackStack(new FragmentCategory(true), true, false);
                }
                break;
            case R.id.rbNotification:
                rgTabStart.clearCheck();
                if (rbNotification.isChecked()) {
                    if(prefHelper.getLoginStatus()) {
                        // replaceFragmentWithClearBackStack(new NotificationsFragment(), true, false);
                    } else {
                        UIHelper.showToast(this, getString(R.string.please_login_viewnotifications));
                    }
                }
                break;
            case R.id.rbprofile:
                rgTabStart.clearCheck();
                if (rbAccount.isChecked()) {
                    if (prefHelper.getLoginStatus()) {
                        //  replaceFragmentWithClearBackStack(new UserAccountFragment(), true, false);
                    } else {

                        UIHelper.showToast(MainActivity.this, getString(R.string.please_login_notification));
                    }
                }


                break;
        }

    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        switch (requestCode) {
            case GooglePlaceHelper.REQUEST_CODE_AUTOCOMPLETE:
                try {
                    onActivityResultInterface.onActivityResultInterface(requestCode, resultCode, data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Activity.RESULT_OK:
                //  getLocation();

                break;
            case Activity.RESULT_CANCELED:
                // The user was asked to change settings, but chose not to
                Toast.makeText(this, "Location Service not Enabled", Toast.LENGTH_SHORT).show();
                break;
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
//                    data.getExtras().getStringArrayList(FilePickerConst.KEY_SELECTED_MEDIA).get(0);
                    File file = new File(data.getExtras().getStringArrayList(FilePickerConst.KEY_SELECTED_MEDIA).get(0));
                    doCrop(Uri.fromFile(file), minWindowWidth, minWindowHeight, aspectRatioX, aspectRatioY);
//                    photoPaths = new ArrayList<>();
//                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
//                    new AsyncTaskRunner().execute(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {


                    mediaPickerListener.onDocClicked(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));

                }
                break;
            case AppConstant.CAMERA_PIC_REQUEST:
                if (data != null && data.getExtras() != null && data.getExtras().get("data") != null) {

                    ArrayList<String> cameraPic = new ArrayList<>();
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(this, (Bitmap) data.getExtras().get("data"));
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    cameraPic.add(getRealPathFromURI(tempUri));


                    new AsyncTaskRunner().execute(cameraPic);

                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                cropImageResult(resultCode, data);
                break;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void setOnActivityResultInterface(OnActivityResultInterface activityResultInterface) {
        this.onActivityResultInterface = activityResultInterface;
    }

    public void openFilePicker(final MediaTypePicker listener, final int count,
                               final Boolean videoenabling) {

        TedPermission.with(this)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        mediaPickerListener = listener;
                        FilePickerBuilder.getInstance()
                                .setMaxCount(count)
                                //.setSelectedFiles(photoPaths)
                                .setActivityTheme(R.style.FilePickeTheme)
                                .enableVideoPicker(videoenabling)
                                .enableCameraSupport(true)
                                .showGifs(true)
                                .enableSelectAll(false)
                                .enableImagePicker(true)
                                .showFolderView(false)
                                .withOrientation(Orientation.UNSPECIFIED)
                                .pickPhoto(MainActivity.this);

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> arrayList) {
                        //Utils.showToast(this, getString(R.string.permission_denied));
                    }
                }).check();
    }

    public void openFilePicker(final MediaTypePicker listener, final int count,
                               final Boolean videoenabling, int minWindowWidth, int minWindowHeight, int aspectRatioX,
                               int aspectRatioY) {
        this.minWindowWidth = minWindowWidth;
        this.minWindowHeight = minWindowHeight;
        this.aspectRatioX = aspectRatioX;
        this.aspectRatioY = aspectRatioY;

        TedPermission.with(this)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        mediaPickerListener = listener;
                        FilePickerBuilder.getInstance()
                                .setMaxCount(count)
                                //.setSelectedFiles(photoPaths)
                                .setActivityTheme(R.style.FilePickeTheme)
                                .enableVideoPicker(videoenabling)
                                .enableCameraSupport(true)
                                .showGifs(true)
                                .enableSelectAll(false)
                                .enableImagePicker(true)
                                .showFolderView(false)
                                .withOrientation(Orientation.UNSPECIFIED)
                                .pickPhoto(MainActivity.this);

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> arrayList) {
                        //Utils.showToast(this, getString(R.string.permission_denied));
                    }
                }).check();
    }

    public void doCrop(Uri uri) {
        Intent intent = CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMinCropWindowSize(500, 350)
                .setAspectRatio(16, 9)
                .getIntent(this);

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public void doCrop(Uri uri, int minWindowWidth, int minWindowHeight, int aspectRatioX,
                       int aspectRatioY) {
        Intent intent = CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMinCropWindowSize(minWindowWidth, minWindowHeight)
                .setAspectRatio(aspectRatioX, aspectRatioY)
                .getIntent(this);

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private void cropImageResult(int resultCode, Intent data) {
        CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (resultCode == RESULT_OK) {
            Uri resultUri = result.getUri();
            ArrayList<String> cameraPic = new ArrayList<>();
            cameraPic.add(resultUri.getPath());
            new AsyncTaskRunner().execute(cameraPic);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Exception error = result.getError();
            Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

        }
    }

//    public void openMap(double latitude, double longitude, String title, String label) {
//        Intent mapActivity = new Intent(this, MapActivity.class);
//        mapActivity.putExtra("latitude", latitude);
//        mapActivity.putExtra("longitude", longitude);
//        mapActivity.putExtra("title", title);
//        mapActivity.putExtra("label", label);
//        startActivity(mapActivity);
//    }



    private class AsyncTaskRunner extends AsyncTask<ArrayList<String>, ArrayList<File>, ArrayList<File>> {

        ProgressDialog progressDialog;

        @SafeVarargs
        @Override
        protected final ArrayList<File> doInBackground(ArrayList<String>... params) {

            ArrayList<File> compressedAndVideoImageFileList = new ArrayList<>();

            for (int index = 0; index < params[0].size(); index++) {

                File file = new File(params[0].get(index));

                if (file.toString().endsWith(".jpg") ||
                        file.toString().endsWith(".jpeg") ||
                        file.toString().endsWith(".png") ||
                        file.toString().endsWith(".gif")) {
                    try {
                        File compressedImageFile = new Compressor(MainActivity.this).compressToFile(file, "compressed_" + file.getName());
                        compressedAndVideoImageFileList.add(compressedImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    compressedAndVideoImageFileList.add(file);
                }
            }
            return compressedAndVideoImageFileList;
        }


        @Override
        protected void onPostExecute(ArrayList<File> result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();

            mediaPickerListener.onPhotoClicked(result);


        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    getApplicationContext().getString(R.string.app_name),
                    getApplicationContext().getString(R.string.compressing_image_please_wait));

            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
    }

//    public void showRegistrationActivity() {
//        Intent i = new Intent(this, RegistrationActivity.class);
//        i.putExtra("login", "login");
//        startActivity(i);
//        finish();
//    }

    public void getSetCode(OnCountryPickerListener listener) {
        CountryPicker.Builder builder =
                new CountryPicker.Builder().with(this)
                        .listener(listener);
        CountryPicker picker = builder.build();
        picker.showBottomSheet(this);
    }

public void show(String id, String total_amount, String reference, String order_typo, String paymento) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView tvTotalAmount = (TextView) dialog.findViewById(R.id.tvTotalAmount);
        TextView tvOrderId = (TextView) dialog.findViewById(R.id.tvOrderId);
        TextView tvOrderType = (TextView) dialog.findViewById(R.id.tvOrderType);
        TextView tvPayment = (TextView) dialog.findViewById(R.id.tvPayment);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);
        tvTotalAmount.setText(" £ " + Double.valueOf(total_amount));
        tvOrderId.setText(id);
        tvOrderType.setText(order_typo);
        tvPayment.setText(paymento);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                is_accepted = true;
                callapi(String.valueOf(id), "Accepted");
//                this.clearBackStack();
//                replaceFragment(new HomeFragment(), true, true);
//                replaceFragment(new OrderListingFragment(), true, true);

            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                is_accepted = false;
                callapi(String.valueOf(id), "Rejected");
//                this.clearBackStack();
//                replaceFragment(new HomeFragment(), true, true);
//                replaceFragment(new OrderListingFragment(), true, true);

            }
        });
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.notification);
        mp.start();
        dialog.getWindow().getAttributes().windowAnimations = R.style.topAnimation;
        dialog.show();

    }

    public class ProductLisitng {
        public ProductLisitng(String productname, Float price, String quantity, String extra, String special_instructions) {
            this.productname = productname;
            this.extra = extra;
            this.price = price;
            this.quantity = quantity;
            this.special_instructions = special_instructions;
        }

        String productname;

        public String getExtra() {
            return extra;
        }

        public void setExtra(String extra) {
            this.extra = extra;
        }

        String extra;

        public String getSpecial_instructions() {
            return special_instructions;
        }

        public void setSpecial_instructions(String special_instructions) {
            this.special_instructions = special_instructions;
        }

        String special_instructions;
        Float price;

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        String quantity;

    }

    ArrayList<ProductLisitng> details;
    String id, delivery_address, phone_number, payment, order_type, user_id;
    Float total_amount_with_fee, delivery_fees;
    Float discounted_amount;
    boolean is_accepted = true;

    public void callapi(String order_id, String status) {
        details = new ArrayList<>();

        String url = AppConstant.BASE_URL + "update-order/" + order_id;


        RequestBody formBody = new FormBody.Builder()
                .add("status", status)
                .build();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .put(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                if (response.code() == 200) {
                    if (!is_accepted) {

                    } else {
                        String responseString = response.body().string();
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(responseString);
                            JSONObject jsonObject = obj.getJSONObject("data");
                            id = jsonObject.getString("id");
                            user_id = jsonObject.getString("user_id");
                            payment = jsonObject.getString("payment");
                            delivery_address = jsonObject.getString("delivery_address");
                            phone_number = jsonObject.getString("phone_number");
                            total_amount_with_fee = Float.valueOf(jsonObject.getString("total_amount_with_fee"));
                            discounted_amount = Float.valueOf(jsonObject.getString("discounted_amount"));
                            delivery_fees = Float.valueOf(jsonObject.getString("delivery_fees"));
                            if (jsonObject.getString("order_type") != null) {
                                order_type = jsonObject.getString("order_type");

                            }
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String product_name = object.getString("product_name");
                                Float price = Float.valueOf(object.getString("price"));
                                String quantity = object.getString("quantity");
                                String extras = object.getString("extras");
                                String special_instruction = object.getString("special_instructions");


                                details.add(new ProductLisitng(product_name, price, quantity, extras, special_instruction != null ? special_instruction : ""));


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        //   OrderUpdateResponse orderUpdateResponse =(OrderUpdateResponse) response.body();
                        if (this.prefHelper.getIp() != null) {
                            Log.v("Printer Printing", "Printer one");
                            for (int j = 0; j < 2; j++) {
                                printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, prefHelper.getIp(), user_id, discounted_amount, phone_number, 1);
                            }
                        }

                    }


                }

            }


            response.body().close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static OutputStream outputStream;

    public void printSlip(ArrayList<ProductLisitng> details,
                          String id, String delivery_address,
                          Float delivery_fees, Float total_amount_with_fee,
                          String pay, String order_type, String ip, String user_id, Float discounted_amount, String phone_number, int i) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(prefHelper.getResturant().getName(), 3, 1);
            printNewLine();
            //printCustom("Pizza and Fish and Chips Specialists", 0, 1);
            printCustom(prefHelper.getResturant().getAddress().getAddress(), 0, 1);
            printCustom("Phone No : " + prefHelper.getResturant().getPhoneNumber(), 0, 1);
            printCustom("Post Code : " + prefHelper.getResturant().getAddress().getZipCode(), 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printCustom(order_type, 2, 1);
            printNewLine();
            if (order_type != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " /*+ dateTime[1]*/, 3, 1);

            }
            printCustom("Order Number : 000" + id/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            if(!order_type.equalsIgnoreCase("Pickup")){
                printCustom("Restaurant Notes ", 0, 0);
                printCustom("Contact Free Delivery", 3, 0);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            }


            printNewLine();
            for (int j = 0; j < details.size(); j++) {
                printCustom(padLine(details.get(j).getQuantity() + " x " + details.get(j).getProductname(), " £ " + (Float.parseFloat(details.get(j).getQuantity())*details.get(j).getPrice()), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(details.get(j).getExtra());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (details.get(j).getSpecial_instructions() != null && !details.get(j).getSpecial_instructions().equals("") && !details.get(j).getSpecial_instructions().equals("null")) {

                        spanny.append(" **" + details.get(j).getSpecial_instructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }


            }

           // printCustom(padLine("Food and drink total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);

            printCustom(padLine("Delivery Fee", delivery_fees > 0 ? "£" + delivery_fees + "/=" : "   Free", 45) + "\n", 1, 0);
            if (discounted_amount > 0) {

                printCustom(padLine("Discount", "- £" + discounted_amount + "/=", 45) + "\n", 1, 0);


            }
            printCustom(padLine("Total", "£" + Float.sum(total_amount_with_fee, /*delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);
            printNewLine();
            printNewLine();
            if(!payment.equalsIgnoreCase("cod")){
                printCustom("Paid By :", 0, 0);
            }
            printCustom(padLine(payment.equalsIgnoreCase("cod") ? (order_type.equalsIgnoreCase("Pickup")?"Cash on Collection":"Cash on Delivery") : "Card : xxxxxxxxxxxxxx", "£" + Float.sum(total_amount_with_fee, /*delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);


            if (!payment.equalsIgnoreCase("cod")) {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER HAS BEEN PAID", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                if (!payment.equalsIgnoreCase("cod")) {
                    printCustom("PAID ONLINE ", 2, 1);

                }
            } else {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom((order_type.equalsIgnoreCase("Pickup")?"CASH ON COLLECTION":"CASH ON DELIVERY"), 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER NOT PAID", 2, 1);

            }
            printCustom("Please Check Notes for Instructions ", 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);

            printNewLine();
            printCustom("Customer ID: ", 0, 0);
            printCustom("" + user_id, 2, 0);
            printNewLine();
            printCustom("Customer details: ", 0, 0);
            try{
            String[] splited = delivery_address.split("\\s+");
            if(splited.length<3){
                printCustom(delivery_address, 2, 0);
               // printNewLine();
            }else if(splited.length<4){
                printCustom(splited[0]+" "+splited[1]+" "+splited[2], 2, 0);
               // printNewLine();
            }else if(splited.length<5){
                printCustom(""+splited[0].concat(" "+splited[1]).concat(" "+splited[2]), 2, 0);
              //  printNewLine();
                printCustom(""+splited[3], 2, 0);

            }else if(splited.length<6){
                printCustom(""+splited[0].concat(" "+splited[1]).concat(" "+splited[2]), 2, 0);
               /// printNewLine();
                printCustom(""+splited[3].concat(" "+splited[4]), 2, 0);

            }else  if(splited.length<7){
                printCustom(""+splited[0].concat(" "+splited[1]).concat(" "+splited[2]), 2, 0);
               // printNewLine();
                printCustom(""+splited[3].concat(" "+splited[4]).concat(" "+splited[5]), 2, 0);
               // printNewLine();

            }else if(splited.length<8){
                printCustom(""+splited[0].concat(" "+splited[1]).concat(" "+splited[2]), 2, 0);
               // printNewLine();
                printCustom(""+splited[3].concat(" "+splited[4]).concat(" "+splited[5]), 2, 0);
               // printNewLine();
                printCustom(""+splited[6], 2, 0);

            }else if(splited.length<9){
                printCustom(""+splited[0].concat(" "+splited[1]).concat(" "+splited[2]), 2, 0);
              //  printNewLine();
                printCustom(""+splited[3].concat(" "+splited[4]).concat(" "+splited[5]), 2, 0);
               // printNewLine();
                printCustom(""+splited[6].concat(" "+splited[7]), 2, 0);

            }else{

            }}
            catch (Exception exception){
                printCustom(delivery_address, 2, 0);

            }
            printNewLine();

           // printNewLine();
            printCustom("To Contact Customer Call: ", 0, 0);
            printCustom(phone_number, 2, 0);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();

            if (i == 1) {
                if (prefHelper.getIptwo() != null) {
                    Log.v("Printer Printing", "Printer two");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, prefHelper.getIptwo(), user_id, discounted_amount, phone_number, 2);
                } else {
                    return;
                }


            }
            if (i == 2) {
                if (prefHelper.getIpthree() != null) {
                    Log.v("Printer Printing", "Printer three");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, prefHelper.getIpthree(), user_id, discounted_amount, phone_number, 3);
                } else {
                    return;
                }


            }

            if (i == 3) {
                if (prefHelper.getIpfour() != null) {
                    Log.v("Printer Printing", "Printer four");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, prefHelper.getIpfour(), user_id, discounted_amount, phone_number, 4);
                } else {
                    return;
                }


            }
            if (i == 4) {
                if (prefHelper.getIpfive() != null) {
                    Log.v("Printer Printing", "Printer five");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, prefHelper.getIpfive(), user_id, discounted_amount, phone_number, -1);
                } else {
                    return;
                }


            }


//            if(i==-1){
//                return;
//            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(this, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(this, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    protected String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine) {
        if (partOne == null) {
            partOne = "";
        }
        if (partTwo == null) {
            partTwo = "";
        }
        String concat;
        if ((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + " " + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(" ", padding) + partTwo;
        }
        return concat;
    }

    /**
     * utility: string repeat
     */
    protected String repeat(String str, int i) {
        return new String(new char[i]).replace("\0", str);
    }


    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
           // outputStream.write(msg.getBytes(),0,msg.getBytes().length);
            outputStream.write(msg.getBytes("Cp858"));
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print photo

    //print unicode


    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print text

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime[] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);

    }

    @Override
    public void onResume() {
        super.onResume();
        filter = new IntentFilter(BuildConfig.APPLICATION_ID);
        registerReceiver(receiver, filter);

    }
}
