package com.application.ordermanagement.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;


import com.application.ordermanagement.R;
import com.application.ordermanagement.helper.BasePreferenceHelper;

import jp.shts.android.storiesprogressview.StoriesProgressView;


public class SplashActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener {

    final int MIN_TIME_INTERVAL_FOR_SPLASH = 2500; // in millis
    StoriesProgressView storiesProgressView;
    ProgressBar mProgress;
    BasePreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferenceHelper = new BasePreferenceHelper(this);
        //mProgress = (ProgressBar) findViewById(R.id.splash_screen_progress_bar);

        // storiesProgressView = findViewById(R.id.stories);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            public void run() {
               doWork();
                try {
                    Thread.sleep(500);
                    initActivity();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //   finish();
            }
        }).start();


        //launchTimerAndTask();
        // initAnimation();
    }

    private void doWork() {
        for (int progress = 0; progress < 100; progress += 10) {
            try {
                Thread.sleep(500);
                mProgress.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
                //  Timber.e(e.getMessage());
            }
        }
    }



    private void initActivity() {
        showMainActivity();
    }


    private void showRegistration() {
//        Intent i = new Intent(this, RegistrationActivity.class);
//        startActivity(i);
//        finish();
    }

    private void showMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        // Very important !
//        storiesProgressView.destroy();
        super.onDestroy();
    }

    @Override
    public void onNext() {

    }

    @Override
    public void onPrev() {

    }

    @Override
    public void onComplete() {
      //  initActivity();
    }
}