package com.application.ordermanagement.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {

            return false;
        }
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean is3g =false;
        boolean isWifi =false;
        if (manager == null)
            return false;
        NetworkInfo.State mobile =  NetworkInfo.State.DISCONNECTED;
        NetworkInfo.State wifi= NetworkInfo.State.DISCONNECTED;
        if(manager.getNetworkInfo(0)!=null){

           mobile = manager.getNetworkInfo(0).getState();

        }
        if(manager.getNetworkInfo(1)!=null){

            wifi = manager.getNetworkInfo(1).getState();


        }


        if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
            //mobile
             is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting();
        } else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
            //wifi
             isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting();
        }
//        // 3g-4g available
//        boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
//                .isConnectedOrConnecting();
//        // wifi available
//        boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
//                .isConnectedOrConnecting();

        // System.out.println(is3g + " net " + isWifi);

        if (!is3g && !isWifi) {
            return false;
        } else
            return true;
    }


    }
