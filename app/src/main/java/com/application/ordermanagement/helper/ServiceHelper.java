package com.application.ordermanagement.helper;

import android.app.Activity;

import android.support.annotation.NonNull;
import android.util.Log;


import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.global.WebServiceConstants;
import com.application.ordermanagement.interfaces.webServiceResponseLisener;
import com.application.ordermanagement.webservice.Api_Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 7/17/2017.
 */

public class ServiceHelper<T> {
    private webServiceResponseLisener serviceResponseLisener;
    private Activity currentActivity;

    public ServiceHelper(webServiceResponseLisener serviceResponseLisener, Activity activity) {
        this.serviceResponseLisener = serviceResponseLisener;
        currentActivity = activity;
    }

    private void showLoader() {
        if (currentActivity instanceof MainActivity)
            ((MainActivity) currentActivity).showLoader();
//        else
//            ((RegistrationActivity) currentActivity).showLoader();
    }

    private void hideLoader() {
        if (currentActivity instanceof MainActivity)
            ((MainActivity) currentActivity).hideLoader();
//        else
////            ((RegistrationActivity) currentActivity).hideLoader();
    }

    @SuppressWarnings("ConstantConditions,unchecked")
    public void enqueueCall(Call<Api_Response<T>> call, final String tag) {
        showLoader();
        if (!NetworkUtils.isNetworkAvailable(currentActivity)) {
            hideLoader();
            UIHelper.showToast(currentActivity, currentActivity.getResources().getString(R.string.no_connection));
            serviceResponseLisener.ResponseFailure(tag);
            return;
        }
        call.enqueue(new Callback<Api_Response<T>>() {
            @Override
            public void onResponse(@NonNull Call<Api_Response<T>> call, @NonNull Response<Api_Response<T>> response) {

                if (response.body() != null) {
                    if (response.code()== (WebServiceConstants.SUCCESS_RESPONSE_CODE)) {
                        serviceResponseLisener.ResponseSuccess(response.body(), tag);
                        ///    UIHelper.showToast(currentActivity, response.body().getMessage());
                        switch (tag) {
                           // case AppConstant.GET_PRODUCT_DAT:
                               // break;
                            case AppConstant.UPDATE_ORDERS:
                            case AppConstant.UPDATE_PRODUCTS:
                               // UIHelper.showAlertDialog(currentActivity, currentActivity.getString(R.string.success), response.body().getMessage());

                                break;
                            case WebServiceConstants.USER_UPDATE:
                            case AppConstant.FORGOT_PASSWORD:
                                UIHelper.showToast(currentActivity, response.body().getMessage());
                                break;

                            case AppConstant.DELETE_PRODUCT:
                              //  UIHelper.showToast(currentActivity, response.body().getMessage());
                                break;


                            case AppConstant.RESEND_CODE:
                                UIHelper.showToast(currentActivity, response.body().getMessage());
                                break;


//                            case AppConstant.UPDATE_PASSWORD:
//                                UIHelper.showToast(currentActivity, currentActivity.getResources().getString(R.string.password_reset));
//                                break;
//
//                            case AppConstant.USER_CHANGE_PASSWORD:
//                                UIHelper.showToast(currentActivity, currentActivity.getResources().getString(R.string.password_changed));
//                                break;

                            case AppConstant.ADD_TO_FAV:
                                UIHelper.showToast(currentActivity, response.body().getMessage());
                                break;
                            case AppConstant.ADD_TO_CART:

                                UIHelper.showToast(currentActivity, response.body().getMessage());

                                break;
                                case AppConstant.REMOVE_FROM_CART:

                                UIHelper.showToast(currentActivity, response.body().getMessage());

                                break;  case AppConstant.MARK_ORDER:

                                UIHelper.showToast(currentActivity, response.body().getMessage());

                                break;


                        }

                    } else {
                        serviceResponseLisener.ResponseFailure(tag);
                        UIHelper.showToast(currentActivity, response.body().getMessage());
                    }
                } else {
//                    serviceResponseLisener.ResponseFailure(tag);
//                    UIHelper.showToast(currentActivity, response.body().getMessage());
                    JSONObject jObjError = null;
                    JSONObject jsonObject = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        jsonObject = jObjError.getJSONObject("error");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    serviceResponseLisener.ResponseFailure(tag);
                    try {
                        if (jsonObject != null) {
                           // if (!tag.equals(AppConstant.QavaShop.FETCH_ADDRESS_BY_MAP)) {
                                UIHelper.showAlertDialog(currentActivity, currentActivity.getResources().getString(R.string.error), jsonObject.getString("message"));
                           // }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                hideLoader();
            }

            @Override
            public void onFailure(@NonNull Call<Api_Response<T>> call, @NonNull Throwable t) {
                hideLoader();
                serviceResponseLisener.ResponseFailure(tag);
                t.printStackTrace();
                Log.e(ServiceHelper.class.getSimpleName() + " by tag: " + tag, t.toString());
            }
        });
    }


}
