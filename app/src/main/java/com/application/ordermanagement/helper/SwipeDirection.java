package com.application.ordermanagement.helper;

/**
 * Created by Addi.
 */
public enum SwipeDirection {
    all, left, right, none ;
}
