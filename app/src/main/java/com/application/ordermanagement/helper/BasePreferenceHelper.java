package com.application.ordermanagement.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.models.Resturant.ResturantResponse;
import com.application.ordermanagement.models.UserModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class BasePreferenceHelper extends PreferenceHelper {


    public static final String USER_TYPE = "type";
    public static final String USER_VENDER = "vendor";
    public static final String USER_BUYER = "user";

    public static final String USER_UNREGISTER = "unregister";
    public static final String KEY_MEMO = "memoList";
    public static final String KEY_CHAT = "chatList";
    public static final String KEY_POST = "postList";
    public static final String image_path = "image_path";
    protected static final String KEY_LOGIN_STATUS = "islogin";
    protected static final String KEY_PRINT_STATUS = "isAutoPrint";
    protected static final String KEY_USER = "user";
    protected static final String KEY_RESTURANT = "resturant";
    public static final String KEY_DEVICE_TOKEN = "device_token";
    public static final String KEY_IP = "ip";
    public static final String KEY_IP_TWO= "ip_two";
    public static final String KEY_IP_THREE = "ip_three";
    public static final String KEY_IP_FOUR = "ip_four";
    public static final String KEY_IP_FIVE = "ip_five";
    public static final String AUTHENTICATE_USER_TOKEN = "user_token";
    public static final String is_verified = "is_verified";
    public static final String opening_time = "opening_time";
    public static final String closing_time = "closing_time";
    public static final String LANGUAGE = "language";

    private Context context;
    private static final String FILENAME = "preferences";

    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public String getIp() {
        return getStringPreference(context, FILENAME, KEY_IP);
    }

    public void setIp(String ip) {
        putStringPreference(context, FILENAME, KEY_IP, ip);
    }


    public String getOpening_time() {
        return getStringPreference(context, FILENAME, opening_time);
    }

    public void setOpening_time(String ip) {
        putStringPreference(context, FILENAME, opening_time, ip);
    }

    public String getClosing_time() {
        return getStringPreference(context, FILENAME, closing_time);
    }

    public void setClosing_time(String ip) {
        putStringPreference(context, FILENAME, closing_time, ip);
    }
////////////////////////////

    public String getIptwo() {
        return getStringPreference(context, FILENAME, KEY_IP_TWO);
    }

    public void setIptwo(String ip) {
        putStringPreference(context, FILENAME, KEY_IP_TWO, ip);
    }
    ////////////////////////////////
    public String getIpthree() {
        return getStringPreference(context, FILENAME, KEY_IP_THREE);
    }

    public void setIpthree(String ip) {
        putStringPreference(context, FILENAME, KEY_IP_THREE, ip);
    }
    ////////////////////////////////
    public String getIpfour() {
        return getStringPreference(context, FILENAME, KEY_IP_FOUR);
    }

    public void setIpfour(String ip) {
        putStringPreference(context, FILENAME, KEY_IP_FOUR, ip);
    }
    ////////////////////////////////
    public String getIpfive() {
        return getStringPreference(context, FILENAME, KEY_IP_FIVE);
    }

    public void setIpfive(String ip) {
        putStringPreference(context, FILENAME, KEY_IP_FIVE, ip);
    }
    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    /*
        public void putPostList(Model value) {

            putStringPreference(context, FILENAME, KEY_POST,
                    new Gson().toJson(value, new TypeToken<Model>() {
                    }.getType()));
        }

        public Model getPostList() {
            Model list = new GsonBuilder().create().fromJson(
                    getStringPreference(context, FILENAME, KEY_POST),
                    new TypeToken<Model>() {
                    }.getType());

            return list == null ? new Model() : list;
        }
        public void putMemoList(ArrayList<MemoItem> value) {

            putStringPreference(context, FILENAME, KEY_MEMO,
                    new Gson().toJson(value, new TypeToken<ArrayList<MemoItem>>() {
                    }.getType()));
        }

        public ArrayList<MemoItem> getMemoList() {
            ArrayList<MemoItem> list = new GsonBuilder().create().fromJson(
                    getStringPreference(context, FILENAME, KEY_MEMO),
                    new TypeToken<ArrayList<MemoItem>>() {
                    }.getType());

            return list == null ? new ArrayList<MemoItem>() : list;
        }


        public void putChatList(ArrayList<ChatItem> value) {

            putStringPreference(context, FILENAME, KEY_CHAT,
                    new Gson().toJson(value, new TypeToken<ArrayList<MemoItem>>() {
                    }.getType()));
        }

        public ArrayList<ChatItem> getChatList() {
            ArrayList<ChatItem> list = new GsonBuilder().create().fromJson(
                    getStringPreference(context, FILENAME, KEY_CHAT),
                    new TypeToken<ArrayList<ChatItem>>() {
                    }.getType());

            return list == null ? new ArrayList<ChatItem>() : list;
        }
    */
    public void setImage_path(String path) {
        putStringPreference(context, FILENAME, image_path, path);
    }

    public String getImage_path() {
        return getStringPreference(context, FILENAME, image_path);
    }

    public void setIs_verified(String path) {
        putStringPreference(context, FILENAME, image_path, path);
    }

    public String getIs_verified() {
        return getStringPreference(context, FILENAME, is_verified);
    }


    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }

    public void setAutoPrintStatus(boolean isAutoPrint) {
        putBooleanPreference(context, FILENAME, KEY_PRINT_STATUS, isAutoPrint);
    }


    public void setStringPrefrence(String key, String value) {
        putStringPreference(context, FILENAME, key, value);
    }

    public String getStringPrefrence(String key) {
        return getStringPreference(context, FILENAME, key);
    }


    public void setIntegerPrefrence(String key, int value) {
        putIntegerPreference(context, FILENAME, key, value);
    }

    public int getIntegerPrefrence(String key) {
        return getIntegerPreference(context, FILENAME, key);
    }


    public void setBooleanPrefrence(String Key, boolean status) {
        putBooleanPreference(context, FILENAME, Key, status);
    }

    public boolean getBooleanPrefrence(String Key) {
        return getBooleanPreference(context, FILENAME, Key);
    }


    public boolean getLoginStatus() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public boolean getAutoPrintStatus() {
        return getBooleanPreference(context, FILENAME, KEY_PRINT_STATUS);
    }

    public boolean getPrintStatus() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public void putDeviceToken(String token) {
        putStringPreference(context, FILENAME, KEY_DEVICE_TOKEN, token);
    }


    public String getDeviceToken() {
        return getStringPreference(context, FILENAME, KEY_DEVICE_TOKEN);
    }


    public void putUserToken(String token) {
        putStringPreference(context, FILENAME, AUTHENTICATE_USER_TOKEN, token);
    }


    public String getUserToken() {
        return getStringPreference(context, FILENAME, AUTHENTICATE_USER_TOKEN);
    }

    public void putUser(UserModel user) {
        putStringPreference(context,
                FILENAME,
                KEY_USER,
                new GsonBuilder()
                        .create()
                        .toJson(user));
    }

    public void putResturant(ResturantResponse.Data user) {
        putStringPreference(context,
                FILENAME,
                KEY_RESTURANT,
                new GsonBuilder()
                        .create()
                        .toJson(user));
    }

    public ResturantResponse.Data getResturant() {
        return new GsonBuilder().create().fromJson(
                getStringPreference(context, FILENAME, KEY_RESTURANT), ResturantResponse.Data.class);
    }

    public UserModel getUser() {
        return new GsonBuilder().create().fromJson(
                getStringPreference(context, FILENAME, KEY_USER), UserModel.class);
    }

    public void removeLoginPreference() {
        setLoginStatus(false);
        removePreference(context, FILENAME, KEY_USER);
        removePreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public static void setDeviceToken(Context context, String token) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.putDeviceToken(token);
    }

    public static void setIPAddress(Context context, String ip) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.setIp(ip);
    }

    public static String getIPAddress(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        return preferenceHelper.getIp();

    }


    ///////////////
    public static void setIPAddresstwo(Context context, String ip) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.setIptwo(ip);
    }

    public static String getIPAddresstwo(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        return preferenceHelper.getIp();

    }
    //////////////////
    public static void setIPAddressthree(Context context, String ip) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.setIpthree(ip);
    }

    public static String getIPAddressthree(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        return preferenceHelper.getIp();

    }
    /////////////////////////
    public static void setIPAddressfour(Context context, String ip) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.setIpfour(ip);
    }

    public static String getIPAddressfour(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        return preferenceHelper.getIp();

    }
    ////////////////////////
    public static void setIPAddressfive(Context context, String ip) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        preferenceHelper.setIpfive(ip);
    }

    public static String getIPAddressfive(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        return preferenceHelper.getIp();

    }
    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    public void putListCategories(String key, ArrayList<OrderPlaceModel> objArray) {
        checkForNullKey(key);
        if (objArray.size() == 0) {
            getSharedPreferences().edit().putString(key, "").apply();
        } else {
            Type listType = new TypeToken<List<OrderPlaceModel>>() {
            }.getType();
            String jsonResponse = new Gson().toJson(objArray, listType);
            getSharedPreferences().edit().putString(key, jsonResponse).apply();
        }
    }
    public ArrayList<OrderPlaceModel> getListCategories(String key) {
        String data = getSharedPreferences().getString(key, "");
        if (data.equals("")) {
            return new ArrayList<>();
        } else {
            Type listType = new TypeToken<List<OrderPlaceModel>>() {
            }.getType();
            List<OrderPlaceModel> list = new Gson().fromJson(data, listType);
            return new ArrayList<OrderPlaceModel>(list);
        }
    }



    public void putPrinters(String key, ArrayList<PrinterResponseList.Datum> objArray) {
        checkForNullKey(key);
        if (objArray.size() == 0) {
            getSharedPreferences().edit().putString(key, "").apply();
        } else {
            Type listType = new TypeToken<List<PrinterResponseList.Datum>>() {
            }.getType();
            String jsonResponse = new Gson().toJson(objArray, listType);
            getSharedPreferences().edit().putString(key, jsonResponse).apply();
        }
    }
    public ArrayList<PrinterResponseList.Datum> getPrinters(String key) {
        String data = getSharedPreferences().getString(key, "");
        if (data.equals("")) {
            return new ArrayList<>();
        } else {
            Type listType = new TypeToken<List<PrinterResponseList.Datum>>() {
            }.getType();
            List<PrinterResponseList.Datum> list = new Gson().fromJson(data, listType);
            return new ArrayList<PrinterResponseList.Datum>(list);
        }
    }

}
