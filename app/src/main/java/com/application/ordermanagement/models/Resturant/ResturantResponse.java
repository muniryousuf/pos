
package com.application.ordermanagement.models.Resturant;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResturantResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
    public class Data {
        public String getmEmail() {
            return mEmail;
        }

        public void setmEmail(String mEmail) {
            this.mEmail = mEmail;
        }

        @SerializedName("email")
        private String mEmail;
        @SerializedName("address")
        private Address mAddress;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_user")
        private Long mIdUser;
        @SerializedName("is_delivery")
        private Long mIsDelivery;
        @SerializedName("is_dine")
        private Long mIsDine;

        public Long getmIsDelivery() {
            return mIsDelivery;
        }

        public void setmIsDelivery(Long mIsDelivery) {
            this.mIsDelivery = mIsDelivery;
        }

        public Long getmIsDine() {
            return mIsDine;
        }

        public void setmIsDine(Long mIsDine) {
            this.mIsDine = mIsDine;
        }

        public Long getmIsPickup() {
            return mIsPickup;
        }

        public void setmIsPickup(Long mIsPickup) {
            this.mIsPickup = mIsPickup;
        }

        @SerializedName("is_pickup")
        private Long mIsPickup;
        @SerializedName("logo")
        private Object mLogo;
        @SerializedName("name")
        private String mName;
        @SerializedName("phone_number")
        private String mPhoneNumber;
        @SerializedName("website_url")
        private String mWebsiteUrl;

        public Address getAddress() {
            return mAddress;
        }

        public void setAddress(Address address) {
            mAddress = address;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdUser() {
            return mIdUser;
        }

        public void setIdUser(Long idUser) {
            mIdUser = idUser;
        }



        public Object getLogo() {
            return mLogo;
        }

        public void setLogo(Object logo) {
            mLogo = logo;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getPhoneNumber() {
            return mPhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            mPhoneNumber = phoneNumber;
        }

        public String getWebsiteUrl() {
            return mWebsiteUrl;
        }

        public void setWebsiteUrl(String websiteUrl) {
            mWebsiteUrl = websiteUrl;
        }

    }
    public class Address {

        @SerializedName("address")
        private String mAddress;
        @SerializedName("city")
        private String mCity;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_country")
        private Long mIdCountry;
        @SerializedName("id_restaurant")
        private Long mIdRestaurant;
        @SerializedName("zip_code")
        private String mZipCode;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public String getCity() {
            return mCity;
        }

        public void setCity(String city) {
            mCity = city;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCountry() {
            return mIdCountry;
        }

        public void setIdCountry(Long idCountry) {
            mIdCountry = idCountry;
        }

        public Long getIdRestaurant() {
            return mIdRestaurant;
        }

        public void setIdRestaurant(Long idRestaurant) {
            mIdRestaurant = idRestaurant;
        }

        public String getZipCode() {
            return mZipCode;
        }

        public void setZipCode(String zipCode) {
            mZipCode = zipCode;
        }

    }
}
