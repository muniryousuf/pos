
package com.application.ordermanagement.models.shopstatus;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("copyright_text")
    private String mCopyrightText;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("currencySign")
    private String mCurrencySign;
    @SerializedName("favicon")
    private String mFavicon;
    @SerializedName("footer_logo")
    private String mFooterLogo;
    @SerializedName("header_logo")
    private String mHeaderLogo;
    @SerializedName("id")
    private Long mId;
    @SerializedName("printer_ip")
    private String mPrinterIp;

    @SerializedName("printer_ip_1")
    private String mPrinterIp1;
    @SerializedName("printer_ip_2")
    private String mPrinterIp2;
    @SerializedName("printer_ip_3")
    private String mPrinterIp3;
    @SerializedName("printer_ip_4")
    private String mPrinterIp4;


    @SerializedName("opening_time")
    private String opening_time;

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    @SerializedName("closing_time")
    private String closing_time;

    public String getmPrinterIp1() {
        return mPrinterIp1;
    }

    public void setmPrinterIp1(String mPrinterIp1) {
        this.mPrinterIp1 = mPrinterIp1;
    }

    public String getmPrinterIp2() {
        return mPrinterIp2;
    }

    public void setmPrinterIp2(String mPrinterIp2) {
        this.mPrinterIp2 = mPrinterIp2;
    }

    public String getmPrinterIp3() {
        return mPrinterIp3;
    }

    public void setmPrinterIp3(String mPrinterIp3) {
        this.mPrinterIp3 = mPrinterIp3;
    }

    public String getmPrinterIp4() {
        return mPrinterIp4;
    }

    public void setmPrinterIp4(String mPrinterIp4) {
        this.mPrinterIp4 = mPrinterIp4;
    }

    public String getmPrinterIp5() {
        return mPrinterIp5;
    }

    public void setmPrinterIp5(String mPrinterIp5) {
        this.mPrinterIp5 = mPrinterIp5;
    }

    @SerializedName("printer_ip_5")
    private String mPrinterIp5;


    @SerializedName("service_charges")
    private Long mServiceCharges;
    @SerializedName("shop_status")
    private Integer mShopStatus;
    @SerializedName("site_name")
    private String mSiteName;
    @SerializedName("site_title")
    private String mSiteTitle;
    @SerializedName("tag_line")
    private String mTagLine;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("Vat")
    private Long mVat;

    public String getCopyrightText() {
        return mCopyrightText;
    }

    public void setCopyrightText(String copyrightText) {
        mCopyrightText = copyrightText;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getCurrencySign() {
        return mCurrencySign;
    }

    public void setCurrencySign(String currencySign) {
        mCurrencySign = currencySign;
    }

    public String getFavicon() {
        return mFavicon;
    }

    public void setFavicon(String favicon) {
        mFavicon = favicon;
    }

    public String getFooterLogo() {
        return mFooterLogo;
    }

    public void setFooterLogo(String footerLogo) {
        mFooterLogo = footerLogo;
    }

    public String getHeaderLogo() {
        return mHeaderLogo;
    }

    public void setHeaderLogo(String headerLogo) {
        mHeaderLogo = headerLogo;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getPrinterIp() {
        return mPrinterIp;
    }

    public void setPrinterIp(String printerIp) {
        mPrinterIp = printerIp;
    }

    public Long getServiceCharges() {
        return mServiceCharges;
    }

    public void setServiceCharges(Long serviceCharges) {
        mServiceCharges = serviceCharges;
    }

    public Integer getShopStatus() {
        return mShopStatus;
    }

    public void setShopStatus(Integer shopStatus) {
        mShopStatus = shopStatus;
    }

    public String getSiteName() {
        return mSiteName;
    }

    public void setSiteName(String siteName) {
        mSiteName = siteName;
    }

    public String getSiteTitle() {
        return mSiteTitle;
    }

    public void setSiteTitle(String siteTitle) {
        mSiteTitle = siteTitle;
    }

    public String getTagLine() {
        return mTagLine;
    }

    public void setTagLine(String tagLine) {
        mTagLine = tagLine;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Long getVat() {
        return mVat;
    }

    public void setVat(Long vat) {
        mVat = vat;
    }

}
