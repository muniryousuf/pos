
package com.application.ordermanagement.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class User {

    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("email_verified_at")
    private Object mEmailVerifiedAt;
    @SerializedName("file_id")
    private Object mFileId;
    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("phone_number")
    private Object mPhoneNumber;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getEmailVerifiedAt() {
        return mEmailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        mEmailVerifiedAt = emailVerifiedAt;
    }

    public Object getFileId() {
        return mFileId;
    }

    public void setFileId(Object fileId) {
        mFileId = fileId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Object getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
