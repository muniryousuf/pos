
package com.application.ordermanagement.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SignInResponse {


    public String getPrinter_ip_1() {
        return printer_ip_1;
    }

    public void setPrinter_ip_1(String printer_ip_1) {
        this.printer_ip_1 = printer_ip_1;
    }

    public String getPrinter_ip_2() {
        return printer_ip_2;
    }

    public void setPrinter_ip_2(String printer_ip_2) {
        this.printer_ip_2 = printer_ip_2;
    }

    public String getPrinter_ip_3() {
        return printer_ip_3;
    }

    public void setPrinter_ip_3(String printer_ip_3) {
        this.printer_ip_3 = printer_ip_3;
    }

    public String getPrinter_ip_4() {
        return printer_ip_4;
    }

    public void setPrinter_ip_4(String printer_ip_4) {
        this.printer_ip_4 = printer_ip_4;
    }

    public String getPrinter_ip_5() {
        return printer_ip_5;
    }

    public void setPrinter_ip_5(String printer_ip_5) {
        this.printer_ip_5 = printer_ip_5;
    }

    @SerializedName("opening_time")
    private String opening_time;

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(String closing_time) {
        this.closing_time = closing_time;
    }

    @SerializedName("closing_time")
    private String closing_time;


    @SerializedName("printer_ip_1")
    private String printer_ip_1;

    @SerializedName("printer_ip_2")
    private String printer_ip_2;

    @SerializedName("printer_ip_3")
    private String printer_ip_3;

    @SerializedName("printer_ip_4")
    private String printer_ip_4;

    @SerializedName("printer_ip_5")
    private String printer_ip_5;


    public int getShop_status() {
        return shop_status;
    }

    public void setShop_status(int shop_status) {
        this.shop_status = shop_status;
    }

    @SerializedName("shop_status")
    private int shop_status;
    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("data")
    private User mUser;
    @SerializedName("message")
    private String mMessage;

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public User getData() {
        return mUser;
    }

    public void setData(User user) {
        mUser = user;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
