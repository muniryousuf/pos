
package com.application.ordermanagement.models.MenuModule;

import com.application.ordermanagement.models.AddOns.AddOnsResponse;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class MenuResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }

    public class Datum {

        public Product getData() {
            return data;
        }

        public void setData(Product data) {
            this.data = data;
        }

        @SerializedName("data")
        private Product data;
        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("id")
        private Long mId;
        @SerializedName("image")
        private String mImage;
        @SerializedName("name")
        private String mName;
        @SerializedName("products")
        private List<Product> mProducts;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getImage() {
            return mImage;
        }

        public void setImage(String image) {
            mImage = image;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public List<Product> getProducts() {
            return mProducts;
        }

        public void setProducts(List<Product> products) {
            mProducts = products;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

    public class Product {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("food_allergy")
        private String mFoodAllergy;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_category")
        private Long mIdCategory;
        @SerializedName("image")
        private Object mImage;
        @SerializedName("image_url")
        private String mImageUrl;
        @SerializedName("name")
        private String mName;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("sizes")
        private List<Size> mSizes;

        public List<Group> getmGroup() {
            return groups;
        }

        public void setmGroup(List<Group> groups) {
            this.groups = groups;
        }

        @SerializedName("groups")
        private List<Group> groups;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public String getFoodAllergy() {
            return mFoodAllergy;
        }

        public void setFoodAllergy(String foodAllergy) {
            mFoodAllergy = foodAllergy;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCategory() {
            return mIdCategory;
        }

        public void setIdCategory(Long idCategory) {
            mIdCategory = idCategory;
        }

        public Object getImage() {
            return mImage;
        }

        public void setImage(Object image) {
            mImage = image;
        }

        public String getImageUrl() {
            return mImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            mImageUrl = imageUrl;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public List<Size> getSizes() {
            return mSizes;
        }

        public void setSizes(List<Size> sizes) {
            mSizes = sizes;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

    public class Group {


        public ArrayList<AddOnsResponse.Choice> getChoices() {
            return choices;
        }

        public void setChoices(ArrayList<AddOnsResponse.Choice> choices) {
            this.choices = choices;
        }

        @SerializedName("choices")
        private ArrayList<AddOnsResponse.Choice> choices;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("updated_at")
        private String mUpdatedAt;
        @SerializedName("name")
        private String name;

        public String getmCreatedAt() {
            return mCreatedAt;
        }

        public void setmCreatedAt(String mCreatedAt) {
            this.mCreatedAt = mCreatedAt;
        }

        public Long getmId() {
            return mId;
        }

        public void setmId(Long mId) {
            this.mId = mId;
        }

        public String getmUpdatedAt() {
            return mUpdatedAt;
        }

        public void setmUpdatedAt(String mUpdatedAt) {
            this.mUpdatedAt = mUpdatedAt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("type")
        private String type;

    }

    public static class Size {
        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        boolean is_checked= false;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_product")
        private Long mIdProduct;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("size")
        private String mSize;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdProduct() {
            return mIdProduct;
        }

        public void setIdProduct(Long idProduct) {
            mIdProduct = idProduct;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getSize() {
            return mSize;
        }

        public void setSize(String size) {
            mSize = size;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class Choice {
        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        boolean is_checked= false;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_group")
        private Long mIdGroup;
        @SerializedName("name")
        private String mName;
        @SerializedName("preselect")
        private Long mPreselect;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdGroup() {
            return mIdGroup;
        }

        public void setIdGroup(Long idGroup) {
            mIdGroup = idGroup;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public Long getPreselect() {
            return mPreselect;
        }

        public void setPreselect(Long preselect) {
            mPreselect = preselect;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
