
package com.application.ordermanagement.models.SalesResponse;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SalesResponseClass {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public class Data {

        @SerializedName("total_sales")
        private String mTotalSales;

        @SerializedName("most_sale_item")
        private String most_sale_item;

        public String getMost_sale_item() {
            return most_sale_item;
        }

        public void setMost_sale_item(String most_sale_item) {
            this.most_sale_item = most_sale_item;
        }

        public String getTota_orders() {
            return tota_orders;
        }

        public void setTota_orders(String tota_orders) {
            this.tota_orders = tota_orders;
        }

        @SerializedName("tota_orders")
        private String tota_orders;

        public String getTotalSales() {
            return mTotalSales;
        }

        public void setTotalSales(String totalSales) {
            mTotalSales = totalSales;
        }

    }
}
