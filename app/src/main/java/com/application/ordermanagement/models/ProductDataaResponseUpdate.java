
package com.application.ordermanagement.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ProductDataaResponseUpdate {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
    public class Data {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_category")
        private Long mIdCategory;
        @SerializedName("image")
        private Object mImage;
        @SerializedName("image_url")
        private String mImageUrl;
        @SerializedName("name")
        private String mName;
        @SerializedName("price")
        private Long mPrice;
        @SerializedName("sizes")
        private List<Object> mSizes;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCategory() {
            return mIdCategory;
        }

        public void setIdCategory(Long idCategory) {
            mIdCategory = idCategory;
        }

        public Object getImage() {
            return mImage;
        }

        public void setImage(Object image) {
            mImage = image;
        }

        public String getImageUrl() {
            return mImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            mImageUrl = imageUrl;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public Long getPrice() {
            return mPrice;
        }

        public void setPrice(Long price) {
            mPrice = price;
        }

        public List<Object> getSizes() {
            return mSizes;
        }

        public void setSizes(List<Object> sizes) {
            mSizes = sizes;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
