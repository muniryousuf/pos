
package com.application.ordermanagement.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class OrderUpdateResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
    public class Data {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("delivery_address")
        private String mDeliveryAddress;
        @SerializedName("delivery_fees")
        private String mDeliveryFees;
        @SerializedName("details")
        private List<Detail> mDetails;
        @SerializedName("id")
        private Long mId;
        @SerializedName("payment")
        private String mPayment;
        @SerializedName("reference")
        private String mReference;
        @SerializedName("status")
        private String mStatus;
        @SerializedName("total_amount_with_fee")
        private Long mTotalAmountWithFee;
        @SerializedName("updated_at")
        private String mUpdatedAt;
        @SerializedName("user_id")
        private Long mUserId;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDeliveryAddress() {
            return mDeliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            mDeliveryAddress = deliveryAddress;
        }

        public String getDeliveryFees() {
            return mDeliveryFees;
        }

        public void setDeliveryFees(String deliveryFees) {
            mDeliveryFees = deliveryFees;
        }

        public List<Detail> getDetails() {
            return mDetails;
        }

        public void setDetails(List<Detail> details) {
            mDetails = details;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getPayment() {
            return mPayment;
        }

        public void setPayment(String payment) {
            mPayment = payment;
        }

        public String getReference() {
            return mReference;
        }

        public void setReference(String reference) {
            mReference = reference;
        }

        public String getStatus() {
            return mStatus;
        }

        public void setStatus(String status) {
            mStatus = status;
        }

        public Long getTotalAmountWithFee() {
            return mTotalAmountWithFee;
        }

        public void setTotalAmountWithFee(Long totalAmountWithFee) {
            mTotalAmountWithFee = totalAmountWithFee;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

        public Long getUserId() {
            return mUserId;
        }

        public void setUserId(Long userId) {
            mUserId = userId;
        }

    }public class Detail {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("extras")
        private String mExtras;
        @SerializedName("id")
        private Long mId;
        @SerializedName("order_id")
        private Long mOrderId;
        @SerializedName("price")
        private Long mPrice;
        @SerializedName("product_id")
        private Long mProductId;
        @SerializedName("product_name")
        private String mProductName;
        @SerializedName("quantity")
        private Long mQuantity;
        @SerializedName("special_instructions")
        private String mSpecialInstructions;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getExtras() {
            return mExtras;
        }

        public void setExtras(String extras) {
            mExtras = extras;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getOrderId() {
            return mOrderId;
        }

        public void setOrderId(Long orderId) {
            mOrderId = orderId;
        }

        public Long getPrice() {
            return mPrice;
        }

        public void setPrice(Long price) {
            mPrice = price;
        }

        public Long getProductId() {
            return mProductId;
        }

        public void setProductId(Long productId) {
            mProductId = productId;
        }

        public String getProductName() {
            return mProductName;
        }

        public void setProductName(String productName) {
            mProductName = productName;
        }

        public Long getQuantity() {
            return mQuantity;
        }

        public void setQuantity(Long quantity) {
            mQuantity = quantity;
        }

        public String getSpecialInstructions() {
            return mSpecialInstructions;
        }

        public void setSpecialInstructions(String specialInstructions) {
            mSpecialInstructions = specialInstructions;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
