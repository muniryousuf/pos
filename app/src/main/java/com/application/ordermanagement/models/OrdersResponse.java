
package com.application.ordermanagement.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrdersResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }
    public class Datum {

        @SerializedName("created_at")
        private String mCreatedAt;

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        @SerializedName("order_type")
        private String order_type;

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        @SerializedName("phone_number")
        private String phone_number;

        public UserData getUser_data() {
            return user_data;
        }

        public void setUser_data(UserData user_data) {
            this.user_data = user_data;
        }

        @SerializedName("user_data")
        private UserData user_data;

        public Float getDiscounted_amount() {
            return discounted_amount;
        }

        public void setDiscounted_amount(Float discounted_amount) {
            this.discounted_amount = discounted_amount;
        }

        @SerializedName("discounted_amount")
        private Float discounted_amount;
        @SerializedName("delivery_address")
        private String mDeliveryAddress;
        @SerializedName("delivery_fees")
        private Float mDeliveryFees;
        @SerializedName("details")
        private List<Detail> mDetails;
        @SerializedName("id")
        private int mId;
        @SerializedName("payment")
        private String mPayment;
        @SerializedName("reference")
        private String mReference;
        @SerializedName("status")
        private String mStatus;
        @SerializedName("total_amount_with_fee")
        private Float mTotalAmountWithFee;
        @SerializedName("updated_at")
        private String mUpdatedAt;
        @SerializedName("user_id")
        private Long mUserId;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDeliveryAddress() {
            return mDeliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            mDeliveryAddress = deliveryAddress;
        }

        public Float getDeliveryFees() {
            return mDeliveryFees;
        }

        public void setDeliveryFees(Float deliveryFees) {
            mDeliveryFees = deliveryFees;
        }

        public List<Detail> getDetails() {
            return mDetails;
        }

        public void setDetails(List<Detail> details) {
            mDetails = details;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        public String getPayment() {
            return mPayment;
        }

        public void setPayment(String payment) {
            mPayment = payment;
        }

        public String getReference() {
            return mReference;
        }

        public void setReference(String reference) {
            mReference = reference;
        }

        public String getStatus() {
            return mStatus;
        }

        public void setStatus(String status) {
            mStatus = status;
        }

        public Float getTotalAmountWithFee() {
            return mTotalAmountWithFee;
        }

        public void setTotalAmountWithFee(Float totalAmountWithFee) {
            mTotalAmountWithFee = totalAmountWithFee;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

        public Long getUserId() {
            return mUserId;
        }

        public void setUserId(Long userId) {
            mUserId = userId;
        }

    }
    public class Detail {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("extras")
        private String mExtras;
        @SerializedName("id")
        private Long mId;
        @SerializedName("order_id")
        private Long mOrderId;
        @SerializedName("price")
        private Float mPrice;
        @SerializedName("product_id")
        private Long mProductId;
        @SerializedName("product_name")
        private String mProductName;
        @SerializedName("quantity")
        private Long mQuantity;
        @SerializedName("special_instructions")
        private String mSpecialInstructions;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getExtras() {
            return mExtras;
        }

        public void setExtras(String extras) {
            mExtras = extras;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getOrderId() {
            return mOrderId;
        }

        public void setOrderId(Long orderId) {
            mOrderId = orderId;
        }

        public Float getPrice() {
            return mPrice;
        }

        public void setPrice(Float price) {
            mPrice = price;
        }

        public Long getProductId() {
            return mProductId;
        }

        public void setProductId(Long productId) {
            mProductId = productId;
        }

        public String getProductName() {
            return mProductName;
        }

        public void setProductName(String productName) {
            mProductName = productName;
        }

        public Long getQuantity() {
            return mQuantity;
        }

        public void setQuantity(Long quantity) {
            mQuantity = quantity;
        }

        public String getSpecialInstructions() {
            return mSpecialInstructions;
        }

        public void setSpecialInstructions(String specialInstructions) {
            mSpecialInstructions = specialInstructions;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class UserData {

        @SerializedName("address")
        private String mAddress;
        @SerializedName("asap")
        private String mAsap;
        @SerializedName("card_holder_name")
        private Object mCardHolderName;
        @SerializedName("card_number")
        private Object mCardNumber;
        @SerializedName("cvc")
        private Object mCvc;
        @SerializedName("deliveryTime")
        private String mDeliveryTime;
        @SerializedName("email")
        private String mEmail;
        @SerializedName("expiration_month")
        private Object mExpirationMonth;
        @SerializedName("expiration_year")
        private Object mExpirationYear;
        @SerializedName("land_mark")
        private Object mLandMark;
        @SerializedName("name")
        private String mName;
        @SerializedName("number")
        private String mNumber;
        @SerializedName("order_type")
        private String mOrderType;
        @SerializedName("payment_type")
        private String mPaymentType;
        @SerializedName("postal_code")
        private String mPostalCode;
        @SerializedName("street")
        private String mStreet;
        @SerializedName("town")
        private String mTown;
        @SerializedName("user_data")
        private UserData mUserData;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public String getAsap() {
            return mAsap;
        }

        public void setAsap(String asap) {
            mAsap = asap;
        }

        public Object getCardHolderName() {
            return mCardHolderName;
        }

        public void setCardHolderName(Object cardHolderName) {
            mCardHolderName = cardHolderName;
        }

        public Object getCardNumber() {
            return mCardNumber;
        }

        public void setCardNumber(Object cardNumber) {
            mCardNumber = cardNumber;
        }

        public Object getCvc() {
            return mCvc;
        }

        public void setCvc(Object cvc) {
            mCvc = cvc;
        }

        public String getDeliveryTime() {
            return mDeliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            mDeliveryTime = deliveryTime;
        }

        public String getEmail() {
            return mEmail;
        }

        public void setEmail(String email) {
            mEmail = email;
        }

        public Object getExpirationMonth() {
            return mExpirationMonth;
        }

        public void setExpirationMonth(Object expirationMonth) {
            mExpirationMonth = expirationMonth;
        }

        public Object getExpirationYear() {
            return mExpirationYear;
        }

        public void setExpirationYear(Object expirationYear) {
            mExpirationYear = expirationYear;
        }

        public Object getLandMark() {
            return mLandMark;
        }

        public void setLandMark(Object landMark) {
            mLandMark = landMark;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String number) {
            mNumber = number;
        }

        public String getOrderType() {
            return mOrderType;
        }

        public void setOrderType(String orderType) {
            mOrderType = orderType;
        }

        public String getPaymentType() {
            return mPaymentType;
        }

        public void setPaymentType(String paymentType) {
            mPaymentType = paymentType;
        }

        public String getPostalCode() {
            return mPostalCode;
        }

        public void setPostalCode(String postalCode) {
            mPostalCode = postalCode;
        }

        public String getStreet() {
            return mStreet;
        }

        public void setStreet(String street) {
            mStreet = street;
        }

        public String getTown() {
            return mTown;
        }

        public void setTown(String town) {
            mTown = town;
        }

        public UserData getUserData() {
            return mUserData;
        }

        public void setUserData(UserData userData) {
            mUserData = userData;
        }

    }

}
