
package com.application.ordermanagement.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductDataResponse {

    @SerializedName("data")
    public List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }
    public class Datum {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("groups")
        private List<Object> mGroups;
        @SerializedName("id")
        private int mId;
        @SerializedName("id_category")
        private Long mIdCategory;
        @SerializedName("image")
        private Object mImage;
        @SerializedName("image_url")
        private String mImageUrl;
        @SerializedName("name")
        private String mName;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("sizes")
        private List<Object> mSizes;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private Object mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public List<Object> getGroups() {
            return mGroups;
        }

        public void setGroups(List<Object> groups) {
            mGroups = groups;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        public Long getIdCategory() {
            return mIdCategory;
        }

        public void setIdCategory(Long idCategory) {
            mIdCategory = idCategory;
        }

        public Object getImage() {
            return mImage;
        }

        public void setImage(Object image) {
            mImage = image;
        }

        public String getImageUrl() {
            return mImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            mImageUrl = imageUrl;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public List<Object> getSizes() {
            return mSizes;
        }

        public void setSizes(List<Object> sizes) {
            mSizes = sizes;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public Object getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
}
