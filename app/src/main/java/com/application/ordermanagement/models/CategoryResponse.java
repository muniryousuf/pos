
package com.application.ordermanagement.models;

import java.util.List;

import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CategoryResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }
    public class Datum {

        public Datum getCategory() {
            return category;
        }

        public void setCategory(Datum category) {
            this.category = category;
        }

        public CategoryResponse.Datum category;
        public boolean isIs_selected() {
            return is_selected;
        }

        public void setIs_selected(boolean is_selected) {
            this.is_selected = is_selected;
        }

        @SerializedName("is_selected")
        private boolean is_selected;
        @SerializedName("created_at")
        private Object mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("id")
        private Long mId;
        @SerializedName("image")
        private String mImage;
        @SerializedName("name")
        private String mName;
        @SerializedName("products")
        private List<Product> mProducts;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private Object mUpdatedAt;

        public Object getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(Object createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getImage() {
            return mImage;
        }

        public void setImage(String image) {
            mImage = image;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public List<Product> getProducts() {
            return mProducts;
        }

        public void setProducts(List<Product> products) {
            mProducts = products;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public Object getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class Product {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_category")
        private Long mIdCategory;
        @SerializedName("image")
        private Object mImage;
        @SerializedName("image_url")
        private String mImageUrl;
        @SerializedName("name")
        private String mName;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("sizes")
        private List<MenuResponse.Size> mSizes;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCategory() {
            return mIdCategory;
        }

        public void setIdCategory(Long idCategory) {
            mIdCategory = idCategory;
        }

        public Object getImage() {
            return mImage;
        }

        public void setImage(Object image) {
            mImage = image;
        }

        public String getImageUrl() {
            return mImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            mImageUrl = imageUrl;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public List<MenuResponse.Size> getSizes() {
            return mSizes;
        }

        public void setSizes(List<MenuResponse.Size> sizes) {
            mSizes = sizes;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
}
