
package com.application.ordermanagement.models.AddOns;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddOnsResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }
    public class Datum {

        @SerializedName("choices")
        private List<Choice> mChoices;
        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("display_type")
        private String mDisplayType;
        @SerializedName("id")
        private Long mId;
        @SerializedName("name")
        private String mName;
        @SerializedName("type")
        private String mType;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public List<Choice> getChoices() {
            return mChoices;
        }

        public void setChoices(List<Choice> choices) {
            mChoices = choices;
        }

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDisplayType() {
            return mDisplayType;
        }

        public void setDisplayType(String displayType) {
            mDisplayType = displayType;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getType() {
            return mType;
        }

        public void setType(String type) {
            mType = type;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class Choice {
        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        boolean is_checked= false;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_group")
        private Long mIdGroup;
        @SerializedName("name")
        private String mName;
        @SerializedName("preselect")
        private Long mPreselect;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdGroup() {
            return mIdGroup;
        }

        public void setIdGroup(Long idGroup) {
            mIdGroup = idGroup;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public Long getPreselect() {
            return mPreselect;
        }

        public void setPreselect(Long preselect) {
            mPreselect = preselect;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
