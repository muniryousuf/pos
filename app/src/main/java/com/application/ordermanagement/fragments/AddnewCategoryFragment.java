package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddnewCategoryFragment extends BaseFragment {
    Unbinder unbinder;

    @BindView(R.id.submmit)
    Button submmit;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.btnUploadPic)
    Button btnUploadPic;
    @BindView(R.id.etname)
    EditText etname;
    @BindView(R.id.etprice)
    EditText etprice;
    @BindView(R.id.status)
    CheckBox status;
    String name;
    MenuResponse.Datum datum;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Menu");

    }

    public static AddnewCategoryFragment newInstance() {
        Bundle args = new Bundle();
        AddnewCategoryFragment fragment = new AddnewCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setdata(MenuResponse.Datum datum) {
        this.datum = datum;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_category, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        submmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etname.getText().toString().length() > 0 && etprice.getText().toString().length() > 0) {
                    if (datum != null) {

                        serviceHelper.enqueueCall(webService.UpdateMenu(datum.getId(), etname.getText().toString(), etprice.getText().toString()), AppConstant.UPDATE_MENU);
                    } else {
                        serviceHelper.enqueueCall(webService.AddMenu(etname.getText().toString(), etprice.getText().toString()), AppConstant.GET_ALL_MENUS);

                    }


                } else {
                    Toast.makeText(mainActivity, "Fields Required", Toast.LENGTH_SHORT).show();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                mainActivity.popFragment();
            }
        });
        btnUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                Toast.makeText(mainActivity, "Implemented later", Toast.LENGTH_SHORT).show();
            }
        });


        if (datum != null) {
            etname.setText(datum.getName());
            etprice.setText(datum.getDescription() != null ? datum.getDescription() : "");
            submmit.setText("Update");
            status.setChecked(datum.getStatus()==1 ?true:false);
        }
        return view;
    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.UPDATE_MENU:
                mainActivity.popFragment();
                break;
            case AppConstant.GET_ALL_MENUS:
                mainActivity.popFragment();
//                MenuResponse menuResponse = (MenuResponse) result;
//               // EventBus.getDefault().post(etname.getText().toString());
//                EventBus.getDefault().post(menuResponse);
                break;


        }

    }


}