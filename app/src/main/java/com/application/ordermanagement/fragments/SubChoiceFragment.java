package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.GetChoice.GetChoiceResponse;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SubChoiceFragment extends BaseFragment {
    Unbinder unbinder;

    @BindView(R.id.submmit)
    Button submmit;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.etname)
    EditText etname;
    @BindView(R.id.etprice)
    EditText etprice;
    @BindView(R.id.swtich)
    Switch swtich;
    String name;

    int isCheck = 0;
    String id ;
    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Choices and Adons");

    }

    public static SubChoiceFragment newInstance() {
        Bundle args = new Bundle();
        SubChoiceFragment fragment = new SubChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }
    GetChoiceResponse.Datum choice;
    public  void setdata(
            GetChoiceResponse.Datum choice){
        this.choice=choice;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_choice, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        if(choice!=null){
            etname.setText(choice.getName());
            etprice.setText(choice.getPrice());
            swtich.setChecked(choice.getPreselect()==1?true:false);
            if (swtich.isChecked()) {
                isCheck = 1;
            } else {
                isCheck = 0;
            }
        }
        swtich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    isCheck = 1;
                } else {
                    isCheck = 0;
                }

            }
        });
        submmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etname.getText().toString().length() > 0 && etprice.getText().toString().length() > 0) {
                    UIHelper.hideSoftKeyboards(mainActivity);
                    if(choice!=null){
                        serviceHelper.enqueueCall(webService.UpdateChoiceToGroup(choice.getId(),etname.getText().toString(), etprice.getText().toString(), isCheck,Integer.parseInt(id)), AppConstant.UPDATE_CHOICE_TO_GROUP);

                    }else{
                        serviceHelper.enqueueCall(webService.AddChoiceToGroup(etname.getText().toString(), etprice.getText().toString(), isCheck,Integer.parseInt(id)), AppConstant.ADD_CHOICE_TO_GROUP);

                    }
                    //EventBus.getDefault().post(etname.getText().toString());

                } else {
                    Toast.makeText(mainActivity, "Fields Required", Toast.LENGTH_SHORT).show();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                mainActivity.popFragment();
            }
        });

        return view;
    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.ADD_CHOICE_TO_GROUP:
                mainActivity.popFragment();
                break;
            case AppConstant.UPDATE_CHOICE_TO_GROUP:
                mainActivity.popFragment();
                break;
        }

    }


    public void setidGroup(String id) {
        this.id =id;
    }
}