package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.HoldItemAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public  class HoldItemsFragment extends BaseFragment implements HoldItemAdapter.onClickHold{
    Unbinder unbinder;
    @BindView(R.id.rvCart)
    RecyclerView rvCart;


    HoldItemAdapter holdItemAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hold_items, container, false);
        unbinder = ButterKnife.bind(this, view);

        holdItemAdapter =new HoldItemAdapter(mainActivity,this);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(holdItemAdapter);
        holdItemAdapter.addAll(preferenceHelper.getListCategories("holditems"));
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
    }

    @Override
    protected void setTitle(Titlebar titlebar) {

    }

    @Override
    public void clickHold(OrderPlaceModel orderDetail) {

        ViewHoldCartFragment viewHoldCartFragment= new ViewHoldCartFragment();
        viewHoldCartFragment.setdata(orderDetail);
        mainActivity.addFragment(viewHoldCartFragment,true,true);
    }
}
