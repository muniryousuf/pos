package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.TableAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.TableResponseModel.TablesResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TabelReservationFragment extends BaseFragment implements TableAdapter.onClickHold {
    Unbinder unbinder;


    @BindView(R.id.rvTable)
    RecyclerView rvTable;
    @BindView(R.id.addTable)
    TextView addTable;


    TableAdapter tableAdapter;

    @Override
    protected void setTitle(Titlebar titlebar) {
        //titlebar.setVisibility(View.VISIBLE);
        // titlebar.setTitle("Dashboard");
        titlebar.hideTitlebar();
        titlebar.hideBackbutton(mainActivity);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabel_reservation, container, false);
        unbinder = ButterKnife.bind(this, view);
        tableAdapter = new TableAdapter(mainActivity, this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(4, LinearLayoutManager.VERTICAL);
        rvTable.setItemAnimator(new DefaultItemAnimator());
        rvTable.setLayoutManager(staggeredGridLayoutManager);
        rvTable.setAdapter(tableAdapter);
        getData();
        addTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdonDialog(mainActivity);

            }
        });
        return view;
    }

    private void getData() {
        serviceHelper.enqueueCall(webService.getTables(), AppConstant.GET_TABLES);
    }


    public class Request {

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        String name;

        public int getIs_reserve() {
            return is_reserve;
        }

        public void setIs_reserve(int is_reserve) {
            this.is_reserve = is_reserve;
        }

        int is_reserve;
    }

    private void addTable(String s) {
        Request request = new Request();
        request.setName(s);
        request.setIs_reserve(0);
        serviceHelper.enqueueCall(webService.addTable(request), AppConstant.ADD_TABLE);
    }

    public void changestatus(int id, String name, int state) {
        Request request = new Request();
        //request.setName(name);
        request.setIs_reserve(state == 1 ? 0 : 1);
        serviceHelper.enqueueCall(webService.changeStatusofTable(request, id), AppConstant.EDIT_TABLE);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.GET_TABLES:
                TablesResponse tablesResponse = (TablesResponse) result;
                tableAdapter.addAll(tablesResponse.getData());

                break;
            case AppConstant.ADD_TABLE:
                getData();
                break;
            case AppConstant.EDIT_TABLE:
                getData();
                break;

        }
    }


    @Override
    public void onCickTable(int position) {
        POSFragment menuListingFragment = new POSFragment();
        menuListingFragment.setPos(true);
        menuListingFragment.setPostalCodeandPhoneNumber("", "", "", "Eat in");
        mainActivity.addFragment(menuListingFragment, true, true);
    }

    @Override
    public void onEdit(TablesResponse.Datum datum) {
        changestatus(datum.getId(), datum.getName(), datum.getIs_reserve());
    }

    public void showAdonDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_add_table);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        EditText name = (EditText) dialog.findViewById(R.id.name);
        TextView submit = (TextView) dialog.findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().length() > 0) {
                    dialog.dismiss();
                    UIHelper.hideSoftKeyboards(mainActivity);

                    addTable(name.getText().toString());
                } else {
                    UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Enter table name");
                }

            }
        });


        dialog.show();

    }

}
