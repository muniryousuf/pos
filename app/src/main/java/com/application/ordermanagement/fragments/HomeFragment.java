package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.BuildConfig;
import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.CheckoutAddonAdapter;
import com.application.ordermanagement.adapter.POS.SizesAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.PrinterCommands;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.model.ResponseForSeperateModels.SingleProductResponse;
import com.application.ordermanagement.models.Resturant.ResturantResponse;
import com.application.ordermanagement.models.shopstatus.ShopStatusResponse;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeFragment extends BaseFragment {
    Unbinder unbinder;


    @BindView(R.id.mainGrid)
    GridLayout mainGrid;
    @BindView(R.id.textGrid)
    TextView textGrid;
    @BindView(R.id.swShop)
    Switch swShop;
    @BindView(R.id.tvLogout)
    TextView tvLogout;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.VISIBLE);
        titlebar.setTitle("Dashboard");
        titlebar.hideTitlebar();
        titlebar.hideBackbutton(mainActivity);
    }

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    BroadcastReceiver receiver;
    IntentFilter filter;
    boolean is_data_loading = false;

    @Override
    public void onResume() {
        super.onResume();
      //  filter = new IntentFilter(BuildConfig.APPLICATION_ID);
       // mainActivity.registerReceiver(receiver, filter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

//        receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                String id = intent.getExtras().getString("id");
//                String total_amount = intent.getExtras().getString("total_amount");
//                String reference = intent.getExtras().getString("reference");
//                String order_typo = intent.getExtras().getString("order_typo");
//                String paymento = intent.getExtras().getString("paymento");
//
//                show(id, total_amount, reference, order_typo, paymento);
//
//
//            }
//        };

        mainGrid.setAlpha(0f);
        mainGrid.setVisibility(View.VISIBLE);

// Animate the content view to 100% opacity, and clear any animation
// listener set on the view.
        mainGrid.animate()
                .alpha(1f)
                .setDuration(2000)
                .setListener(null);

        //Set Event
        setSingleEvent(mainGrid);// Create an object of CustomAdapter and set Adapter to GirdView
        textGrid.setText("Welcome " + preferenceHelper.getUser().getFullName());
        swShop.setChecked(preferenceHelper.getIntegerPrefrence("shop_status") == 1 ? true : false);

        swShop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                serviceHelper.enqueueCall(webService.updateShopstatus(preferenceHelper.getIntegerPrefrence("shop_status") == 1 ? 0 : 1), AppConstant.SHOP_STATUS);

            }
        });
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preferenceHelper.putUser(null);
                preferenceHelper.setLoginStatus(false);
                preferenceHelper.setIp(null);
                mainActivity.clearBackStack();
                mainActivity.addFragment(new LoginFragment(), false, false);
                Toast.makeText(mainActivity, "Logged out Successfully", Toast.LENGTH_SHORT).show();
            }
        });
        // if (preferenceHelper.getLoginStatus()) {
        getResturant();
        serviceHelper.enqueueCall(webService.getPrinters(), AppConstant.GET_PRINTERS);



        //}

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
      //  mainActivity.unregisterReceiver(receiver);

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.SHOP_STATUS:
                ShopStatusResponse shopStatusResponse = (ShopStatusResponse) result;
                preferenceHelper.setIntegerPrefrence("shop_status", shopStatusResponse.getData().getShopStatus());
                swShop.setChecked(preferenceHelper.getIntegerPrefrence("shop_status") == 1 ? true : false);
                break;

            case AppConstant.GET_RESTURANT:
                ResturantResponse resturantResponse = (ResturantResponse) result;
                preferenceHelper.putResturant(resturantResponse.getData());
                break;
            case AppConstant.GET_PRINTERS:
                PrinterResponseList responseBody = (PrinterResponseList) result;
                if(preferenceHelper.getPrinters("printers")!=null && preferenceHelper.getPrinters("printers").size()>0){
                    preferenceHelper.getPrinters("printers").clear();
                    preferenceHelper.putPrinters("printers",responseBody.getData());
                }else{
                    preferenceHelper.putPrinters("printers",responseBody.getData());
                }
                break;


        }


    }

    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    switch (finalI) {
                        case 0:
                            mainActivity.replaceFragment(new OrderListingFragment(), true, true);
                            break;
//                        case 1:
//                            mainActivity.replaceFragment(new ProductListingFragment(), true, true);
//
//                            break;
                        case 1:
                            mainActivity.replaceFragment(new ReportFragment(), true, true);

                            break;
                        case 2:

                            break;
                        case 3:
                            mainActivity.replaceFragment(new GeneralSettings(), true, true);

                            break;
                        case 4:
                            mainActivity.replaceFragment(new DealsFragment(), true, true);
                            break;
                        case 5:
                            mainActivity.replaceFragment(new MenuListingFragment(), true, true);

                            break;
                        case 6:
                            mainActivity.replaceFragment(new AddChoicesListingFragment(), true, true);

                            break;
                        case 7:
                            UIHelper.hideSoftKeyboards(mainActivity);
                            SelectionScreenFragment selectionScreen =new SelectionScreenFragment();
                            mainActivity.addFragment(selectionScreen, true, true);


//                            POSFragment menuListingFragment = new POSFragment();
//                            menuListingFragment.setPos(true);
//                            mainActivity.addFragment(menuListingFragment, true, true);
                            break;
                        case 9:
                            break;
                        case 10:
                            break;
                        case 11:
                            break;


                    }

                }
            });
        }
    }

    public void getResturant() {

        serviceHelper.enqueueCall(webService.getResturant(), AppConstant.GET_RESTURANT);
    }

    public void show(String id, String total_amount, String reference, String order_typo, String paymento) {
        Dialog dialog = new Dialog(mainActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView tvTotalAmount = (TextView) dialog.findViewById(R.id.tvTotalAmount);
        TextView tvOrderId = (TextView) dialog.findViewById(R.id.tvOrderId);
        TextView tvOrderType = (TextView) dialog.findViewById(R.id.tvOrderType);
        TextView tvPayment = (TextView) dialog.findViewById(R.id.tvPayment);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);
        tvTotalAmount.setText(" £ " + Math.round(Double.valueOf(total_amount)));
        tvOrderId.setText(id);
        tvOrderType.setText(order_typo);
        tvPayment.setText(paymento);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                is_accepted = true;
                callapi(String.valueOf(id), "Accepted");
//                mainActivity.clearBackStack();
//                replaceFragment(new HomeFragment(), true, true);
//                replaceFragment(new OrderListingFragment(), true, true);

            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                is_accepted = false;
                callapi(String.valueOf(id), "Rejected");
//                mainActivity.clearBackStack();
//                replaceFragment(new HomeFragment(), true, true);
//                replaceFragment(new OrderListingFragment(), true, true);

            }
        });
        final MediaPlayer mp = MediaPlayer.create(mainActivity, R.raw.notification);
        mp.start();
        dialog.getWindow().getAttributes().windowAnimations = R.style.topAnimation;
        dialog.show();

    }

    public class ProductLisitng {
        public ProductLisitng(String productname, Float price, String quantity, String extra, String special_instructions) {
            this.productname = productname;
            this.extra = extra;
            this.price = price;
            this.quantity = quantity;
            this.special_instructions = special_instructions;
        }

        String productname;

        public String getExtra() {
            return extra;
        }

        public void setExtra(String extra) {
            this.extra = extra;
        }

        String extra;

        public String getSpecial_instructions() {
            return special_instructions;
        }

        public void setSpecial_instructions(String special_instructions) {
            this.special_instructions = special_instructions;
        }

        String special_instructions;
        Float price;

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        String quantity;

    }

    ArrayList<ProductLisitng> details;
    String id, delivery_address, phone_number, payment, order_type, user_id;
    Float total_amount_with_fee, delivery_fees;
    Float discounted_amount;
    boolean is_accepted = true;

    public void callapi(String order_id, String status) {
        details = new ArrayList<>();

        String url = AppConstant.BASE_URL + "update-order/" + order_id;


        RequestBody formBody = new FormBody.Builder()
                .add("status", status)
                .build();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .put(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                if (response.code() == 200) {
                    if (!is_accepted) {

                    } else {
                        String responseString = response.body().string();
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(responseString);
                            JSONObject jsonObject = obj.getJSONObject("data");
                            id = jsonObject.getString("id");
                            user_id = jsonObject.getString("user_id");
                            payment = jsonObject.getString("payment");
                            delivery_address = jsonObject.getString("delivery_address");
                            phone_number = jsonObject.getString("phone_number");
                            total_amount_with_fee = Float.valueOf(jsonObject.getString("total_amount_with_fee"));
                            discounted_amount = Float.valueOf(jsonObject.getString("discounted_amount"));
                            delivery_fees = Float.valueOf(jsonObject.getString("delivery_fees"));
                            if (jsonObject.getString("order_type") != null) {
                                order_type = jsonObject.getString("order_type");

                            }
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String product_name = object.getString("product_name");
                                Float price = Float.valueOf(object.getString("price"));
                                String quantity = object.getString("quantity");
                                String extras = object.getString("extras");
                                String special_instruction = object.getString("special_instructions");


                                details.add(new ProductLisitng(product_name, price, quantity, extras, special_instruction != null ? special_instruction : ""));


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        //   OrderUpdateResponse orderUpdateResponse =(OrderUpdateResponse) response.body();
                        if (mainActivity.prefHelper.getIp() != null) {
                            Log.v("Printer Printing", "Printer one");
                            for (int j = 0; j < 2; j++) {
                                printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, preferenceHelper.getIp(), user_id, discounted_amount, phone_number, 1);
                            }
                        }

                    }


                }

            }


            response.body().close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static OutputStream outputStream;

    public void printSlip(ArrayList<ProductLisitng> details,
                          String id, String delivery_address,
                          Float delivery_fees, Float total_amount_with_fee,
                          String pay, String order_type, String ip, String user_id, Float discounted_amount, String phone_number, int i) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(preferenceHelper.getResturant().getName(), 3, 1);
            printNewLine();
            //printCustom("Pizza and Fish and Chips Specialists", 0, 1);
            printCustom(preferenceHelper.getResturant().getAddress().getAddress(), 0, 1);
            printCustom("Phone No : " + preferenceHelper.getResturant().getPhoneNumber(), 0, 1);
            printCustom("Post Code : " + preferenceHelper.getResturant().getAddress().getZipCode(), 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printCustom(order_type, 2, 1);
            printNewLine();
            if (order_type != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " + dateTime[1], 3, 1);

            }
            printCustom("Order Number : 000" + id/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            if(!order_type.equalsIgnoreCase("Pickup")){
                printCustom("Restaurant Notes ", 0, 0);
                printCustom("Contact Free Delivery", 3, 0);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            }


            printNewLine();
            for (int j = 0; j < details.size(); j++) {
                printCustom(padLine(details.get(j).getQuantity() + " x " + details.get(j).getProductname(), " £ " + details.get(j).getPrice(), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(details.get(j).getExtra());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (details.get(j).getSpecial_instructions() != null && !details.get(j).getSpecial_instructions().equals("") && !details.get(j).getSpecial_instructions().equals("null")) {

                        spanny.append(" **" + details.get(j).getSpecial_instructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }


            }

          //  printCustom(padLine("Food and drink total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);

            printCustom(padLine("Delivery Fee", delivery_fees > 0 ? "£" + delivery_fees + "/=" : "   Free", 45) + "\n", 1, 0);
            if (discounted_amount > 0) {

                printCustom(padLine("Discount", "- £" + discounted_amount + "/=", 45) + "\n", 1, 0);


            }
            printCustom(padLine("Total", "£" + total_amount_with_fee + "/=", 45) + "\n", 1, 0);
            printNewLine();
            printNewLine();
            if(!payment.equalsIgnoreCase("cod")){
                printCustom("Paid By :", 0, 0);
            }
            printCustom(padLine(payment.equalsIgnoreCase("cod") ? "Cash on Delivery" : "Card : xxxxxxxxxxxxxx", "£" + total_amount_with_fee + "/=", 45) + "\n", 1, 0);


            if (!payment.equalsIgnoreCase("cod")) {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER HAS BEEN PAID", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                if (!payment.equalsIgnoreCase("cod")) {
                    printCustom("PAID ONLINE ", 2, 1);

                }
            } else {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("CASH ON DELIVERY ", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER NOT PAID", 2, 1);

            }
            printCustom("Please Check Notes for Instructions ", 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);

            printNewLine();
            printCustom("Customer ID: ", 0, 0);
            printCustom("" + user_id, 2, 0);
            printNewLine();
            printCustom("Customer details: ", 0, 0);
            printCustom(delivery_address, 2, 0);
            printNewLine();
            printCustom("To Contact Customer Call: ", 0, 0);
            printCustom(phone_number, 2, 0);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();

            if (i == 1) {
                if (preferenceHelper.getIptwo() != null) {
                    Log.v("Printer Printing", "Printer two");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, preferenceHelper.getIptwo(), user_id, discounted_amount, phone_number, 2);
                } else {
                    return;
                }


            }
            if (i == 2) {
                if (preferenceHelper.getIpthree() != null) {
                    Log.v("Printer Printing", "Printer three");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, preferenceHelper.getIpthree(), user_id, discounted_amount, phone_number, 3);
                } else {
                    return;
                }


            }

            if (i == 3) {
                if (preferenceHelper.getIpfour() != null) {
                    Log.v("Printer Printing", "Printer four");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, preferenceHelper.getIpfour(), user_id, discounted_amount, phone_number, 4);
                } else {
                    return;
                }


            }
            if (i == 4) {
                if (preferenceHelper.getIpfive() != null) {
                    Log.v("Printer Printing", "Printer five");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, preferenceHelper.getIpfive(), user_id, discounted_amount, phone_number, -1);
                } else {
                    return;
                }


            }


//            if(i==-1){
//                return;
//            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    protected String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine) {
        if (partOne == null) {
            partOne = "";
        }
        if (partTwo == null) {
            partTwo = "";
        }
        String concat;
        if ((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + " " + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(" ", padding) + partTwo;
        }
        return concat;
    }

    /**
     * utility: string repeat
     */
    protected String repeat(String str, int i) {
        return new String(new char[i]).replace("\0", str);
    }


    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes("Cp858"));
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print photo

    //print unicode


    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print text

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime[] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }
}
