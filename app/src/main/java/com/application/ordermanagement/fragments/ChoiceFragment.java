package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.AdonChoiceAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.GetChoice.GetChoiceResponse;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChoiceFragment extends BaseFragment implements AdonChoiceAdapter.ISubCategoryProductAdapter {
    Unbinder unbinder;
    AdonChoiceAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;
    @BindView(R.id.addchoices)
    LinearLayout addchoices;
    @BindView(R.id.tvChoice)
    TextView tvChoice;
    ArrayList<AddOnsResponse.Choice> items;
    AddOnsResponse.Datum name;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Choices and Adons");

    }

    public static ChoiceFragment newInstance() {
        Bundle args = new Bundle();
        ChoiceFragment fragment = new ChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setname(AddOnsResponse.Datum name) {

        this.name = name;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choices, container, false);
        unbinder = ButterKnife.bind(this, view);
        items = new ArrayList<>();
        // Create an object of CustomAdapter and set Adapter to GirdView
        tvChoice.setText("Add Choice to " + name.getName());
        productListingAdapter = new AdonChoiceAdapter(this, mainActivity, true);
        rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvProducts.setAdapter(productListingAdapter);
        setData();
        addchoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubChoiceFragment subChoiceFragment = new SubChoiceFragment();
                subChoiceFragment.setidGroup(String.valueOf(name.getId()));
                mainActivity.replaceFragment(subChoiceFragment, true, true);
            }
        });
        return view;
    }

    private void setData() {
        serviceHelper.enqueueCall(webService.getChoice(name.getId()), AppConstant.GET_CHOICES);

        //  items.addAll(name.getChoices());
        //   productListingAdapter.addAll(name.getChoices());


    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.DELETE_CHOICE:


                break;
            case AppConstant.GET_CHOICES:
                GetChoiceResponse getChoiceResponse = (GetChoiceResponse) result;
                productListingAdapter.addAll(getChoiceResponse.getData());

                break;
        }

    }


//    @Override
//    public void onClick(String position) {
//        ChoiceFragment choiceFragment = new ChoiceFragment();
//        choiceFragment.setname(position);
//        mainActivity.replaceFragment(choiceFragment, true, true);
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String name) {
        UIHelper.hideSoftKeyboards(mainActivity);
        UIHelper.showAlertDialog(mainActivity, getString(R.string.success), name + " Added Successfully");
        mainActivity.popFragment();
        //  productListingAdapter.add(name);
    }

    @Override
    public void onClickAddMenu(GetChoiceResponse.Datum position) {

    }

    @Override
    public void onCLickDelete(GetChoiceResponse.Datum position) {

        serviceHelper.enqueueCall(webService.deleteChoice(position.getId()), AppConstant.DELETE_CHOICE);

    }


    @Override
    public void onCLickEdit(GetChoiceResponse.Datum position) {
        SubChoiceFragment subChoiceFragment = new SubChoiceFragment();
        subChoiceFragment.setidGroup(String.valueOf(name.getId()));
        subChoiceFragment.setdata(position);
        mainActivity.replaceFragment(subChoiceFragment, true, true);
    }
}