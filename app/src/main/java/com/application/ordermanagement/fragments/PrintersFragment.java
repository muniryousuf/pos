package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.PrintersAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.helper.Utils;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.model.Printers.PrintersResponse;
import com.application.ordermanagement.model.ResturantInfoBodyRequest;
import com.application.ordermanagement.models.Resturant.ResturantResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PrintersFragment extends BaseFragment implements PrintersAdapter.onClickPrinter{
    Unbinder unbinder;

    @BindView(R.id.lladdprinters)
    LinearLayout lladdprinters;
    @BindView(R.id.rvPrinters)
    RecyclerView rvPrinters;
    PrintersAdapter printersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_printers, container, false);
        unbinder = ButterKnife.bind(this, view);

        lladdprinters.setOnClickListener(v -> {

            mainActivity.replaceFragment(new AddPrintersFragment(),true,true);
        });
        getPrinters();

        return view;
    }

    private void getPrinters() {
        printersAdapter =new PrintersAdapter(mainActivity,this);
        rvPrinters.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvPrinters.setAdapter(printersAdapter);
        serviceHelper.enqueueCall(webService.getPrinters(), AppConstant.GET_PRINTERS);

    }

    @Override
    protected void setTitle(Titlebar titlebar) {

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.GET_PRINTERS:
                PrinterResponseList responseBody = (PrinterResponseList) result;
                printersAdapter.addAll(responseBody.getData());
               // mainActivity.prefHelper.putResturant(responseBody.getData());
                break;
            case AppConstant.DELETE_PRINTERS:
                getPrinters();
                break;

        }
    }

    @Override
    public void onClickPrinter(PrinterResponseList.Datum datum) {

    }
    @Override
    public void onClickPrinterDelete(PrinterResponseList.Datum datum) {
        serviceHelper.enqueueCall(webService.addprinterdelete(datum.getId()),AppConstant.DELETE_PRINTERS);
    }

    @Override
    public void onEditPrinters(PrinterResponseList.Datum datum) {
        AddPrintersFragment addPrintersFragment =new AddPrintersFragment();
        addPrintersFragment.setdata(datum,true);
        mainActivity.replaceFragment(addPrintersFragment,true,true);
    }
}
