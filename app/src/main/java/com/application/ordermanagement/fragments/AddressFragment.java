package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.model.PlaceOrderResponse.PlaceOrderDataResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AddressFragment extends BaseFragment {

    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    @BindView(R.id.llCreditCard)
    LinearLayout llCreditCard;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.number)
    EditText number;
    @BindView(R.id.email)
    EditText email;


    Unbinder unbinder;


    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.hideTitlebar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {

                    case R.id.rbtnCC:
                        llCreditCard.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rbtnCod:
                        llCreditCard.setVisibility(View.GONE);
                        break;
                }
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().equals("")  || number.getText().equals("") || email.getText(). equals("")){
                    UIHelper.showAlertDialog(mainActivity,getString(R.string.success),"Fields Required");
                }else{
                    OrderPlaceModel.UserData userData =new OrderPlaceModel.UserData();
                    userData.setAddress(null);
                    userData.setName(name.getText().toString());
                    userData.setNumber(number.getText().toString());
                    userData.setEmail(email.getText().toString());
                    userData.setAsap("As soon as possible");
                    userData.setPaymentType("COD");
                    userData.setOrderType("Pickup");
                    orderPlaceModel.setDeliveryFees("0");
                    orderPlaceModel.setUserData(userData);
                    orderPlaceModel.setUserId(11);
                    orderPlaceModel.setOrderType("Pickup");
                    orderPlaceModel.setDeliveryAddress("--- --- ---");
                   // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");

                   // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
                    //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
                    serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);

                }

                //  UIHelper.showAlertDialog(mainActivity, "Success", "Order placed successfully");

            }
        });
        return view;
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {

        switch (Tag) {

            case AppConstant.Place_order:
                PlaceOrderDataResponse placeOrderDataResponse = (PlaceOrderDataResponse) result;
                UIHelper.showAlertDialog(mainActivity, "Success", "your order has been placed successfully");
                mainActivity.clearBackStack();
                mainActivity.addFragment(new HomeFragment(), true, true);
                break;

        }

    }

    OrderPlaceModel orderPlaceModel;

    public void setdata(OrderPlaceModel orderPlaceModel) {
        this.orderPlaceModel = orderPlaceModel;
    }
}
