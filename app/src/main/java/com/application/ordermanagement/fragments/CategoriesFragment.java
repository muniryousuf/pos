package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.CategoryListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.models.CategoryResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CategoriesFragment extends BaseFragment {

    Unbinder unbinder;
    CategoryListingAdapter categoryListingAdapter;
    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    @BindView(R.id.nocategoryfound)
    TextView nocategoryfound;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Categories");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        categoryListingAdapter = new CategoryListingAdapter(mainActivity);

        rvCategory.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCategory.setAdapter(categoryListingAdapter);

        serviceHelper.enqueueCall(webService.getAllCategory(), AppConstant.ALL_CATEGORYS);
        return view;
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.ALL_CATEGORYS:
                CategoryResponse categoryResponse = (CategoryResponse) result;
                if (categoryResponse.getData().size() > 0) {

                    categoryListingAdapter.addAll(categoryResponse.getData());
                    nocategoryfound.setVisibility(View.GONE);
                } else {
                    rvCategory.setVisibility(View.GONE);
                    nocategoryfound.setVisibility(View.VISIBLE);

                }
                break;

        }

    }
}
