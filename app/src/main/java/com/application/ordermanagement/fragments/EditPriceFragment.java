package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.UpdatePriceAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.Sizes;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class EditPriceFragment extends BaseFragment implements UpdatePriceAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;

    int id;
    List<MenuResponse.Size> sizes;
    UpdatePriceAdapter updatePriceAdapter;
    @BindView(R.id.rvPrices)
    RecyclerView rvPrices;
    @BindView(R.id.btnupdatePrice)
    Button btnupdatePrice;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.VISIBLE);
        titlebar.setTitle("Dashboard");
        titlebar.showBackButton(mainActivity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_editprice, container, false);
        unbinder = ButterKnife.bind(this, view);
        updatePriceAdapter = new UpdatePriceAdapter(this, mainActivity, this, true);
        rvPrices.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvPrices.setAdapter(updatePriceAdapter);
        btnupdatePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updatePriceAdapter.getSizeArray();

            }
        });
        updatePriceAdapter.addAll(sizes);
        updatePriceAdapter.notifyDataSetChanged();
        return view;
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.UPDATE_PRODUCTS:
                mainActivity.popFragment();
                break;

        }


    }


    @Override
    public void size(ArrayList<MenuResponse.Size> datum) {
        Sizes sizes = new Sizes();
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < datum.size(); i++) {
            if(datum.get(i).getSize().equals("LARGE")){
                map.put("Large",datum.get(i).getPrice());


            }if(datum.get(i).getSize().equals("REGULAR")){
                map.put("Regular", datum.get(i).getPrice());


            }if(datum.get(i).getSize().equals("SMALL")){
                map.put("Small", datum.get(i).getPrice());


            }
        }
        sizes.setMap(map);
        String size = new Gson().toJson(sizes);

        Map<String,Object> object =new HashMap<>();
        //object.put("price","0");
        object.put("size",size);
        serviceHelper.enqueueCall(webService.updateproduct(id, object,0), AppConstant.UPDATE_PRODUCTS);

    }

    @Override
    public void delete(MenuResponse.Size datum, int position) {

    }

    @Override
    public void onPriceUpdate(String price, int position) {

    }

    @Override
    public void retryPageLoad() {

    }

    public void setdata(int id, List<MenuResponse.Size> sizes) {
        this.id = id;
        this.sizes = sizes;

    }
}
