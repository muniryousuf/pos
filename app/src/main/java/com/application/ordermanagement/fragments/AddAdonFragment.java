package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.AddChoicesListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddAdonFragment extends BaseFragment implements AddChoicesListingAdapter.ISubCategoryProductAdapter {
    Unbinder unbinder;
    AddChoicesListingAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;


    String id;
    List<MenuResponse.Group> groups;

    public void id(String id, List<MenuResponse.Group> groups) {

        this.id = id;
        this.groups = groups;
    }

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Add Choices");

    }

    public static AddAdonFragment newInstance() {
        Bundle args = new Bundle();
        AddAdonFragment fragment = new AddAdonFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adon_choices_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        productListingAdapter = new AddChoicesListingAdapter(this, mainActivity, true,b);
        rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvProducts.setAdapter(productListingAdapter);
        setData();

        return view;
    }

    private void setData() {

        serviceHelper.enqueueCall(webService.getAdons(), AppConstant.GET_ADONS);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.GET_ADONS:
                AddOnsResponse addOnsResponse = (AddOnsResponse) result;

                if(groups !=null && groups.size()>0){
                    productListingAdapter.addAll(addOnsResponse.getData());


                    for(int j =0;j <groups.size();j++){

                        for(int  i= 0;i<addOnsResponse.getData().size();i++){

                            if(addOnsResponse.getData().get(i).getName().equals(groups.get(j).getName())){

                                productListingAdapter.remove(addOnsResponse.getData().get(i));


                            }else{
                                //productListingAdapter.add(addOnsResponse.getData().get(i));

                            }


                        }


                    }

                }else{

                    productListingAdapter.addAll(addOnsResponse.getData());
                }

                break;

            case AppConstant.AD_ADON_PRODUCT:

                mainActivity.popFragment();
                break;

        }

    }


    @Override
    public void onClickAddChoices(AddOnsResponse.Datum position) {


        serviceHelper.enqueueCall(webService.addAdonToProduct(String.valueOf(position.getId()), id), AppConstant.AD_ADON_PRODUCT);
//        ChoiceFragment choiceFragment = new ChoiceFragment();
//        choiceFragment.setname(position);
//        mainActivity.replaceFragment(choiceFragment, true, true);
    }

    @Override
    public void onClickEdit(AddOnsResponse.Datum position) {

    }

    @Override
    public void onClickDelete(AddOnsResponse.Datum position) {

    }

    boolean b= false;
    public void setIsShow(boolean b) {
        this.b= b;
    }
}