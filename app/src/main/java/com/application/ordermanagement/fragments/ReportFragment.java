package com.application.ordermanagement.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.ReportAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.SalesResponse.SalesResponseClass;
import com.daimajia.androidanimations.library.Techniques;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ReportFragment extends BaseFragment {
    @BindView(R.id.tvEndDate)
    TextView tvEndDate;
    @BindView(R.id.tvStartDate)
    TextView tvStartDate;
    @BindView(R.id.tvTotalSales)
    TextView tvTotalSales;

    @BindView(R.id.tvMostSaleItem)
    TextView tvMostSaleItem;

    @BindView(R.id.tvTotalOrders)
    TextView tvTotalOrders;

    Unbinder unbinder;

    ReportAdapter reportAdapter;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setTitle("Sales Report");
        titlebar.showBackButton(mainActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        unbinder = ButterKnife.bind(this, view);


        tvStartDate.setOnClickListener(v -> {
            openStartDatePicker(1);
        });
        tvEndDate.setOnClickListener(v -> {
            openStartDatePicker(0);
        });
        return view;
    }

    public void getTotalSales(String start_date, String end_date) {

        hitcall(start_date, end_date);
    }

    final Calendar myCalendar = Calendar.getInstance();
    String start, end;
    private int mYear, mMonth, mDay, mHour, mMinute;

    public void openStartDatePicker(int i) {


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if(i==1){

                            tvStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            if(!tvStartDate.getText().toString().equals("0000-00-00") && !tvEndDate.getText().toString().equals("0000-00-00")){
                                hitcall(tvStartDate.getText().toString(),tvEndDate.getText().toString());
                            }
                        }else{

                            tvEndDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            if(!tvStartDate.getText().toString().equals("0000-00-00") && !tvEndDate.getText().toString().equals("0000-00-00")){
                                hitcall(tvStartDate.getText().toString(),tvEndDate.getText().toString());
                            }
                        }
                        //tvStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        //  selectDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        //  selectDate.setText(year + "-" + (monthOfYear + 1) + "-" + year);


                    }
                }, mYear, mMonth, mDay);
        if (i == 1) {

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        }else{
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);


           // datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        }
        datePickerDialog.show();


    }

    public void hitcall(String start_date, String end_date) {


        serviceHelper.enqueueCall(webService.getTotalSales(start_date, end_date), AppConstant.GET_TOTAL_SALES);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {


        SalesResponseClass salesResponseClass = (SalesResponseClass) result;
        tvTotalSales.setText(salesResponseClass.getData().getTotalSales()+" £");
        tvMostSaleItem.setText(salesResponseClass.getData().getMost_sale_item());
        tvTotalOrders.setText(salesResponseClass.getData().getTota_orders()+ " Orders");
        UIHelper.animation(Techniques.Pulse,1000,1,tvTotalSales);
        UIHelper.animation(Techniques.Pulse,1000,1,tvMostSaleItem);
        UIHelper.animation(Techniques.Pulse,1000,1,tvTotalOrders);
    }
}
