package com.application.ordermanagement.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.MenuAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MenuListingFragment extends BaseFragment implements MenuAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;
    MenuAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;
    @BindView(R.id.addchoices)
    LinearLayout addchoices;
    ArrayList<String> items;


    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Menu");

    }

    boolean posBoolean = false;

    public void setPos(boolean posBoolean) {
        this.posBoolean = posBoolean;
    }

    public static MenuListingFragment newInstance() {
        Bundle args = new Bundle();
        MenuListingFragment fragment = new MenuListingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    GridLayoutManager gridLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        items = new ArrayList<>();
        if (posBoolean) {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
            rvProducts.setLayoutManager(gridLayoutManager);

        } else {
            rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));

        }
        productListingAdapter = new MenuAdapter(this, mainActivity, this, true);

        rvProducts.setAdapter(productListingAdapter);

        addchoices.setVisibility(!posBoolean ? View.VISIBLE : View.GONE);
        addchoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.replaceFragment(new AddnewCategoryFragment(), true, true);
            }
        });
        getMenu();
        return view;
    }

    public void getMenu() {
        serviceHelper.enqueueCall(webService.getMenu(), AppConstant.GET_ALL_MENUS);
    }



    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.GET_ALL_MENUS:
                MenuResponse menuResponse = (MenuResponse) result;
                productListingAdapter.addAll(menuResponse.getData());
                productListingAdapter.notifyDataSetChanged();
                break;
            case AppConstant.DELETE_CATEGORY:
                getMenu();
                break;
        }
    }


//    @Override
//    public void onClick(String position) {
//        ChoiceFragment choiceFragment = new ChoiceFragment();
//        choiceFragment.setname(position);
//        mainActivity.replaceFragment(choiceFragment, true, true);
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MenuResponse menuResponse) {
        UIHelper.hideSoftKeyboards(mainActivity);
        UIHelper.showAlertDialog(mainActivity, getString(R.string.success), "Category Added Successfully");
        mainActivity.popFragment();
        getMenu();
        //productListingAdapter.addAll(menuResponse.getData());
    }

    @Override
    public void clickmenu(MenuResponse.Datum datum, String name) {
        AddNewMenuFragment menuProductsListingFragment = new AddNewMenuFragment();
        menuProductsListingFragment.setname(datum.getProducts(), name, String.valueOf(datum.getId()));
        menuProductsListingFragment.setPos(posBoolean);
        mainActivity.replaceFragment(menuProductsListingFragment, true, true);

    }

    @Override
    public void clickmenudelete(long datum) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        serviceHelper.enqueueCall(webService.deleteCategory(datum), AppConstant.DELETE_CATEGORY);
                        dialog.dismiss();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    @Override
    public void edit(MenuResponse.Datum datum) {
        AddnewCategoryFragment addnewCategoryFragment =new AddnewCategoryFragment();
        addnewCategoryFragment.setdata(datum);
        mainActivity.replaceFragment(addnewCategoryFragment, true, true);

    }

    @Override
    public void retryPageLoad() {

    }


}