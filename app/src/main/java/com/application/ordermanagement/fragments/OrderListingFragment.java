package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.OrderListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.models.OrdersResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OrderListingFragment extends BaseFragment implements OrderListingAdapter.IOrderUpdateAdapter {
    Unbinder unbinder;

    OrderListingAdapter orderListingAdapter;

    @BindView(R.id.rvOrders)
    RecyclerView rvOrders;
    @BindView(R.id.nocategoryfound)
    TextView nocategoryfound;

    @BindView(R.id.llCustomDate)
    LinearLayout llCustomDate;

    @BindView(R.id.tvCustom)
    Button tvCustom;


    @BindView(R.id.tvStartDate)
    Button tvStartDate;
    @BindView(R.id.tvEndDate)
    Button tvEndDate;

    @BindView(R.id.tvToday)
    Button tvToday;

    @BindView(R.id.tvYesterday)
    Button tvYesterday;


    int k=-1;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setTitle("Order Listing");
        titlebar.showBackButton(mainActivity);
    }

    public static OrderListingFragment newInstance() {
        Bundle args = new Bundle();
        OrderListingFragment fragment = new OrderListingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    int i = 0;
    String today, yesterday;
    String start, end;

    public void changeColor(int a){
        switch (a){

            case 0:
                tvToday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.red_color));
                tvYesterday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));
                tvCustom.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));

                break;
            case 1:
                tvToday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));
                tvYesterday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.red_color));
                tvCustom.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));
                break;
            case 2:
                tvToday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));
                tvYesterday.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.white));
                tvCustom.setBackgroundTintList(ContextCompat.getColorStateList(mainActivity, R.color.red_color));
                break;
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orderlisting, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        orderListingAdapter = new OrderListingAdapter(mainActivity, this);
        rvOrders.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvOrders.setAdapter(orderListingAdapter);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentTime = Calendar.getInstance().getTime();
        today = dateFormat.format(currentTime);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        yesterday = dateFormat.format(cal.getTime());

        changeColor(0);
        tvCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                k=2;
                i = 3;
                changeColor(k);
                if (llCustomDate.getVisibility() == View.VISIBLE) {
                    llCustomDate.setVisibility(View.GONE);
                } else {

                    llCustomDate.setVisibility(View.VISIBLE);

                }
            }
        });

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStartDatePicker(1);
            }
        });
        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStartDatePicker(0);

            }
        });

        tvToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 1;
                k=0;
                changeColor(k);

                serviceHelper.enqueueCall(webService.getAllOrders(today, today), AppConstant.GET_ALL_ORDERS);

            }
        });
        tvYesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 2;
                k=1;
                changeColor(k);

                serviceHelper.enqueueCall(webService.getAllOrders(yesterday, yesterday), AppConstant.GET_ALL_ORDERS);

            }
        });
        serviceHelper.enqueueCall(webService.getAllOrders(today, today), AppConstant.GET_ALL_ORDERS);
        return view;
    }

    int mYear, mMonth, mDay;

    public void openStartDatePicker(int i) {


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (i == 1) {

                            tvStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            if (!tvStartDate.getText().toString().equals("0000-00-00") && !tvEndDate.getText().toString().equals("0000-00-00")) {
                                //   hitcall(tvStartDate.getText().toString(),tvEndDate.getText().toString());
                                serviceHelper.enqueueCall(webService.getAllOrders(tvStartDate.getText().toString(), tvEndDate.getText().toString()), AppConstant.GET_ALL_ORDERS);

                            }
                        } else {

                            tvEndDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            if (!tvStartDate.getText().toString().equals("0000-00-00") && !tvEndDate.getText().toString().equals("0000-00-00")) {

                                serviceHelper.enqueueCall(webService.getAllOrders(tvStartDate.getText().toString(), tvEndDate.getText().toString()), AppConstant.GET_ALL_ORDERS);

                                ///   hitcall(tvStartDate.getText().toString(),tvEndDate.getText().toString());
                            }
                        }
                        //tvStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        //  selectDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        //  selectDate.setText(year + "-" + (monthOfYear + 1) + "-" + year);


                    }
                }, mYear, mMonth, mDay);
        if (i == 1) {

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        } else {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);


            // datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        }
        datePickerDialog.show();


    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.UPDATE_ORDERS:
                switch (i) {
                    case 0:
                        serviceHelper.enqueueCall(webService.getAllOrders(today, today), AppConstant.GET_ALL_ORDERS);

                        break;
                    case 1:
                        serviceHelper.enqueueCall(webService.getAllOrders(today, today), AppConstant.GET_ALL_ORDERS);

                        break;
                    case 2:
                        serviceHelper.enqueueCall(webService.getAllOrders(yesterday, yesterday), AppConstant.GET_ALL_ORDERS);

                        break;
                    case 3:
                        serviceHelper.enqueueCall(webService.getAllOrders(tvStartDate.getText().toString(), tvEndDate.getText().toString()), AppConstant.GET_ALL_ORDERS);

                        break;

                }
                break;
            case AppConstant.GET_ALL_ORDERS:
                OrdersResponse ordersResponse = (OrdersResponse) result;
                if (ordersResponse.getData().size() > 0) {
                    orderListingAdapter.addAll(ordersResponse.getData());
                    nocategoryfound.setVisibility(View.GONE);
                } else {
                    rvOrders.setVisibility(View.GONE);
                    nocategoryfound.setVisibility(View.VISIBLE);
                }
                break;

        }

    }

    @Override
    public void updateOrder(int id, String status) {
        serviceHelper.enqueueCall(webService.updateOrder(id, status), AppConstant.UPDATE_ORDERS);
    }

    @Override
    public void onClickitem(OrdersResponse.Datum datum) {
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        orderDetailFragment.setdata(datum);
        mainActivity.replaceFragment(orderDetailFragment, true, true);


    }
}
