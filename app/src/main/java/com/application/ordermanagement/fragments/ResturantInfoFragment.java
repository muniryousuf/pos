package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.model.ResturantInfoBodyRequest;
import com.application.ordermanagement.models.Resturant.ResturantResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ResturantInfoFragment extends BaseFragment {
    ResturantInfoBodyRequest resturantInfoBodyRequest;
    Unbinder unbinder;

    @BindView(R.id.swDineIN)
    Switch swDineIN;
    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.swPickup)
    Switch swPickup;

    @BindView(R.id.swDelivery)
    Switch swDelivery;

    @BindView(R.id.tvName)
    EditText tvName;

    @BindView(R.id.tvEmail)
    EditText tvEmail;
    @BindView(R.id.tvWebsite)
    EditText tvWebsite;
    @BindView(R.id.tvZipCode)
    EditText tvZipCode;
    @BindView(R.id.tvPhoneNumber)
    EditText tvPhoneNumber;
    @BindView(R.id.tvCity)
    EditText tvCity;
    @BindView(R.id.tvAddress)
    EditText tvAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resturant_info, container, false);
        unbinder = ButterKnife.bind(this, view);

        resturantInfoBodyRequest = new ResturantInfoBodyRequest();

        swDineIN.setChecked(mainActivity.prefHelper.getResturant().getmIsDine() == 1 ? true : false);
        swPickup.setChecked(mainActivity.prefHelper.getResturant().getmIsPickup() == 1 ? true : false);
        swDelivery.setChecked(mainActivity.prefHelper.getResturant().getmIsDelivery() == 1 ? true : false);
        resturantInfoBodyRequest.setIsPickup(Long.valueOf(mainActivity.prefHelper.getResturant().getmIsPickup()));
        resturantInfoBodyRequest.setIsDelivery(Long.valueOf(mainActivity.prefHelper.getResturant().getmIsDelivery()));
        resturantInfoBodyRequest.setIsDine(Long.valueOf(mainActivity.prefHelper.getResturant().getmIsDine()));

        tvName.setText(mainActivity.prefHelper.getResturant().getName());
        tvAddress.setText(mainActivity.prefHelper.getResturant().getAddress().getAddress());
        tvZipCode.setText(mainActivity.prefHelper.getResturant().getAddress().getZipCode());
        tvCity.setText(mainActivity.prefHelper.getResturant().getAddress().getCity());
        tvPhoneNumber.setText(mainActivity.prefHelper.getResturant().getPhoneNumber());
        tvEmail.setText(mainActivity.prefHelper.getResturant().getmEmail());
        tvWebsite.setText(mainActivity.prefHelper.getResturant().getWebsiteUrl());
        swDineIN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    resturantInfoBodyRequest.setIsDine(Long.valueOf(0));
                } else {
                    resturantInfoBodyRequest.setIsDine(Long.valueOf(1));

                }
            }
        });

        swDelivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    resturantInfoBodyRequest.setIsDelivery(Long.valueOf(0));
                } else {
                    resturantInfoBodyRequest.setIsDelivery(Long.valueOf(1));

                }
            }
        });

        swPickup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    resturantInfoBodyRequest.setIsPickup(Long.valueOf(0));
                } else {
                    resturantInfoBodyRequest.setIsPickup(Long.valueOf(1));

                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ResturantInfoBodyRequest.Address address = new ResturantInfoBodyRequest.Address();
                address.setAddress("" + tvAddress.getText().toString());
                address.setZipCode("" + tvZipCode.getText().toString());
                address.setCity("" + tvCity.getText().toString());
                resturantInfoBodyRequest.setAddress(address);
                resturantInfoBodyRequest.setName("" + tvName.getText().toString());
                resturantInfoBodyRequest.setEmail("" + tvEmail.getText().toString());
                resturantInfoBodyRequest.setWebsiteUrl("" + tvWebsite.getText().toString());
                resturantInfoBodyRequest.setPhoneNumber("" + tvPhoneNumber.getText().toString());

                serviceHelper.enqueueCall(webService.updateResturantInfo(resturantInfoBodyRequest), AppConstant.RESTURANT_INFO);

            }
        });
        return view;
    }

    @Override
    protected void setTitle(Titlebar titlebar) {

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.RESTURANT_INFO:
                ResturantResponse responseBody = (ResturantResponse) result;
                mainActivity.prefHelper.putResturant(responseBody.getData());
                break;

        }
    }
}
