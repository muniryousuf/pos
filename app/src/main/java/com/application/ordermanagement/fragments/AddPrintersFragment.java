package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Switch;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.CategoryListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.model.Printers.PrintersResponse;
import com.application.ordermanagement.models.CategoryResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddPrintersFragment extends BaseFragment {
    Unbinder unbinder;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.printerIp)
    EditText printerIp;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.numbercounter)
    NumberPicker numbercounter;
    @BindView(R.id.swStatus)
    Switch swStatus;
    @BindView(R.id.swStatusisDefault)
    Switch swStatusisDefault;
    int number = 0;
    CategoryListingAdapter categoryListingAdapter;
    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_printers, container, false);
        unbinder = ButterKnife.bind(this, view);
        categoryListingAdapter = new CategoryListingAdapter(mainActivity);

        rvCategory.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        rvCategory.setAdapter(categoryListingAdapter);
        numbercounter.setMaxValue(100);
        numbercounter.setMinValue(0);
        numbercounter.setOrientation(LinearLayout.HORIZONTAL);

        btnSubmit.setOnClickListener(v -> {
            if (name.getText().toString().length() == 0) {
                UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Printer name required");

            } else if (printerIp.getText().toString().length() == 0) {
                UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Ip required");

            } else {

                PrintersResponse.Datum datum = new PrintersResponse.Datum();
                datum.setName(name.getText().toString());
                datum.setIp(printerIp.getText().toString());
                datum.setStatus(swStatus.isChecked() ? 1 : 0);
                datum.setIs_default(swStatusisDefault.isChecked() ? 1 : 0);
                datum.setPrint_count(number);
                datum.setCategories(categoryListingAdapter.getSelectedIds());
                if(is_edit){

                    serviceHelper.enqueueCall(webService.addprinterput(datum.getId(),datum), AppConstant.ADD_PRINTERS);
                }else{

                    serviceHelper.enqueueCall(webService.addprinter(datum), AppConstant.ADD_PRINTERS);
                }

            }

        });
        numbercounter.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                number = newVal;

            }
        });
        if(datum!=null){
            numbercounter.setValue(datum.getPrint_count());
            swStatus.setChecked(datum.getStatus()==1?true:false);
            name.setText(datum.getName());
            swStatusisDefault.setChecked(datum.getIs_default()==1?true:false);
            printerIp.setText(datum.getIp());
            serviceHelper.enqueueCall(webService.getAllCategory(), AppConstant.ALL_CATEGORYS);

        }else{
            serviceHelper.enqueueCall(webService.getAllCategory(), AppConstant.ALL_CATEGORYS);

        }
        return view;
    }



    @Override
    protected void setTitle(Titlebar titlebar) {

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.ALL_CATEGORYS:
                CategoryResponse categoryResponse = (CategoryResponse) result;
                if (categoryResponse.getData().size() > 0) {
                    if(datum!=null){

                        for(int j=0; j<datum.getCategories().size();j++){
                            for(int i =0; i<categoryResponse.getData().size();i++){
                                if(datum.getCategories().get(j).getCategory().getId()==categoryResponse.getData().get(i).getId()){
                                    categoryResponse.getData().get(i).setIs_selected(true);
                                }
                            }
                        }
                        categoryListingAdapter.addAll(categoryResponse.getData());
                    }else{

                        categoryListingAdapter.addAll(categoryResponse.getData());
                    }

                } else {
                    rvCategory.setVisibility(View.GONE);

                }
                break;
            case AppConstant.ADD_PRINTERS:
                mainActivity.onBackPressed();
                break;

        }

    }
    PrinterResponseList.Datum datum;
    boolean is_edit =false;
    public void setdata(PrinterResponseList.Datum datum,boolean is_edit) {
        this.is_edit =is_edit;
        this.datum =datum;
    }
}
