package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SelectionScreenFragment extends BaseFragment {
    Unbinder unbinder;


    @BindView(R.id.mainGrid)
    GridLayout mainGrid;

    @BindView(R.id.tvonHoldItems)
    TextView tvonHoldItems;


    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.VISIBLE);
        titlebar.setTitle("Dashboard");
        titlebar.hideTitlebar();
        titlebar.hideBackbutton(mainActivity);
    }

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selection_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainGrid.setAlpha(0f);
        mainGrid.setVisibility(View.VISIBLE);

        tvonHoldItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoldItemsFragment holdItemsFragment = new HoldItemsFragment();
                mainActivity.addFragment(holdItemsFragment, true, true);
            }
        });
        mainGrid.animate()
                .alpha(1f)
                .setDuration(2000)
                .setListener(null);

        //Set Event
        setSingleEvent(mainGrid);// Create an object of CustomAdapter and set Adapter to GirdView

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
    }

    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (finalI == 2) {
                        mainActivity.replaceFragment(new OrderListingFragment(), true, true);


                    } else if (finalI == 3 || finalI == 4) {

                        showAdonDialog(mainActivity, finalI == 3 ? true : false);
                    } else {
                        POSFragment menuListingFragment = new POSFragment();
                        menuListingFragment.setPos(true);

                        switch (finalI) {

                            case 0:
                                TabelReservationFragment tabelReservationFragment =new TabelReservationFragment();
                            //    menuListingFragment.setPostalCodeandPhoneNumber("", "","", "Eat in");
                                mainActivity.addFragment(tabelReservationFragment, true, true);
                                break;
                            case 1:
                                menuListingFragment.setPostalCodeandPhoneNumber("", "","","Take away" );
                                mainActivity.addFragment(menuListingFragment, true, true);
                                break;
                            case 5:
                                menuListingFragment.setPostalCodeandPhoneNumber("", "","","Discount" );
                                mainActivity.addFragment(menuListingFragment, true, true);
                                break;

                        }

                    }


                }
            });
        }
    }

    public void showAdonDialog(Activity activity, boolean is_delivery) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        EditText name = (EditText) dialog.findViewById(R.id.name);
        EditText phone = (EditText) dialog.findViewById(R.id.phone);
        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        EditText postalCode = (EditText) dialog.findViewById(R.id.postalCode);

        if (!is_delivery) {
            postalCode.setVisibility(View.GONE);
            name.setVisibility(View.VISIBLE);
        } else {

            name.setVisibility(View.GONE);

        }


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone.getText().toString().length() > 10) {
                    dialog.dismiss();
                    UIHelper.hideSoftKeyboards(mainActivity);
//                        SelectionScreenFragment selectionScreen =new SelectionScreenFragment();
//                        mainActivity.addFragment(selectionScreen, true, true);
                    POSFragment menuListingFragment = new POSFragment();
                    menuListingFragment.setPos(true);
                    menuListingFragment.setPostalCodeandPhoneNumber(phone.getText().toString(), postalCode.getText().toString(), name.getText().toString(), is_delivery ? "Delivery" : "Collection");
                    mainActivity.addFragment(menuListingFragment, true, true);


                } else {
                    UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Enter valid  phone number");
                }

            }
        });


        dialog.show();

    }


}
