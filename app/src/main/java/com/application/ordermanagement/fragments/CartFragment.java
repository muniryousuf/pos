package com.application.ordermanagement.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.CartAdapter;
import com.application.ordermanagement.adapter.ReportAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.SalesResponse.SalesResponseClass;
import com.daimajia.androidanimations.library.Techniques;

import org.w3c.dom.Text;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CartFragment extends BaseFragment {

    @BindView(R.id.rvCart)
    RecyclerView rvCart;

    @BindView(R.id.tvPlaceOrder)
    TextView tvPlaceOrder;

    Unbinder unbinder;

    CartAdapter reportAdapter;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setTitle("Cart");
        titlebar.showBackButton(mainActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this, view);

        reportAdapter = new CartAdapter( mainActivity);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(reportAdapter);
        reportAdapter.notifyDataSetChanged();

        tvPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.showAlertDialog(mainActivity,"Success","Order placed successfully");
                mainActivity.popFragment();
            }
        });
        return view;
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {



    }
}
