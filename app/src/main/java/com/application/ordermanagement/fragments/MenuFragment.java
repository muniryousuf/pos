package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.MenuAdapter;
import com.application.ordermanagement.adapter.OrderListingAdapter;
import com.application.ordermanagement.adapter.ProductListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MenuFragment extends BaseFragment implements MenuAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;


    @BindView(R.id.rvmenu)
    RecyclerView rvmenu;

    MenuAdapter menuAdapter;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.VISIBLE);
        titlebar.setTitle("Menu");
        titlebar.showBackButton(mainActivity);
    }

    public static MenuFragment newInstance() {
        Bundle args = new Bundle();

        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        menuAdapter = new MenuAdapter(this, mainActivity, this, true);
        rvmenu.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvmenu.setAdapter(menuAdapter);
        getMenu();

        return view;
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.GET_ALL_MENUS:
                MenuResponse menuResponse = (MenuResponse) result;
                menuAdapter.addAll(menuResponse.getData());
                menuAdapter.notifyDataSetChanged();

                break;


        }


    }


    public void getMenu() {
        serviceHelper.enqueueCall(webService.getMenu(), AppConstant.GET_ALL_MENUS);
    }


    @Override
    public void clickmenu(MenuResponse.Datum list,String name) {
        MenuProductsListingFragment menuProductsListingFragment = new MenuProductsListingFragment();
        menuProductsListingFragment.setData(list.getProducts());
        mainActivity.replaceFragment(menuProductsListingFragment,true,true);
    }

    @Override
    public void clickmenudelete(long datum) {

    }

    @Override
    public void edit(MenuResponse.Datum datum) {

    }

    @Override
    public void retryPageLoad() {

    }
}

