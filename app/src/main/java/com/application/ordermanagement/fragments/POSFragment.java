package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.CheckoutAddonAdapter;
import com.application.ordermanagement.adapter.POS.POSCartAdapter;
import com.application.ordermanagement.adapter.POS.PosMenuAdapter;
import com.application.ordermanagement.adapter.POS.PosProductMenuAdapter;
import com.application.ordermanagement.adapter.POS.SizesAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class POSFragment extends BaseFragment implements POSCartAdapter.onClickOptions, PosMenuAdapter.ISubCategoryProductAdapter, PosProductMenuAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;
    PosMenuAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;
    ArrayList<String> items;

    PosProductMenuAdapter productMenuAdapter;
    @BindView(R.id.rvSubProducts)
    RecyclerView rvSubProducts;
    POSCartAdapter checkoutCartAdapter;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottom_sheet;
    @BindView(R.id.rvCart)
    RecyclerView rvCart;
    @BindView(R.id.btnPayment)
    TextView btnPayment;
    @BindView(R.id.tvCategoryName)
    TextView tvCategoryName;
    @BindView(R.id.totalAmount)
    TextView totalAmount;
    @BindView(R.id.btnDeleteItem)
    TextView btnDeleteItem;
    @BindView(R.id.btnholditem)
    TextView btnholditem;
    @BindView(R.id.btnAdditem)
    TextView btnAdditem;
    @BindView(R.id.btnhold)
    TextView btnhold;
    @BindView(R.id.etDetails)
    EditText etDetails;

    POSCartAdapter posCartAdapter;
    String name;
    String phoneNumber;
    String postalCode;

    BottomSheetBehavior sheetBehavior;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("POS");
        titlebar.hideTitlebar();

    }

    boolean posBoolean = false;

    public void setPos(boolean posBoolean) {
        this.posBoolean = posBoolean;
    }

    public static POSFragment newInstance() {
        Bundle args = new Bundle();
        POSFragment fragment = new POSFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    GridLayoutManager gridLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pos, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        items = new ArrayList<>();
        posCartAdapter = new POSCartAdapter(mainActivity, this, true);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(posCartAdapter);


        posCartAdapter.notifyDataSetChanged();
        if (orderPlaceModel != null && orderPlaceModel.getOrderDetails() != null && orderPlaceModel.getOrderDetails().size() > 0) {
            posCartAdapter.addAll(orderPlaceModel.getOrderDetails());
            UIHelper.showAlertDialog(mainActivity, "Success", "Product Added to Cart");
            total_amount = orderPlaceModel.getTotalAmountWithFee();

            current_amount = current_amount + total_amount;
            total_amount = 0;
            totalAmount.setText("£  " + setdecimal((float) current_amount));

            // orderPlaceModel.setUserId(Long.valueOf(preferenceHelper.getUser().getId().toString()));
            orderPlaceModel.setCvvNumber(null);
            orderPlaceModel.setPayment("COD");
            orderPlaceModel.setDiscountedAmount(Long.valueOf("0"));

            orderPlaceModel.setTotalAmountWithFee(current_amount);
        }

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderPlaceModel.setOrderDetails(posCartAdapter.getCart());
                if (orderPlaceModel.getOrderDetails().size() > 0) {
                    CheckOutPosFragment checkOutPosFragment = new CheckOutPosFragment();
                    checkOutPosFragment.setdata(orderPlaceModel, orderType);
                    mainActivity.addFragment(checkOutPosFragment, true, true);
                }


            }
        });

//        btnDeleteItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (posCartAdapter.getItemCount() > 0) {
//                    if (posCartAdapter.is_delete_btn) {
//                        posCartAdapter.viewAllDelete(false);
//                    } else {
//                        posCartAdapter.viewAllDelete(true);
//                    }
//                }
//            }
//        });
//        btnAdditem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (posCartAdapter.getItemCount() > 0) {
//                    if (posCartAdapter.is_open_edit) {
//                        posCartAdapter.openPOSEditOption(false);
//                    } else {
//                        posCartAdapter.openPOSEditOption(true);
//                    }
//                }
//            }
//        });

        btnholditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posCartAdapter.getItemCount() > 0) {
                    showAdonDialog(mainActivity);
                }
            }
        });
        btnhold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoldItemsFragment holdItemsFragment = new HoldItemsFragment();
                mainActivity.addFragment(holdItemsFragment, true, true);
            }
        });

        if (posBoolean) {
            // gridLayoutManager = new GridLayoutManager(getContext(), 2);
            rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, true));

            //rvProducts.setLayoutManager(new GridLayoutManager(mainActivity,1));

        } else {
            rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, true));

            // rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));

        }
        productListingAdapter = new PosMenuAdapter(this, mainActivity, this, true);

        rvProducts.setAdapter(productListingAdapter);


        productMenuAdapter = new PosProductMenuAdapter(this, mainActivity, this, true, true);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL);
        rvSubProducts.setItemAnimator(new DefaultItemAnimator());
        rvSubProducts.setLayoutManager(staggeredGridLayoutManager);
        rvSubProducts.setAdapter(productMenuAdapter);

        getMenu();
        setData();
        // sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        //   sheetBehavior.setHideable(false);
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
//        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                switch (newState) {
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        break;
//                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        // btnBottomSheet.setText("Close Sheet");
//                    }
//                    break;
//                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        //      btnBottomSheet.setText("Expand Sheet");
//                    }
//                    break;
//                    case BottomSheetBehavior.STATE_DRAGGING:
//                        break;
//                    case BottomSheetBehavior.STATE_SETTLING:
//                        break;
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });

        return view;
    }

    private void setData() {
        etDetails.setText((name != null ? (!name.equalsIgnoreCase("") ? ("Name : ") + name + "\n" : "") : "") + (phoneNumber != null ? (!phoneNumber.equalsIgnoreCase("") ? ("Phone number : ") + phoneNumber + "\n" : "") : "") + (postalCode != null ? (!postalCode.equalsIgnoreCase("") ? ("Post Code : ") + postalCode : "") : ""));
    }

    public void getMenu() {
        serviceHelper.enqueueCall(webService.getMenu(), AppConstant.GET_ALL_MENUS);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    int position = 0;

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.GET_PRODUCT_DATA:
                MenuResponse.Datum singleProductResponse = (MenuResponse.Datum) result;

                showAdonDialog(mainActivity, singleProductResponse.getData(), is_edit?orderDetail:null);

                break;
            case AppConstant.GET_ALL_MENUS:
                MenuResponse menuResponse = (MenuResponse) result;
                productListingAdapter.addAll(menuResponse.getData());
                productListingAdapter.notifyDataSetChanged();


                tvCategoryName.setText(menuResponse.getData().get(position).getName());

                productMenuAdapter.addAll(menuResponse.getData().get(position).getProducts());
                productMenuAdapter.notifyDataSetChanged();
                break;
            case AppConstant.DELETE_CATEGORY:
                getMenu();
                break;

        }
    }


//    @Override
//    public void onClick(String position) {
//        ChoiceFragment choiceFragment = new ChoiceFragment();
//        choiceFragment.setname(position);
//        mainActivity.replaceFragment(choiceFragment, true, true);
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MenuResponse menuResponse) {
        UIHelper.hideSoftKeyboards(mainActivity);
        UIHelper.showAlertDialog(mainActivity, getString(R.string.success), "Category Added Successfully");
        mainActivity.popFragment();
        getMenu();
        //productListingAdapter.addAll(menuResponse.getData());
    }

    @Override
    public void clickmenu(MenuResponse.Datum datum, String name) {
//        AddNewMenuFragment menuProductsListingFragment = new AddNewMenuFragment();
//        menuProductsListingFragment.setname(datum.getProducts(), name,String.valueOf(datum.getId()));
//        menuProductsListingFragment.setPos(posBoolean);
//        mainActivity.replaceFragment(menuProductsListingFragment, true, true);

        tvCategoryName.setText(name);
        productMenuAdapter.addAll(datum.getProducts());
        productMenuAdapter.notifyDataSetChanged();
        //   productListingAdapter.addAll(datum);

    }

    @Override
    public void clickmenudelete(long datum) {

        serviceHelper.enqueueCall(webService.deleteCategory(datum), AppConstant.DELETE_CATEGORY);
    }

    @Override
    public void retryPageLoad() {

    }


    @Override
    public void updatePrice(int id, String price) {

    }

    public class ContaingElemet {

        int position;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public int containsName(final List<OrderPlaceModel.OrderDetail> list, long name) {

        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).getProductId() == name) {
                return j;
            }
        }
        return -1;

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void clickmenu(MenuResponse.Product datum) {
        orderDetail = null;
        if (datum.getSizes().size() == 0 && datum.getmGroup().size() == 0) {
            if (posCartAdapter.getCart() != null && posCartAdapter.getCart().size() > 0) {
                for (int i = 0; i < posCartAdapter.getCart().size(); i++) {
                    if (containsName(posCartAdapter.getCart(), datum.getId()) == -1) {

                        ArrayList<OrderPlaceModel.Extra> extras = new ArrayList<>();
                        OrderPlaceModel.OrderDetail detail = new OrderPlaceModel.OrderDetail();

                        detail.setProductName(datum.getName());
                        detail.setProductId(datum.getId());
                        detail.setQuantity(Long.valueOf(1));
                        detail.setPrice(Double.valueOf(datum.getPrice()));
                        detail.setSingleProductTotalAmount(Double.valueOf(String.valueOf(1)) * Double.valueOf(datum.getPrice()));
                        detail.setProductType("product");
                        detail.setSpecial_instructions("");


                        if (total_amount > 0) {
                            total_amount = total_amount * Long.valueOf(1);
                        } else {
                            total_amount = Double.valueOf(datum.getPrice()) * Long.valueOf(1);
                        }
                        detail.setExtras(extras);
                        orderDetails.add(detail);
                        posCartAdapter.add(detail);
                        orderPlaceModel.setCvvNumber(null);
                        orderPlaceModel.setPayment("COD");
                        orderPlaceModel.setDiscountedAmount(Long.valueOf("0"));

                        orderPlaceModel.setTotalAmountWithFee(current_amount);
                        orderPlaceModel.setOrderDetails(orderDetails);
                        break;


                    } else {
                        posCartAdapter.getCart().get(containsName(posCartAdapter.getCart(), datum.getId())).setQuantity(posCartAdapter.getCart().get(containsName(posCartAdapter.getCart(), datum.getId())).getQuantity() + 1);
                        posCartAdapter.getCart().set(containsName(posCartAdapter.getCart(), datum.getId()), posCartAdapter.getCart().get(containsName(posCartAdapter.getCart(), datum.getId())));
                        posCartAdapter.addAll(orderPlaceModel.order_details);
                        break;
                    }

                }
            } else {
                ArrayList<OrderPlaceModel.Extra> extras = new ArrayList<>();
                OrderPlaceModel.OrderDetail detail = new OrderPlaceModel.OrderDetail();
                detail.setProductName(datum.getName());
                detail.setProductId(datum.getId());
                detail.setQuantity(Long.valueOf(1));
                detail.setPrice(Double.valueOf(datum.getPrice()));
                detail.setSingleProductTotalAmount(Double.valueOf(String.valueOf(1)) * Double.valueOf(datum.getPrice()));
                detail.setProductType("product");
                detail.setSpecial_instructions("");


                if (total_amount > 0) {
                    total_amount = total_amount * Long.valueOf(1);
                } else {
                    total_amount = Double.valueOf(datum.getPrice()) * Long.valueOf(1);
                }
                detail.setExtras(extras);
                orderDetails.add(detail);
                posCartAdapter.add(detail);
                orderPlaceModel.setCvvNumber(null);
                orderPlaceModel.setPayment("COD");
                orderPlaceModel.setDiscountedAmount(Long.valueOf("0"));

                orderPlaceModel.setTotalAmountWithFee(posCartAdapter.getCureentCartAmount());
                orderPlaceModel.setOrderDetails(orderDetails);
            }
            current_amount =posCartAdapter.getCureentCartAmount();
            totalAmount.setText("£  " + setdecimal((float) current_amount));


        } else {
            is_edit =false;
            serviceHelper.enqueueCall(webService.getProductData(datum.getId()), AppConstant.GET_PRODUCT_DATA);
            // showAdonDialog(mainActivity, datum,null);
        }

        //  serviceHelper.enqueueCall(webService.getProductData(datum.getId()), AppConstant.GET_PRODUCT_DATA);
    }

    @Override
    public void updateAvailibity(int id, String price, int status) {

    }


    @Override
    public void addtocart() {
//        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//        bottom_sheet.setVisibility(View.VISIBLE);
    }

    double total_amount = 0;
    double current_amount = 0;
    String orderType;
    OrderPlaceModel orderPlaceModel = new OrderPlaceModel();
    ArrayList<OrderPlaceModel.OrderDetail> orderDetails = new ArrayList<>();

    public void showAdonDialog(Activity activity, MenuResponse.Product datum, final OrderPlaceModel.OrderDetail orderDetail) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_adon_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        CheckoutAddonAdapter checkoutAddonAdapter = new CheckoutAddonAdapter(mainActivity);
        SizesAdapter sizesAdapter = new SizesAdapter(mainActivity);

        LinearLayout layoutSizes = (LinearLayout) dialog.findViewById(R.id.layoutSizes);
        RecyclerView rvAdones = (RecyclerView) dialog.findViewById(R.id.rvAdones);
        ElegantNumberButton counter = (ElegantNumberButton) dialog.findViewById(R.id.counter);
        RecyclerView rvSizes = (RecyclerView) dialog.findViewById(R.id.rvSizes);

        TextView totalamount = (TextView) dialog.findViewById(R.id.totalAmount);
        TextView tvprice = (TextView) dialog.findViewById(R.id.tvprice);
        TextView prodictname = (TextView) dialog.findViewById(R.id.prodictname);
        TextView description = (TextView) dialog.findViewById(R.id.description);
        EditText etSpecialInstruction = (EditText) dialog.findViewById(R.id.etSpecialInstruction);

        rvAdones.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvSizes.setLayoutManager(new LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, false));

        rvAdones.setAdapter(checkoutAddonAdapter);
        rvSizes.setAdapter(sizesAdapter);


        sizesAdapter.addAll(datum.getSizes());


        if (sizesAdapter.getItemCount() > 0) {
            layoutSizes.setVisibility(View.VISIBLE);
        } else {
            layoutSizes.setVisibility(View.GONE);
        }
        if (orderDetail != null && orderDetail.getExtras() != null) {

        } else {
            checkoutAddonAdapter.addAll(datum.getmGroup());
            checkoutAddonAdapter.notifyDataSetChanged();
        }

        tvprice.setText("£ :" + datum.getPrice());
        prodictname.setText(datum.getName());
        description.setText(datum.getDescription());
        totalamount.setText("Total Amount: £" + (1 * (Float.valueOf(datum.getPrice()))));
        final int[] count = {0};
        count[0] = 1;
        counter.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                count[0] = Integer.valueOf(newValue);
                totalamount.setText("Total Amount: £" + (newValue * Float.valueOf(datum.getPrice())));


            }
        });
        for (int i = 0; i < sizesAdapter.getSizes().size(); i++) {
            if (orderDetail != null && orderDetail.getExtras() != null) {
                for (int k = 0; k < orderDetail.getExtras().size(); k++) {

                    if (orderDetail.getExtras().get(k).getChoice().equalsIgnoreCase(sizesAdapter.getSizes().get(i).getSize())) {
                        sizesAdapter.getSizes().get(i).setIs_checked(true);
                        sizesAdapter.lastCheckedPosition = i;
                        sizesAdapter.notifyDataSetChanged();
                        break;
                        //sizesAdapter.getSizes().get(i).setIs_checked(true);
                    }
                }
            }
        }
        List<MenuResponse.Group> groups = datum.getmGroup();
        for (int i = 0; i < groups.size(); i++) {
            for (int j = 0; j < groups.get(i).getChoices().size(); j++) {
                if (orderDetail != null && orderDetail.getExtras() != null) {
                    for (int k = 0; k < orderDetail.getExtras().size(); k++) {
                        if (orderDetail.getExtras().get(k).getChoice().equalsIgnoreCase(groups.get(i).getChoices().get(j).getName())) {
                            groups.get(i).getChoices().get(j).setIs_checked(true);

                            /// checkoutAddonAdapter.setubCheckoutAdonAdapterPosition(j);
                            break;
                            //sizesAdapter.getSizes().get(i).setIs_checked(true);
                        } else {
                            groups.get(i).getChoices().get(j).setIs_checked(false);
                        }
                    }
                }
            }

        }
        if (orderDetail != null && orderDetail.getExtras() != null) {
            checkoutAddonAdapter.addAll(groups);
            checkoutAddonAdapter.notifyDataSetChanged();
        }


        TextView dialogButton = (TextView) dialog.findViewById(R.id.addItem);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deletedposition != -1) {
                    posCartAdapter.remove(deletedposition);
                    deletedposition = -1;

                }
                ArrayList<OrderPlaceModel.Extra> extras = new ArrayList<>();
                OrderPlaceModel.OrderDetail detail = new OrderPlaceModel.OrderDetail();

                detail.setProductName(datum.getName());
                detail.setProductId(datum.getId());
                detail.setQuantity(Long.valueOf(count[0]));
                detail.setPrice(Double.valueOf(datum.getPrice()));
                detail.setSingleProductTotalAmount(Double.valueOf(String.valueOf(count[0])) * Double.valueOf(datum.getPrice()));
                detail.setProductType("product");
                detail.setSpecial_instructions(etSpecialInstruction.getText() != null ? etSpecialInstruction.getText().toString() : "");

                for (int i = 0; i < sizesAdapter.getSizes().size(); i++) {
                    if (sizesAdapter.getSizes().get(i).isIs_checked()) {
                        OrderPlaceModel.Extra details = new OrderPlaceModel.Extra();
                        details.setChoice(sizesAdapter.getSizes().get(i).getSize());
                        details.setPrice(sizesAdapter.getSizes().get(i).getPrice());
                        details.setGroupName("size");
                        extras.add(details);
                      //  total_amount = total_amount + Double.valueOf(sizesAdapter.getSizes().get(i).getPrice());

                    }
                }


                ArrayList<MenuResponse.Group> groups = checkoutAddonAdapter.getAdons();
                for (int i = 0; i < groups.size(); i++) {
                    for (int j = 0; j < groups.get(i).getChoices().size(); j++) {
                        if (groups.get(i).getChoices().get(j).isIs_checked()) {
                            OrderPlaceModel.Extra details = new OrderPlaceModel.Extra();
                            details.setChoice(groups.get(i).getChoices().get(j).getName());
                            details.setPrice(groups.get(i).getChoices().get(j).getPrice());
                            details.setGroupName(groups.get(i).getName());
                          //  total_amount = total_amount + Double.valueOf(groups.get(i).getChoices().get(j).getPrice());

                            extras.add(details);

                        }

                    }

                }
//                if (total_amount > 0) {
//                    total_amount = total_amount * Long.valueOf(count[0]);
//                } else {
//                    total_amount = Double.valueOf(datum.getPrice()) * Long.valueOf(count[0]);
//                }
                detail.setExtras(extras);
                orderDetails.add(detail);
                posCartAdapter.add(detail);


                // UIHelper.showAlertDialog(mainActivity, "Success", "Product Added to Cart");
             //   current_amount = current_amount + total_amount;
              //  total_amount = 0;
               // totalAmount.setText("£  " + setdecimal((float) current_amount));

                // orderPlaceModel.setUserId(Long.valueOf(preferenceHelper.getUser().getId().toString()));
                orderPlaceModel.setCvvNumber(null);
                orderPlaceModel.setPayment("COD");
                orderPlaceModel.setDiscountedAmount(Long.valueOf("0"));

                orderPlaceModel.setTotalAmountWithFee(current_amount);
                orderPlaceModel.setOrderDetails(orderDetails);
                current_amount =(posCartAdapter.getCureentCartAmount());
                totalAmount.setText("£  " + setdecimal((float) current_amount));
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    public String setdecimal(float a) {
        NumberFormat formatter = new DecimalFormat("##.##");
        formatter.setRoundingMode(RoundingMode.CEILING);
        return formatter.format(a);
    }

    public void showAdonDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_hold);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        EditText phone = (EditText) dialog.findViewById(R.id.phone);
        TextView submit = (TextView) dialog.findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<OrderPlaceModel> list = new ArrayList<>();
                list = preferenceHelper.getListCategories("holditems");
                orderPlaceModel.setName(phone.getText().toString().length() > 0 ? phone.getText().toString() : "Hold Cart");
                list.add(orderPlaceModel);
                preferenceHelper.putListCategories("holditems", list);
                orderDetails.clear();
                posCartAdapter.addAll(orderDetails);
                //  UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.success), "Cart on hold");
                current_amount = 0;
                total_amount = 0;
                current_amount = current_amount + total_amount;
                totalAmount.setText("£  " + setdecimal((float) current_amount));

                // orderPlaceModel.setUserId(Long.valueOf(preferenceHelper.getUser().getId().toString()));
                orderPlaceModel.setCvvNumber(null);
                orderPlaceModel.setPayment("COD");
                orderPlaceModel.setDiscountedAmount(Long.valueOf("0"));

                orderPlaceModel.setTotalAmountWithFee(current_amount);
                orderPlaceModel.setOrderDetails(orderDetails);

                dialog.dismiss();

//                } else {
//                    UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Enter customer name");
//                }

            }
        });


        dialog.show();

    }

    public void setPostalCodeandPhoneNumber(String phoneNumber, String postalCode, String name, String orderType) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.orderType = orderType;
    }

    public void setdata(OrderPlaceModel detailArrayList) {
        this.orderPlaceModel = detailArrayList;


    }

    OrderPlaceModel.OrderDetail orderDetail;
    int deletedposition = -1;

    @Override
    public void onDeleteOption(int pos) {
        orderPlaceModel.getOrderDetails().remove(pos);

       current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("£  " + setdecimal((float) current_amount));

    }

    @Override
    public void onIncrese() {
        current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("£  " + setdecimal((float) current_amount));
    }

    @Override
    public void onDecrese() {
        current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("£  " + setdecimal((float) current_amount));
    }

    boolean is_edit = false;

    @Override
    public void onClickOption(OrderPlaceModel.OrderDetail orderDetail, int deletedposition) {
        this.deletedposition = deletedposition;
        this.orderDetail = orderDetail;
        is_edit =true;
        serviceHelper.enqueueCall(webService.getProductData(orderDetail.getProductId()), AppConstant.GET_PRODUCT_DATA);
        //showAdonDialog(mainActivity,null,orderDetail);
    }
}