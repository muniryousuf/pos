package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.models.Resturant.ResturantResponse;
import com.application.ordermanagement.models.shopstatus.ShopStatusResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class GeneralSettings extends BaseFragment {

    Unbinder unbinder;


    @BindView(R.id.swAutoPRint)
    Switch swAutoPRint;
    @BindView(R.id.cvResturantinfo)
    CardView cvResturantinfo;
    @BindView(R.id.cvPRinters)
    CardView cvPRinters;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.btnSubmittwo)
    Button btnSubmittwo;
    @BindView(R.id.btnSubmitthree)
    Button btnSubmitthree;
    @BindView(R.id.btnSubmitfour)
    Button btnSubmitfour;
    @BindView(R.id.btnSubmitfive)
    Button btnSubmitfive;

    @BindView(R.id.timeClosing)
    TimePicker timePickerClosing;

    @BindView(R.id.timeOpening)
    TimePicker timePickerOpening;


    @BindView(R.id.etIp)
    EditText etIp;


    @BindView(R.id.etIptwo)
    EditText etIptwo;


    @BindView(R.id.etIpthree)
    EditText etIpthree;


    @BindView(R.id.etIpfour)
    EditText etIpfour;

    @BindView(R.id.etIpfive)
    EditText etIpfive;
    @BindView(R.id.done)
    Button done;
    private Calendar calendar;
    private String format = "";
    private String opening_time = null;
    private String closing_time = null;

    public static GeneralSettings newInstance() {
        Bundle args = new Bundle();
        GeneralSettings fragment = new GeneralSettings();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setTitle("General Settings");
        titlebar.showBackButton(mainActivity);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    public void clickListners() {
        cvResturantinfo.setOnClickListener(v -> {

            mainActivity.addFragment(new ResturantInfoFragment(),true,true);
        });    cvPRinters.setOnClickListener(v -> {

            mainActivity.addFragment(new PrintersFragment(),true,true);
        });

        btnSubmit.setOnClickListener(v -> {
            if (Patterns.IP_ADDRESS.matcher(etIp.getText().toString()).matches() || etIp.getText().toString().trim().length() == 0) {
                serviceHelper.enqueueCall(webService.updateIp(etIp.getText().toString()), AppConstant.SHOP_STATUS);
            } else {

                Toast.makeText(mainActivity, "Invalid Ip Address", Toast.LENGTH_SHORT).show();
            }
        });
        btnSubmittwo.setOnClickListener(v -> {
            if (Patterns.IP_ADDRESS.matcher(etIptwo.getText().toString()).matches() || etIptwo.getText().toString().trim().length() == 0) {
                serviceHelper.enqueueCall(webService.updateIptwo(etIptwo.getText().toString()), AppConstant.SHOP_STATUS_TWO);
            } else {
                Toast.makeText(mainActivity, "Invalid Ip Address", Toast.LENGTH_SHORT).show();
            }
        });
        btnSubmitthree.setOnClickListener(v -> {
            if (Patterns.IP_ADDRESS.matcher(etIpthree.getText().toString()).matches() || etIpthree.getText().toString().trim().length() == 0) {
                serviceHelper.enqueueCall(webService.updateIpthree(etIpthree.getText().toString()), AppConstant.SHOP_STATUS_THREE);
            } else {

                Toast.makeText(mainActivity, "Invalid Ip Address", Toast.LENGTH_SHORT).show();
            }
        });
        btnSubmitfour.setOnClickListener(v -> {
            if (Patterns.IP_ADDRESS.matcher(etIpfour.getText().toString()).matches() || etIpfour.getText().toString().trim().length() == 0) {
                serviceHelper.enqueueCall(webService.updateIpfour(etIpfour.getText().toString()), AppConstant.SHOP_STATUS_FOUR);
            } else {
                Toast.makeText(mainActivity, "Invalid Ip Address", Toast.LENGTH_SHORT).show();
            }
        });
        btnSubmitfive.setOnClickListener(v -> {
            if (Patterns.IP_ADDRESS.matcher(etIpfive.getText().toString()).matches() || etIpfive.getText().toString().trim().length() == 0) {
                serviceHelper.enqueueCall(webService.updateIpfive(etIpfive.getText().toString()), AppConstant.SHOP_STATUS_FIVE);
            } else {
                Toast.makeText(mainActivity, "Invalid Ip Address", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_settings, container, false);
        unbinder = ButterKnife.bind(this, view);

        clickListners();
        swAutoPRint.setChecked(preferenceHelper.getAutoPrintStatus() ? true : false);

        swAutoPRint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (preferenceHelper.getAutoPrintStatus()) {
                    preferenceHelper.setAutoPrintStatus(false);
                    swAutoPRint.setChecked(false);

                } else {

                    preferenceHelper.setAutoPrintStatus(true);
                    swAutoPRint.setChecked(true);
                }
            }
        });


        if (preferenceHelper.getIp() != null) {
            etIp.setText(preferenceHelper.getIp());
        }
        if (preferenceHelper.getIptwo() != null) {
            etIptwo.setText(preferenceHelper.getIptwo());
        }
        if (preferenceHelper.getIpthree() != null) {
            etIpthree.setText(preferenceHelper.getIpthree());
        }
        if (preferenceHelper.getIpfour() != null) {
            etIpfour.setText(preferenceHelper.getIpfour());
        }
        if (preferenceHelper.getIpfive() != null) {
            etIpfive.setText(preferenceHelper.getIpfive());
        }

        if (preferenceHelper.getOpening_time() != null && !preferenceHelper.getOpening_time().equals("")) {

            opening_time = preferenceHelper.getOpening_time();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(opening_time);
            } catch (ParseException e) {
            }
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            // TimePicker picker = new TimePicker(mainActivity);
            timePickerOpening.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
            timePickerOpening.setCurrentMinute(c.get(Calendar.MINUTE));

        }

        if (preferenceHelper.getClosing_time() != null && !preferenceHelper.getClosing_time().equals("")) {
            closing_time = preferenceHelper.getClosing_time();

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(closing_time);
            } catch (ParseException e) {
            }
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            // TimePicker picker = new TimePicker(mainActivity);
            timePickerClosing.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
            timePickerClosing.setCurrentMinute(c.get(Calendar.MINUTE));

        }
        timePickerOpening.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                int hour = view.getCurrentHour();
                int min = view.getCurrentMinute();
                setTimePickerOpening(hour, min);
            }
        });
        timePickerClosing.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                int hour = view.getCurrentHour();
                int min = view.getCurrentMinute();
                setTimePickerClosing(hour, min);
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (opening_time != null && closing_time != null) {

                    Date date = null;
                    String output = null;

                    Date dateclosing = null;
                    String outputclosing = null;
                    try {
                        //Converting the input String to Date
                        DateFormat df = new SimpleDateFormat("hh:mm aa");
                        DateFormat outputformat = new SimpleDateFormat("HH:mm:ss");

                        date = df.parse(opening_time);
                        //Changing the format of date and storing it in String
                        opening_time = outputformat.format(date);

                        dateclosing = df.parse(closing_time);
                        closing_time = outputformat.format(dateclosing);
                        //Displaying the date
                        //  System.out.println(output);
                    } catch (ParseException pe) {
                        pe.printStackTrace();
                    }
                    serviceHelper.enqueueCall(webService.timings(opening_time, closing_time), AppConstant.TIME_SET);
                } else {
                    Toast.makeText(mainActivity, "Set Time first", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }

    public void setTimePickerOpening(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        opening_time = new StringBuilder().append(hour).append(":").append(min)
                .append(" ").append(format).toString();
    }

    public void setTimePickerClosing(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        closing_time = new StringBuilder().append(hour).append(":").append(min)
                .append(" ").append(format).toString();

    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.TIME_SET:
                ShopStatusResponse response = (ShopStatusResponse) result;
                preferenceHelper.setOpening_time(response.getData().getOpening_time());
                preferenceHelper.setClosing_time(response.getData().getClosing_time());

                // preferenceHelper.setIp(shopStatusResponse.getData().getmPrinterIp1());
                Toast.makeText(mainActivity, "Time set successfully", Toast.LENGTH_SHORT).show();
                break;

            case AppConstant.SHOP_STATUS:
                ShopStatusResponse shopStatusResponse = (ShopStatusResponse) result;
                preferenceHelper.setIp(shopStatusResponse.getData().getmPrinterIp1());
                Toast.makeText(mainActivity, "IP Address Updated Successfully", Toast.LENGTH_SHORT).show();
                break;
            case AppConstant.SHOP_STATUS_TWO:
                ShopStatusResponse shopStatusResponsetwo = (ShopStatusResponse) result;
                preferenceHelper.setIptwo(shopStatusResponsetwo.getData().getmPrinterIp2());
                Toast.makeText(mainActivity, "IP Address Updated Successfully", Toast.LENGTH_SHORT).show();
                break;
            case AppConstant.SHOP_STATUS_THREE:
                ShopStatusResponse shopStatusResponsethree = (ShopStatusResponse) result;
                preferenceHelper.setIpthree(shopStatusResponsethree.getData().getmPrinterIp3());
                Toast.makeText(mainActivity, "IP Address Updated Successfully", Toast.LENGTH_SHORT).show();
                break;
            case AppConstant.SHOP_STATUS_FOUR:
                ShopStatusResponse shopStatusResponsefour = (ShopStatusResponse) result;
                preferenceHelper.setIpfour(shopStatusResponsefour.getData().getmPrinterIp4());
                Toast.makeText(mainActivity, "IP Address Updated Successfully", Toast.LENGTH_SHORT).show();
                break;
            case AppConstant.SHOP_STATUS_FIVE:
                ShopStatusResponse shopStatusResponsefive = (ShopStatusResponse) result;
                preferenceHelper.setIpfive(shopStatusResponsefive.getData().getmPrinterIp5());
                Toast.makeText(mainActivity, "IP Address Updated Successfully", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
