package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddGroupFragment extends BaseFragment {
    Unbinder unbinder;

    @BindView(R.id.submmit)
    Button submmit;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.etname)
    EditText etname;

    @BindView(R.id.displaytype)
    RadioGroup displaytype;

    @BindView(R.id.type)
    RadioGroup type;

    @BindView(R.id.optional)
    RadioButton optional;
    @BindView(R.id.mandatory)
    RadioButton mandatory;

    @BindView(R.id.single)
    RadioButton single;
    @BindView(R.id.multiple)
    RadioButton multiple;


    String dType = "";
    String singlemultiple = "";
    ArrayList<String> items = new ArrayList<>();
    String name;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Choices and Adons");

    }

    AddOnsResponse.Datum datum;

    public void setdata(AddOnsResponse.Datum datum) {
        this.datum = datum;

    }

    public static AddGroupFragment newInstance() {
        Bundle args = new Bundle();
        AddGroupFragment fragment = new AddGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_group, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        if (datum != null) {

            etname.setText(datum.getName());
            dType = datum.getType();
            singlemultiple = datum.getDisplayType();
            if (dType.equals("Optional")) {
                optional.setChecked(true);
                mandatory.setChecked(false);

            } else {
                optional.setChecked(false);
                mandatory.setChecked(true);
            }

            if (singlemultiple.equals("single")) {
                single.setChecked(true);
                multiple.setChecked(false);
            } else {
                single.setChecked(false);
                multiple.setChecked(true);
            }
        }

        submmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etname.getText().toString().length() > 0) {

                    if (!dType.equals("")) {
                        if (!singlemultiple.equals("")) {
                            UIHelper.hideSoftKeyboards(mainActivity);
                            if (datum != null) {
                                serviceHelper.enqueueCall(webService.UpdateGroup(datum.getId(), etname.getText().toString(), dType, singlemultiple), AppConstant.UPDATE_GROUP);

                            } else {

                                serviceHelper.enqueueCall(webService.AddGroup(etname.getText().toString(), dType, singlemultiple), AppConstant.ADD_GROUP);
                            }

                        } else {
                            Toast.makeText(mainActivity, "Select Display type", Toast.LENGTH_SHORT).show();


                        }

                    } else {
                        Toast.makeText(mainActivity, "Select Group type", Toast.LENGTH_SHORT).show();


                    }
                    //EventBus.getDefault().post(etname.getText().toString());


                } else {
                    Toast.makeText(mainActivity, "Enter Name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.optional:
                        dType = "Optional";
                        // do operations specific to this selection
                        break;
                    case R.id.mandatory:
                        dType = "Mandatory";

                        // do operations specific to this selection
                        break;

                }
            }
        });

        displaytype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.single:
                        singlemultiple = "single";
                        // do operations specific to this selection
                        break;
                    case R.id.multiple:
                        singlemultiple = "multiple";

                        // do operations specific to this selection
                        break;

                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                mainActivity.popFragment();
            }
        });

        return view;
    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.ADD_GROUP:
                mainActivity.popFragment();
                break;
            case AppConstant.UPDATE_GROUP:
                mainActivity.popFragment();
                break;


        }

    }


}