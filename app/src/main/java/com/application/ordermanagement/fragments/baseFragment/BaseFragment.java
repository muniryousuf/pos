package com.application.ordermanagement.fragments.baseFragment;

import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;


import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.BasePreferenceHelper;
import com.application.ordermanagement.helper.ServiceHelper;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.webServiceResponseLisener;
import com.application.ordermanagement.webservice.WebServiceFactory;
import com.application.ordermanagement.webservice.webservice;


abstract public class BaseFragment extends Fragment implements webServiceResponseLisener {
    protected ServiceHelper serviceHelper;
    protected webservice webService;

   public MainActivity mainActivity;
    public BasePreferenceHelper preferenceHelper;

    public BaseFragment() {
    }

    protected abstract void setTitle(Titlebar titlebar);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webService = WebServiceFactory.getInstance(AppConstant.BASE_URL);
        serviceHelper = new ServiceHelper(this, getActivity());

        setPreferenceHelper();
    }

    public void setPreferenceHelper() {

        if (mainActivity != null) {

            preferenceHelper = getMainActivity().prefHelper;
        }
        if (preferenceHelper == null && mainActivity != null) {
            preferenceHelper = new BasePreferenceHelper(mainActivity);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity)
            mainActivity = (MainActivity) context;


    }

    @Override
    public void onResume() {
        super.onResume();
        if (getMainActivity() != null && getMainActivity().getDrawerLayout() != null) {
            getMainActivity().lockDrawer();
        }
       if (getActivity() instanceof MainActivity) {
            setTitle(((MainActivity) getActivity()).getTitleBar());
        }
    }

    private MainActivity getMainActivity() {
        if (getActivity() instanceof MainActivity)
            return (MainActivity) getActivity();
        else return null;
    }


    public void closeDrawer() {
        if (mainActivity != null) {
            mainActivity.closeDrawer();
        }

    }

    protected void willbeimplementedinfuture() {
        UIHelper.showToast(getActivity(), "Will be implemented in Next Module");
    }

    protected void replaceFragment(BaseFragment frag, boolean isAddToBackStack, boolean animate) {

            mainActivity.replaceFragment(frag, isAddToBackStack, animate);
        UIHelper.hideSoftKeyboards(mainActivity);

    }


    public void popBackStackTillEntry(int entryIndex) {
        if (mainActivity != null) {
            mainActivity.popBackStackTillEntry(entryIndex);
        }
    }

    public void popFragment() {
        if (mainActivity != null) {
            mainActivity.popFragment();
        }
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
    }

    @Override
    public void ResponseFailure(String tag) {
    }
}
