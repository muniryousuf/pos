package com.application.ordermanagement.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.AddChoicesListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddChoicesListingFragment extends BaseFragment implements AddChoicesListingAdapter.ISubCategoryProductAdapter {
    Unbinder unbinder;
    AddChoicesListingAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;
    @BindView(R.id.addchoices)
    LinearLayout addchoices;


    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Add Choices");

    }

    public static AddChoicesListingFragment newInstance() {
        Bundle args = new Bundle();
        AddChoicesListingFragment fragment = new AddChoicesListingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choices_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        productListingAdapter = new AddChoicesListingAdapter(this, mainActivity, true,false);
        rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvProducts.setAdapter(productListingAdapter);
        setData();
        addchoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.replaceFragment(new AddGroupFragment(), true, true);
            }
        });
        return view;
    }

    private void setData() {
        serviceHelper.enqueueCall(webService.getAdons(), AppConstant.GET_ADONS);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.GET_ADONS:
                AddOnsResponse addOnsResponse = (AddOnsResponse) result;
                productListingAdapter.addAll(addOnsResponse.getData());
                break;

            case AppConstant.DELETE_GROUP:
                //setData();
                break;

        }

    }


    @Override
    public void onClickAddChoices(AddOnsResponse.Datum position) {
        ChoiceFragment choiceFragment = new ChoiceFragment();
        choiceFragment.setname(position);
        mainActivity.replaceFragment(choiceFragment, true, true);
    }

    @Override
    public void onClickEdit(AddOnsResponse.Datum position) {
        AddGroupFragment addGroupFragment = new AddGroupFragment();
        addGroupFragment.setdata(position);
        mainActivity.replaceFragment(addGroupFragment, true, true);
    }

    @Override
    public void onClickDelete(AddOnsResponse.Datum position) {
        serviceHelper.enqueueCall(webService.deleteGroup(position.getId()), AppConstant.DELETE_GROUP);


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String name) {
        UIHelper.hideSoftKeyboards(mainActivity);
        UIHelper.showAlertDialog(mainActivity, getString(R.string.success), name + " Added Successfully");
        mainActivity.popFragment();
        //  productListingAdapter.add(name);
    }
}