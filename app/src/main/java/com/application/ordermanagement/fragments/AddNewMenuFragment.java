package com.application.ordermanagement.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.adapter.CheckoutCartAdapter;
import com.application.ordermanagement.adapter.ChoiceAdapter;
import com.application.ordermanagement.adapter.ProductMenuAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddNewMenuFragment extends BaseFragment implements PaginationAdapterCallback, ChoiceAdapter.ISubCategoryProductAdapter, ProductMenuAdapter.ISubCategoryProductAdapter {
    Unbinder unbinder;
    //ChoiceAdapter productListingAdapter;
    ProductMenuAdapter productMenuAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;
    @BindView(R.id.addchoices)
    LinearLayout addchoices;
    @BindView(R.id.tvChoice)
    TextView tvChoice;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottom_sheet;
    @BindView(R.id.rvCart)
    RecyclerView rvCart;
    @BindView(R.id.btnPayment)
    Button btnPayment;

    CheckoutCartAdapter checkoutCartAdapter;


    BottomSheetBehavior sheetBehavior;
    //  ArrayList<String> items ;
    List<MenuResponse.Product> list;
    String name;
    String category_id;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle(pos ? "Products" : "Choices and Adons");

    }

    public static AddNewMenuFragment newInstance() {
        Bundle args = new Bundle();
        AddNewMenuFragment fragment = new AddNewMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    boolean pos = false;

    public void setPos(boolean pos) {
        this.pos = pos;

    }

    public void setname(List<MenuResponse.Product> list, String name, String category_id) {

        this.list = list;
        this.name = name;
        this.category_id = category_id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choices, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView


        tvChoice.setText("Add Menu to " + name);
        addchoices.setVisibility(pos ? View.GONE : View.VISIBLE);
       // bottom_sheet.setVisibility(pos ? View.VISIBLE : View.GONE);
        checkoutCartAdapter =new CheckoutCartAdapter(mainActivity);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(checkoutCartAdapter);
        checkoutCartAdapter.notifyDataSetChanged();
//        productListingAdapter = new ChoiceAdapter(this, mainActivity, true);
//        rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));
//        rvProducts.setAdapter(productListingAdapter);
        productMenuAdapter = new ProductMenuAdapter(this, mainActivity, this, true, pos);
        rvProducts.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvProducts.setAdapter(productMenuAdapter);
        setData();
        addchoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSubNewMenuFragment addNewMenuFragment = new AddSubNewMenuFragment();
                addNewMenuFragment.setCategoryId(category_id);
                mainActivity.replaceFragment(addNewMenuFragment, true, true);
            }
        }); btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mainActivity.addFragment(new AddressFragment(),true,true);
            }
        });
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setHideable(false);
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        //      btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        return view;
    }

    private void setData() {
        getMenu();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.GET_ALL_MENUS:
                MenuResponse menuResponse = (MenuResponse) result;
                for (int i = 0; i < menuResponse.getData().size(); i++) {
                    if (String.valueOf(menuResponse.getData().get(i).getId()).equals(category_id)) {
                        productMenuAdapter.addAll(menuResponse.getData().get(i).getProducts());
                        productMenuAdapter.notifyDataSetChanged();
                        break;
                    }

                }

                break;
            case AppConstant.UPDATE_PRODUCTS:


                // serviceHelper.enqueueCall(webService.getAllproducts(),AppConstant.ALL_PRODUCTS);
                break;
            case AppConstant.DELETE_PRODUCT:
                getMenu();
                break;

                case AppConstant.DELETE_ADON:
                getMenu();
                break;
        }

    }


    //    @Override
//    public void onClick(String position) {
//        AddNewMenuFragment choiceFragment = new AddNewMenuFragment();
//        choiceFragment.setname(position);
//        mainActivity.replaceFragment(choiceFragment, true, true);
//    }
    int i = 0;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String name) {
        popFragment();
        getMenu();

    }

    public void getMenu() {
        serviceHelper.enqueueCall(webService.getMenu(), AppConstant.GET_ALL_MENUS);
    }

    @Override
    public void onClickAddMenu(MenuResponse.Product position) {

    }

    @Override
    public void updatePrice(int id, String price) {
        serviceHelper.enqueueCall(webService.updateproduct(id, price), AppConstant.UPDATE_PRODUCTS);

    }

    @Override
    public void deleteSubMenu(Long id) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        serviceHelper.enqueueCall(webService.deleteProduct(id), AppConstant.DELETE_PRODUCT);
                        dialog.dismiss();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    @Override
    public void edit(MenuResponse.Product datum) {
            AddSubNewMenuFragment addSubNewMenuFragment =new AddSubNewMenuFragment();
            addSubNewMenuFragment.setCategoryId(category_id);
            addSubNewMenuFragment.setdata(datum);
            mainActivity.replaceFragment(addSubNewMenuFragment,true,true);
    }

    @Override
    public void editAdon(MenuResponse.Product datum) {

        showAlertDialog(datum);
    }
    int positon=-1;
    private void showAlertDialog(MenuResponse.Product data) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle("Click Group to Remove");


        String[] items ;
        items =new String[data.getmGroup().size()];


        for(int i =0; i<data.getmGroup().size();i++){
            items[i]= data.getmGroup().get(i).getName();
        }

        alertDialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positon =which;
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                RemoveAdonRequest  removeAdonRequest =new RemoveAdonRequest(data.getId(), data.getmGroup().get(positon).getmId());
                                serviceHelper.enqueueCall(webService.RemoveAdon(removeAdonRequest), AppConstant.DELETE_ADON);
                                dialog.cancel();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                dialog.cancel();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
                builder.setMessage("Are you sure to delete?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();







            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    public class RemoveAdonRequest{
        public RemoveAdonRequest(Long id_product, Long id_group) {
            this.id_product = id_product;
            this.id_group = id_group;
        }

        Long id_product;

        public Long getId_product() {
            return id_product;
        }

        public void setId_product(Long id_product) {
            this.id_product = id_product;
        }

        public Long getId_group() {
            return id_group;
        }

        public void setId_group(Long id_group) {
            this.id_group = id_group;
        }

        Long id_group;

    }
    @Override
    public void clickmenu(MenuResponse.Product datum) {

    }

    @Override
    public void updateAvailibity(int id, String price, int status) {
        serviceHelper.enqueueCall(webService.updateproductAvailibity(id, price, status), AppConstant.UPDATE_PRODUCTS);

    }

    @Override
    public void addtocart() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottom_sheet.setVisibility(pos?View.VISIBLE:View.GONE);
    }

    @Override
    public void retryPageLoad() {

    }
}