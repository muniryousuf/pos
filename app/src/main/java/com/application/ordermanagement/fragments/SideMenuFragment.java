package com.application.ordermanagement.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.ordermanagement.R;
import com.application.ordermanagement.customViews.CustomRecyclerView;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.BasePreferenceHelper;
import com.application.ordermanagement.helper.Titlebar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SideMenuFragment extends BaseFragment {
    //    @BindView(R.id.img_background)
//    CircleImageView imgBackground;
//    @BindView(R.id.txt_username)
//    TextView txtUsername;
//    @BindView(R.id.txt_useraddress)
//    TextView txtUseraddress;
    @BindView(R.id.sideoptions)
    CustomRecyclerView rvSideMenuOptions;
    Unbinder unbinder;
    private ArrayList<String> sideMenuOptions;

    // TermsAndConditon termsAndConditon;
//    private RecyclerItemClickListener menuItemClickListener = ((ent, position, id) -> {
//        if ((ent).equals(getString(R.string.home))) {
//            // closeDrawer();
//            popBackStackTillEntry(0);
//            replaceFragment(new HomeFragment(), true, true);
//
//        } else if ((ent).equals(getString(R.string.orderHistory))) {
//            if(!preferenceHelper.getLoginStatus()){
//                UIHelper.showToast(getActivity(), getActivity().getResources().getString(R.string.please_login_notification));
//            }else{
//                if(preferenceHelper.getUser() != null){
//                    replaceFragment(new OrdersHistoryFragment(), true, false);
//                }
//            }
//
//        } else if ((ent).equals(getString(R.string.changePassword))) {
//            if (preferenceHelper != null && preferenceHelper.getUser() != null) {
//                replaceFragment(new ChangePasswordFragment(), true, true);
//            } else {
//                UIHelper.showToast(getActivity(), getActivity().getResources().getString(R.string.please_login_notification));
//            }
//
//
//        } else if ((ent).equals(getString(R.string.aboutUs))) {
//
//            termsAndConditon = new TermsAndConditon();
//            termsAndConditon.setkey("About Us");
//            replaceFragment(termsAndConditon, true, true);
//
//        } else if ((ent).equals(getString(R.string.rateApp))) {
//            willbeimplementedinfuture();
//
//        } else if ((ent).equals(getString(R.string.termCondition))) {
//            termsAndConditon = new TermsAndConditon();
//            termsAndConditon.setkey("Terms and Condition");
//            replaceFragment(termsAndConditon, true, true);
//
//
//        } else if ((ent).equals(getString(R.string.privacyPolicy))) {
//            termsAndConditon = new TermsAndConditon();
//            termsAndConditon.setkey("Privacy Policy");
//            replaceFragment(termsAndConditon, true, true);
//
//        } else if ((ent).equals(getString(R.string.login)) || (ent).equals(getString(R.string.logout))) {
//
//            if ((ent).equals(getString(R.string.login))) {
//
//
//                logout();
//            } else {
//
//                DialogFactory.createQuitDialog(mainActivity, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        logoutService();
//                                    /* activityReference.clearStack();
//                                    activityReference.initFragments(new LoginFragment());
//                                    preferenceHelper.putUser(null);
//                                    preferenceHelper.putUserToken(null);*/
//                    }
//                }, R.string.message_logout).show();
//
//
//            }
//            //   mainActivity.showRegistrationActivity();
//            // willbeimplementedinfuture();
//        }
//    });
//
    private void logout() {
        preferenceHelper.putUser(null);
        preferenceHelper.setLoginStatus(false);
//        preferenceHelper.putDeviceToken(null);
        // mainActivity.showRegistrationActivity();
    }


    public static SideMenuFragment newInstance() {
        Bundle args = new Bundle();

        SideMenuFragment fragment = new SideMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindSideMenuOptions();

        //   setListner(preferenceHelper);

    }

    private void bindSideMenuOptions() {
        sideMenuOptions = new ArrayList<>();

//        sideMenuOptions.add(getString(R.string.home));
//        sideMenuOptions.add(getString(R.string.orderHistory));
//        sideMenuOptions.add(getString(R.string.changePassword));
//        sideMenuOptions.add(getString(R.string.aboutUs));
//        sideMenuOptions.add(getString(R.string.rateApp));
//        sideMenuOptions.add(getString(R.string.termCondition));
//        sideMenuOptions.add(getString(R.string.privacyPolicy));
//        if (preferenceHelper.getLoginStatus()) {
//
//            sideMenuOptions.add(getString(R.string.logout));
//        } else {
//
//            sideMenuOptions.add(getString(R.string.login));
//
//        }

        //  rvSideMenuOptions.bindRecyclerView(new SideMenuBinder(menuItemClickListener), sideMenuOptions, new LinearLayoutManager(getActivity()), new DefaultItemAnimator());


    }

    @Override
    public void setTitle(Titlebar titlebar) {
        titlebar.hideTitle();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setListner(final BasePreferenceHelper preferenceHelper) {


        if (preferenceHelper != null && preferenceHelper.getUser() != null) {

//            txtUsername.setText(preferenceHelper.getUser().getFullName());
//            txtUseraddress.setText(preferenceHelper.getUser().getEmail());
//            UIHelper.setImagewithGlide(mainActivity, imgBackground, preferenceHelper.getUser().getProfileImage());

        }


    }


    public void logoutService() {
        //  serviceHelper.enqueueCall(webService.logout(preferenceHelper.getUser().getId(), preferenceHelper.getDeviceToken()), AppConstant.USER_LOGOUT);

    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.USER_LOGOUT:

//                UIHelper.showToast(mainActivity, getString(R.string.logout_success));
//                logout();
                break;
        }
    }
}