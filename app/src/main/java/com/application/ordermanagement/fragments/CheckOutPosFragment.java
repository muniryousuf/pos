package com.application.ordermanagement.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.POS.POSCartAdapter;
import com.application.ordermanagement.adapter.POS.PosMenuAdapter;
import com.application.ordermanagement.adapter.POS.PosProductMenuAdapter;
import com.application.ordermanagement.adapter.PPAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.model.PlaceOrderResponse.PlaceOrderDataResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CheckOutPosFragment extends BaseFragment implements POSCartAdapter.onClickOptions, PosMenuAdapter.ISubCategoryProductAdapter, PosProductMenuAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;


    @BindView(R.id.rvCart)
    RecyclerView rvCart;

    @BindView(R.id.cash)
    CardView cash;
    @BindView(R.id.creditCard)
    CardView creditCard;
    @BindView(R.id.totalAmount)
    TextView totalAmount;
    @BindView(R.id.totalAmountBottom)
    TextView totalAmountBottom;
    @BindView(R.id.tvTotalAmountBig)
    TextView tvTotalAmountBig;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.tvten)
    TextView tvten;
    @BindView(R.id.tvTwenty)
    TextView tvTwenty;
    @BindView(R.id.tvFifty)
    TextView tvFifty;
    @BindView(R.id.tvFive)
    TextView tvFive;
    @BindView(R.id.tvExact)
    TextView tvExact;
    @BindView(R.id.tvHundred)
    TextView tvHundred;
    @BindView(R.id.printing)
    TextView printing;
    @BindView(R.id.tvPerPerson)
    TextView tvPerPerson;


    POSCartAdapter posCartAdapter;
    boolean is_print = true;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("POS");
        titlebar.hideTitlebar();

    }

    public void setdata(OrderPlaceModel orderPlaceModel, String ordertype) {

        this.orderPlaceModel = orderPlaceModel;
        this.ordertype = ordertype;
    }

    boolean posBoolean = false;

    public void setPos(boolean posBoolean) {
        this.posBoolean = posBoolean;
    }

    public static CheckOutPosFragment newInstance() {
        Bundle args = new Bundle();
        CheckOutPosFragment fragment = new CheckOutPosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    OrderPlaceModel orderPlaceModel;
    String ordertype;
    ArrayList<OrderPlaceModel.OrderDetail> orderDetails = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    GridLayoutManager gridLayoutManager;
    double input1 = 0, input2 = 0;
    TextView edt1;
    boolean Addition, Subtract, Multiplication, Division, mRemainder, decimal;
    Button button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, buttonAdd, buttonSub,
            buttonMul, buttonDivision, buttonEqual, buttonDel, buttonDot, Remainder;

    public String setdecimal(float a) {
        NumberFormat formatter = new DecimalFormat("##.##");
        formatter.setRoundingMode(RoundingMode.CEILING);
        return formatter.format(a);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fargment_sub_pos, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        //  items = new ArrayList<>();
        setViews(view);

        posCartAdapter = new POSCartAdapter(mainActivity, this, false);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(posCartAdapter);
        posCartAdapter.addAll(orderPlaceModel.getOrderDetails());
        posCartAdapter.notifyDataSetChanged();
        totalAmount.setText("Total Amount :   £" + setdecimal(Float.parseFloat(String.valueOf(posCartAdapter.getCureentCartAmount()))));
        tvTotalAmountBig.setText("Total Amount :   £" + setdecimal(Float.parseFloat(String.valueOf(posCartAdapter.getCureentCartAmount()))));
        totalAmountBottom.setText("£  " + setdecimal(Float.parseFloat(String.valueOf(posCartAdapter.getCureentCartAmount()))));
        //  totalAmount.setText("£  " + orderPlaceModel.getTotalAmountWithFee());
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());


        String currentTime = new SimpleDateFormat("HH:mm:ss aa", Locale.getDefault()).format(new Date());
        //   Date dt = new Date(currentDate);
        //   SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        //  String time1 = sdf.format(dt);
        tvTime.setText(currentTime);
        tvDate.setText(currentDate);
        tvPerPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdonDialog(mainActivity);

            }
        });
        tvExact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("" + setdecimal((float) posCartAdapter.getCureentCartAmount()));


            }
        });
        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Double.parseDouble((edt1.getText().toString().equals("") ? String.valueOf(0) : edt1.getText().toString())) >= orderPlaceModel.getTotalAmountWithFee()) {

                    if (Double.parseDouble(edt1.getText().toString()) >= orderPlaceModel.getTotalAmountWithFee()) {
//                        OrderPlaceModel.UserData userData = new OrderPlaceModel.UserData();
//                        userData.setAddress(null);
//                        userData.setName("N/A");
//                        userData.setNumber("N/A");
//                        userData.setEmail("N/A");
//                        userData.setAsap("As soon as possible");
//                        userData.setPaymentType("COD");
//                        userData.setOrderType("Pickup");
//                        orderPlaceModel.setDeliveryFees("0");
//                        orderPlaceModel.setUserData(userData);
//                        orderPlaceModel.setUserId(11);
//                        orderPlaceModel.setOrderType("Pickup");
//                        orderPlaceModel.setDeliveryAddress("--- --- ---");
//                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");
//
//                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
//                        //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
//                        serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mainActivity);

                        // builder1.setMessage("Please Return : £ " + (Double.valueOf(edt1.getText().toString()) - orderPlaceModel.getTotalAmountWithFee() + " to customer"));
                        DecimalFormat f = new DecimalFormat("##.00");

                        builder1.setMessage("Total Amount : £ " + f.format(posCartAdapter.getCureentCartAmount()) + " \n\n" + "Paid Amount : £ " + f.format(Double.valueOf(edt1.getText().toString())) + "\n\nChange Amount :£ " + f.format(Double.valueOf(edt1.getText().toString()) - posCartAdapter.getCureentCartAmount()));
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        OrderPlaceModel.UserData userData = new OrderPlaceModel.UserData();
                                        userData.setAddress(null);
                                        userData.setName("N/A");
                                        userData.setNumber("N/A");
                                        userData.setEmail("N/A");
                                        userData.setAsap("As soon as possible");
                                        userData.setPaymentType("COD");
                                        userData.setOrderType(ordertype);
                                        orderPlaceModel.setDeliveryFees("0");
                                        orderPlaceModel.setUserData(userData);
                                        orderPlaceModel.setUserId(11);
                                        orderPlaceModel.setIs_pos(true);
                                        orderPlaceModel.setOrderType(ordertype);
                                        orderPlaceModel.setDeliveryAddress("--- --- ---");
                                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");

                                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
                                        //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
                                        serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);

                                        dialog.cancel();
                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else {

                    }


                } else {

                    Toast.makeText(mainActivity, "Collect total amount from customer", Toast.LENGTH_SHORT).show();
                }

//
//                AddressFragment addressFragment = new AddressFragment();
//                addressFragment.setdata(orderPlaceModel);
//                mainActivity.addFragment(addressFragment, true, true);


            }
        });
        creditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Double.parseDouble((edt1.getText().toString().equals("") ? String.valueOf(0) : edt1.getText().toString())) >= orderPlaceModel.getTotalAmountWithFee()) {

                    if (Double.parseDouble(edt1.getText().toString()) >= orderPlaceModel.getTotalAmountWithFee()) {
//                        OrderPlaceModel.UserData userData = new OrderPlaceModel.UserData();
//                        userData.setAddress(null);
//                        userData.setName("N/A");
//                        userData.setNumber("N/A");
//                        userData.setEmail("N/A");
//                        userData.setAsap("As soon as possible");
//                        userData.setPaymentType("COD");
//                        userData.setOrderType("Pickup");
//                        orderPlaceModel.setDeliveryFees("0");
//                        orderPlaceModel.setUserData(userData);
//                        orderPlaceModel.setUserId(11);
//                        orderPlaceModel.setOrderType("Pickup");
//                        orderPlaceModel.setDeliveryAddress("--- --- ---");
//                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");
//
//                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
//                        //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
//                        serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(mainActivity);

                        // builder1.setMessage("Please Return : £ " + (Double.valueOf(edt1.getText().toString()) - orderPlaceModel.getTotalAmountWithFee() + " to customer"));
                        DecimalFormat f = new DecimalFormat("##.00");

                        builder1.setMessage("Total Amount : £ " + f.format(posCartAdapter.getCureentCartAmount()) + " \n\n" + "Paid Amount : £ " + f.format(Double.valueOf(edt1.getText().toString())) + "\n\nChange Amount :£ " + f.format(Double.valueOf(edt1.getText().toString()) - posCartAdapter.getCureentCartAmount()));
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        OrderPlaceModel.UserData userData = new OrderPlaceModel.UserData();
                                        userData.setAddress(null);
                                        userData.setName("N/A");
                                        userData.setNumber("N/A");
                                        userData.setEmail("N/A");
                                        userData.setAsap("As soon as possible");
                                        userData.setPaymentType("Credit Card");
                                        userData.setOrderType(ordertype);
                                        orderPlaceModel.setDeliveryFees("0");
                                        orderPlaceModel.setUserData(userData);
                                        orderPlaceModel.setUserId(11);
                                        orderPlaceModel.setOrderType(ordertype);
                                        orderPlaceModel.setDeliveryAddress("--- --- ---");
                                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");

                                        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
                                        //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
                                        serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);

                                        dialog.cancel();
                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else {

                    }


                } else {

                    Toast.makeText(mainActivity, "Collect total amount from customer", Toast.LENGTH_SHORT).show();
                }

//
//                AddressFragment addressFragment = new AddressFragment();
//                addressFragment.setdata(orderPlaceModel);
//                mainActivity.addFragment(addressFragment, true, true);


            }
        });
        tvTwenty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("20");
            }
        });
        tvHundred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("100");
            }
        });
        tvFifty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("50");
            }
        });
        tvten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("10");
            }
        });
        tvFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText("5");
            }
        });
        return view;
    }

    private void setViews(View views) {
        button0 = (Button) views.findViewById(R.id.button0);
        button1 = (Button) views.findViewById(R.id.button1);
        button2 = (Button) views.findViewById(R.id.button2);
        button3 = (Button) views.findViewById(R.id.button3);
        button4 = (Button) views.findViewById(R.id.button4);
        button5 = (Button) views.findViewById(R.id.button5);
        button6 = (Button) views.findViewById(R.id.button6);
        button7 = (Button) views.findViewById(R.id.button7);
        button8 = (Button) views.findViewById(R.id.button8);
        button9 = (Button) views.findViewById(R.id.button9);
        buttonDot = (Button) views.findViewById(R.id.buttonDot);
        buttonAdd = (Button) views.findViewById(R.id.buttonadd);
        buttonSub = (Button) views.findViewById(R.id.buttonsub);
        buttonMul = (Button) views.findViewById(R.id.buttonmul);
        buttonDivision = (Button) views.findViewById(R.id.buttondiv);
        Remainder = (Button) views.findViewById(R.id.Remainder);
        buttonDel = (Button) views.findViewById(R.id.buttonDel);
        buttonEqual = (Button) views.findViewById(R.id.buttoneql);

        edt1 = (TextView) views.findViewById(R.id.display);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "1");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "2");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "3");
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "4");
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "5");
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "6");
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "7");
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "8");
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "9");
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt1.setText(edt1.getText() + "0");
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt1.getText().length() != 0) {
                    input1 = Float.parseFloat(edt1.getText() + "");
                    Addition = true;
                    decimal = false;
                    edt1.setText(null);
                }
            }
        });

        buttonSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt1.getText().length() != 0) {
                    input1 = Float.parseFloat(edt1.getText() + "");
                    Subtract = true;
                    decimal = false;
                    edt1.setText(null);
                }
            }
        });

        buttonMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt1.getText().length() != 0) {
                    input1 = Float.parseFloat(edt1.getText() + "");
                    Multiplication = true;
                    decimal = false;
                    edt1.setText(null);
                }
            }
        });

        buttonDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt1.getText().length() != 0) {
                    input1 = Float.parseFloat(edt1.getText() + "");
                    Division = true;
                    decimal = false;
                    edt1.setText(null);
                }
            }
        });

        Remainder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt1.getText().length() != 0) {
                    input1 = Float.parseFloat(edt1.getText() + "");
                    mRemainder = true;
                    decimal = false;
                    edt1.setText(null);
                }
            }
        });

        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Addition || Subtract || Multiplication || Division || mRemainder) {
                    input2 = Float.parseFloat(edt1.getText() + "");
                }

                if (Addition) {

                    edt1.setText(input1 + input2 + "");
                    Addition = false;
                }

                if (Subtract) {

                    edt1.setText(input1 - input2 + "");
                    Subtract = false;
                }

                if (Multiplication) {
                    edt1.setText(input1 * input2 + "");
                    Multiplication = false;
                }

                if (Division) {
                    edt1.setText(input1 / input2 + "");
                    Division = false;
                }
                if (mRemainder) {
                    edt1.setText(input1 % input2 + "");
                    mRemainder = false;
                }
            }
        });

        buttonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decimal = false;
                edt1.setText("");
                input1 = 0.0;
                input2 = 0.0;
            }
        });

        buttonDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decimal) {
                    //do nothing or you can show the error
                } else {
                    edt1.setText(edt1.getText() + ".");
                    decimal = true;
                }

            }
        });
        printing.setOnClickListener(v -> {
            if (is_print) {
                is_print = false;
            } else {
                is_print = true;
            }
            printing.setText(is_print ? "Print" : "No Print");
        });
    }


    int position = 0;

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.Place_order:
                PlaceOrderDataResponse placeOrderDataResponse = (PlaceOrderDataResponse) result;
                UIHelper.showAlertDialog(mainActivity, "Success", "your order has been placed successfully");
                mainActivity.popFragment();
                mainActivity.popFragment();
                //mainActivity.addFragment(new HomeFragment(), true, true);
                break;
        }
    }


    @Override
    public void clickmenu(MenuResponse.Datum datum, String name) {

    }

    @Override
    public void clickmenudelete(long datum) {


    }

    @Override
    public void retryPageLoad() {

    }


    @Override
    public void updatePrice(int id, String price) {

    }

    @Override
    public void clickmenu(MenuResponse.Product datum) {


    }

    @Override
    public void updateAvailibity(int id, String price, int status) {

    }


    @Override
    public void addtocart() {

    }

    double current_amount = 0;

    @Override
    public void onDeleteOption(int pos) {
        orderPlaceModel.getOrderDetails().remove(pos);

        current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("Total Amount :   £" + setdecimal((float) current_amount));
        tvTotalAmountBig.setText("Total Amount :   £" + setdecimal((float) current_amount));
        totalAmountBottom.setText("£  " + setdecimal((float) current_amount));
    }

    @Override
    public void onIncrese() {
        current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("Total Amount :   £" + setdecimal((float) current_amount));
        tvTotalAmountBig.setText("Total Amount :   £" + setdecimal((float) current_amount));
        totalAmountBottom.setText("£  " + setdecimal((float) current_amount));
    }

    @Override
    public void onDecrese() {
        current_amount = (posCartAdapter.getCureentCartAmount());
        totalAmount.setText("Total Amount :   £" + setdecimal((float) current_amount));
        tvTotalAmountBig.setText("Total Amount :   £" + setdecimal((float) current_amount));
        totalAmountBottom.setText("£  " + setdecimal((float) current_amount));
    }


    @Override
    public void onClickOption(OrderPlaceModel.OrderDetail orderDetail, int i) {

    }

    public void showAdonDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_pp);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        EditText phone = (EditText) dialog.findViewById(R.id.phone);
        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        TextView tvPP = (TextView) dialog.findViewById(R.id.tvPP);
        RecyclerView rvPPPersons = (RecyclerView) dialog.findViewById(R.id.rvPPPersons);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone.getText().toString().length() > 0) {
                    //dialog.dismiss();
                    UIHelper.hideSoftKeyboards(mainActivity);
                    int i = 0;
                    i = Integer.valueOf(phone.getText().toString());
                    float pp_value = (float) (posCartAdapter.getCureentCartAmount() / i);
                    PPAdapter ppAdapter = new PPAdapter(mainActivity);
                    rvPPPersons.setAdapter(ppAdapter);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL);
                    rvPPPersons.setLayoutManager(staggeredGridLayoutManager);
                    rvPPPersons.setItemAnimator(new DefaultItemAnimator());
                    ppAdapter.setItemCount(i, pp_value);

                    phone.setVisibility(View.GONE);
                    tvPP.setVisibility(View.GONE);
                    submit.setVisibility(View.GONE);
                    rvPPPersons.setVisibility(View.VISIBLE);
                    ok.setVisibility(View.VISIBLE);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            placeorder();
                        }
                    });

                } else {
                    UIHelper.showAlertDialog(mainActivity, mainActivity.getString(R.string.error), "Enter Number of Persons");
                }

            }
        });


        dialog.show();

    }

    private void placeorder() {
        OrderPlaceModel.UserData userData = new OrderPlaceModel.UserData();
        userData.setAddress(null);
        userData.setName("N/A");
        userData.setNumber("N/A");
        userData.setEmail("N/A");
        userData.setAsap("As soon as possible");
        userData.setPaymentType("Per Person");
        userData.setOrderType(ordertype);
        orderPlaceModel.setDeliveryFees("0");
        orderPlaceModel.setUserData(userData);
        orderPlaceModel.setUserId(11);
        orderPlaceModel.setIs_pos(true);
        orderPlaceModel.setOrderType(ordertype);
        orderPlaceModel.setDeliveryAddress("--- --- ---");
        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), "{\"user_id\":11,\"total_amount_with_fee\":10.98,\"delivery_fees\":\"0\",\"discounted_amount\":0,\"payment\":\"COD\",\"delivery_address\":\"--- --- ---\",\"order_details\":[{\"product_id\":25,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE OCEAN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]},{\"product_id\":26,\"product_type\":\"product\",\"quantity\":1,\"product_name\":\"THE KLASSIC CHICKEN BURGER\",\"price\":5.49,\"single_product_total_amount\":5.49,\"extras\":[{\"group_name\":\"Add Extras (Burger)\",\"choice\":\"Gourmet Patty\",\"price\":\"2.00\"}]}],\"user_data\":{\"address\":\"---\",\"street\":\"---\",\"town\":null,\"postal_code\":\"---\",\"land_mark\":\"\",\"email\":\"ASD@SADAS.COM\",\"number\":\"12345678910\",\"name\":\"ASDASD\",\"payment_type\":\"COD\",\"order_type\":\"Pickup\",\"card_holder_name\":\"\",\"card_number\":\"\",\"expiration_month\":\"\",\"expiration_year\":\"\",\"cvc\":\"\",\"deliveryTime\":\"Tuesday 12:50\",\"asap\":\"\"},\"order_type\":\"Pickup\",\"card_no\":\"\",\"ccExpiryMonth\":\"\",\"ccExpiryYear\":\"\",\"cvvNumber\":\"\"}");

        // RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), orderPlaceModel.toString());
        //  OrderPlaceRequest p = g.fromJson(request, OrderPlaceRequest.class);
        serviceHelper.enqueueCall(webService.placeOrderResponse(orderPlaceModel), AppConstant.Place_order);

    }

}