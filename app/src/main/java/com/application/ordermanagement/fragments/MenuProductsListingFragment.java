package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.ProductMenuAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MenuProductsListingFragment extends BaseFragment implements ProductMenuAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;


    @BindView(R.id.rvmenu)
    RecyclerView rvmenu;

    ProductMenuAdapter menuAdapter;
    List<MenuResponse.Product> list;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.VISIBLE);
        titlebar.setTitle("Products");
        titlebar.showBackButton(mainActivity);
    }

    public static MenuProductsListingFragment newInstance() {
        Bundle args = new Bundle();

        MenuProductsListingFragment fragment = new MenuProductsListingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public void setData(List<MenuResponse.Product> list) {
        this.list = list;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        menuAdapter = new ProductMenuAdapter(this, mainActivity, this, true,false);
        rvmenu.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvmenu.setAdapter(menuAdapter);
        menuAdapter.addAll(list);
        menuAdapter.notifyDataSetChanged();


        return view;
    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {

            case AppConstant.UPDATE_PRODUCTS:


                // serviceHelper.enqueueCall(webService.getAllproducts(),AppConstant.ALL_PRODUCTS);
                break;
        }
    }


    @Override
    public void updatePrice(int id, String price) {
        serviceHelper.enqueueCall(webService.updateproduct(id,price),AppConstant.UPDATE_PRODUCTS);


    }

    @Override
    public void deleteSubMenu(Long id) {

    }

    @Override
    public void edit(MenuResponse.Product datum) {

    }

    @Override
    public void editAdon(MenuResponse.Product datum) {

    }


    @Override
    public void clickmenu(MenuResponse.Product datum) {
    }

    @Override
    public void updateAvailibity(int id, String price,int status) {
        serviceHelper.enqueueCall(webService.updateproductAvailibity(id, price,status), AppConstant.UPDATE_PRODUCTS);

    }

    @Override
    public void addtocart() {

    }

    @Override
    public void retryPageLoad() {
    }
}


