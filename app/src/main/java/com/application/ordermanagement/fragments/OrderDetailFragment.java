package com.application.ordermanagement.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.application.ordermanagement.BuildConfig;
import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.OrderHistoryProductAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.PrinterCommands;
import com.application.ordermanagement.helper.Spanny;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.Utils;
import com.application.ordermanagement.models.OrdersResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.BitSet;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.support.constraint.Constraints.TAG;

public class OrderDetailFragment extends BaseFragment implements
        AdapterView.OnItemSelectedListener {
    Unbinder unbinder;
    @BindView(R.id.rvProducts)
    RecyclerView recyclerView;
    @BindView(R.id.lyDownloadPDF)
    RelativeLayout lyDownloadPDF;
    @BindView(R.id.lblOrderID)
    TextView lblOrderID;
    @BindView(R.id.lblPayment)
    TextView lblPayment;
    @BindView(R.id.lbldeliveryAddressDetail)
    TextView lbldeliveryAddressDetail;
    @BindView(R.id.lblsubitemPrice)
    TextView lblsubitemPrice;
    @BindView(R.id.lblTotalSum)
    TextView lblTotalSum;
    @BindView(R.id.lblShippingPrice)
    TextView lblShippingPrice;

    @BindView(R.id.lblShippingBy)
    TextView lblShippingBy;


    @BindView(R.id.rlDiscount)
    RelativeLayout rlDiscount;

    @BindView(R.id.lblDiscount)
    TextView lblDiscount;

    @BindView(R.id.lblDiscountPrice)
    TextView lblDiscountPrice;

    @BindView(R.id.spin)
    Spinner spin;
    OrderHistoryProductAdapter orderHistoryProductAdapter;
    String[] statuses = {"Order Placed", "Accepted", "Rejected", "In Progress", "Delivered"};

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setTitle("Order Detail");
    }

    public static OrderDetailFragment newInstance() {
        Bundle args = new Bundle();
        OrderDetailFragment fragment = new OrderDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    OrdersResponse.Datum datum;
    int mLastClickTime = -1;

    public void setdata(OrdersResponse.Datum datum) {
        this.datum = datum;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        spin.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(mainActivity, android.R.layout.simple_spinner_item, statuses);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner


        spin.setAdapter(aa);
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i].equals(datum.getStatus())) {

                spin.setSelection(i);
            }
        }
        orderHistoryProductAdapter = new OrderHistoryProductAdapter(mainActivity);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.setAdapter(orderHistoryProductAdapter);
        orderHistoryProductAdapter.addAll(datum.getDetails());
        lyDownloadPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                    return;
                }

                //  Toast.makeText(mainActivity, "Will be implemented later", Toast.LENGTH_SHORT).show();
                if (preferenceHelper.getIp() != null) {
                    Log.v("Printer Printing", "Printer one");
                    for (int j = 0; j < 2; j++) {
                        printSlip(preferenceHelper.getIp(), 1);
                    }
                }
            }
        });
        try {
            setSomeData();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void setSomeData() throws JSONException {
//        Date date = null;
//        try {
//            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datum.getUser_data().getDeliveryTime());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        String newString = new SimpleDateFormat("H:mm").format(date); // 9:00
        lblShippingBy.setText(datum.getDetails().get(0).getProductName());
        lblOrderID.setText("000" + datum.getId());
        lblPayment.setText(datum.getPayment().equals("cod") ? "Cash On Delivery" : "Credit Card");
        lbldeliveryAddressDetail.setText(datum.getDeliveryAddress());
        lblShippingPrice.setText(datum.getDeliveryFees() + " £");
        lblTotalSum.setText("" + datum.getTotalAmountWithFee() + " £");
        lblsubitemPrice.setText("" + (datum.getTotalAmountWithFee() + datum.getDiscounted_amount() - datum.getDeliveryFees()) + " £");

        if (datum.getDiscounted_amount() > 0) {

            lblDiscountPrice.setText("-" + datum.getDiscounted_amount() + " £");
            rlDiscount.setVisibility(View.VISIBLE);
        }

    }

    public String getURLForResource(int resourceId) {
        //use BuildConfig.APPLICATION_ID instead of R.class.getPackage().getName() if both are not same
        return Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/" + resourceId).toString();
    }

    //    public void printSlip(String ip, int i) {
//        try {
//            Socket sock = new Socket(ip, 9100);
//            String dateTime[] = getDateTime();
//            outputStream = sock.getOutputStream();
//            printNewLine();
//            printCustom(preferenceHelper.getResturant().getName(), 3, 1);
//            printNewLine();
//            printCustom("Healthy Food Everywhere", 0, 1);
//            printCustom(preferenceHelper.getResturant().getAddress().getAddress(), 0, 1);
//            printCustom("Phone No : +" + preferenceHelper.getResturant().getPhoneNumber(), 0, 1);
//            printCustom("Post Code : " + preferenceHelper.getResturant().getAddress().getZipCode(), 0, 1);
//            printNewLine();
//            printText(leftRightAlign("Order Date : " + dateTime[0], "     Order Time : " + dateTime[1]));
//            printNewLine();
//            printUnicode();
//            printNewLine();
//            if (datum.getOrder_type().equals("Pickup")) {
//            } else {
//                printCustom("Delivery address : " + datum.getDeliveryAddress(), 1, 1);
//            }
//            printNewLine();
//            if (datum.getOrder_type() != null) {
//
//                printCustom("Order Type : " + datum.getOrder_type()/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
//            }
//            printNewLine();
//            printCustom("Order Id : 000" + datum.getId()/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
//            printNewLine();
//            printNewLine();
//
//
//            printCustom(new String(new char[50]).replace("\0", "."), 0, 1);
//            printCustom(leftRightAlign("Product Name", "Price"), 1, 1);
//
//            printCustom(new String(new char[50]).replace("\0", "."), 0, 1);
//
//            printNewLine();
//            printNewLine();
//            printNewLine();
//
//
//            for (int j = 0; j < datum.getDetails().size(); j++) {
//
//                printCustom(leftRightAlign("  Name :" + datum.getDetails().get(j).getProductName(), ""), 1, 0);
//                printNewLine();
//                printCustom(leftRightAlign("Qty : X" + datum.getDetails().get(j).getQuantity() + new String(new char[5]).replace("\0", "."), "Price : £ " + datum.getDetails().get(j).getPrice()), 0, 1);
//                printNewLine();
//
//                try {
//                    JSONArray jsonArray = new JSONArray(datum.getDetails().get(j).getExtras());
//                    String choice, group_name, price = "";
//                    Spanny spanny = new Spanny();
//                    for (int k = 0; k < jsonArray.length(); k++) {
//                        JSONObject currentObject = jsonArray.getJSONObject(k);
//
//                        if (currentObject.getString("group_name") == null) {
//
//                        } else {
//                            group_name = currentObject.getString("group_name");
//                            spanny.append(!group_name.equals("") ? group_name : "").append("\n");
//                        }
//                        if (currentObject.getString("choice") == null) {
//
//                        } else {
//                            choice = currentObject.getString("choice");
//                            spanny.append(!choice.equals("") ? choice : "").append("\n");
//                        }
//
//
//                        if (currentObject.getString("price") == null) {
//
//                        } else {
//                            price = currentObject.getString("price");
//                            spanny.append(!price.equals("") ? "Price :" + price + " £" : "").append("\n\n");
//                        }
//
//                        //  spanny.append(!group_name.equals("")?group_name:"").append("\n").append(!choice.equals("")?choice:"").append("\n").append(!price.equals("")?"Price :"+price+" £":"").append("\n\n");
//
//
//                        //  printCustom(spanny.toString(),1,0);
//                    }
//                    printCustom("" + spanny.toString(), 0, 0);
//                } catch (Exception e) {
//
//                }
//
//
//            }
//
//            printNewLine();
//            printNewLine();
//
//
//            printCustom(new String(new char[32]).replace("\0", "."), 0, 1);
//
//            printText(leftRightAlign("Delivery Fee", datum.getDeliveryFees() > 0 ? "£" + datum.getDeliveryFees() + "/=" : "Free"));
//            printNewLine();
//            printText(leftRightAlign(" Total       ", "£" + datum.getTotalAmountWithFee() + "/="));
//            printNewLine();
//            if (datum.getOrder_type() != null) {
//                printCustom(datum.getOrder_type() + ":" + "  ASAP", 0, 1);
//            }
//            printNewLine();
//            printCustom("Thank you for ordering & we look", 0, 1);
//            printCustom("forward to serve you again", 0, 1);
//            printCustom("Payment :" + (datum.getPayment().equals("cod") ? " Cash On Delivery" : " Credit Card"), 3, 1);
//
//            printNewLine();
//            printNewLine();
//            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
//            outputStream.flush();
//            sock.close();
//
//            if (i == 1) {
//                if (preferenceHelper.getIptwo() != null) {
//                    Log.v("Printer Printing", "Printer two");
//
//                    printSlip(preferenceHelper.getIptwo(), 2);
//                } else {
//                    return;
//                }
//
//            }
//
//            if (i == 2) {
//                if (preferenceHelper.getIpthree() != null) {
//                    Log.v("Printer Printing", "Printer three");
//
//                    printSlip(preferenceHelper.getIpthree(), 3);
//                } else {
//                    return;
//                }
//
//            }
//
//            if (i == 3) {
//                if (preferenceHelper.getIpfour() != null) {
//                    Log.v("Printer Printing", "Printer four");
//
//                    printSlip(preferenceHelper.getIpfour(), 4);
//                } else {
//                    return;
//                }
//
//            }
//            if (i == 4) {
//                if (preferenceHelper.getIpfive() != null) {
//                    Log.v("Printer Printing", "Printer five");
//
//                    printSlip(preferenceHelper.getIpfive(), -1);
//                } else {
//                    return;
//                }
//
//            }
//
////            if(i==-1){
////                return;
////            }
//
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//        }
//
//    }
    public void printSlip(String ip, int i) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(preferenceHelper.getResturant().getName(), 3, 1);
            printNewLine();
            //printCustom("Pizza and Fish and Chips Specialists", 0, 1);
            printCustom(preferenceHelper.getResturant().getAddress().getAddress(), 0, 1);
            printCustom("Phone No : " + preferenceHelper.getResturant().getPhoneNumber(), 0, 1);
            printCustom("Post Code : " + preferenceHelper.getResturant().getAddress().getZipCode(), 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printCustom(datum.getOrder_type(), 2, 1);
            printNewLine();
            if (datum.getOrder_type() != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " /*+ dateTime[1]*/, 3, 1);

            }
            printCustom("Order Number : 000" + datum.getId()/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            if (!datum.getOrder_type().equalsIgnoreCase("Pickup")) {
                printCustom("Restaurant Notes ", 0, 0);
                printCustom("Contact Free Delivery", 3, 0);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            }


            printNewLine();
            for (int j = 0; j < datum.getDetails().size(); j++) {
                printCustom(padLine(datum.getDetails().get(j).getQuantity() + " x " + datum.getDetails().get(j).getProductName(), " £ " + (datum.getDetails().get(j).getQuantity()*datum.getDetails().get(j).getPrice()), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(datum.getDetails().get(j).getExtras());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (datum.getDetails().get(j).getSpecialInstructions() != null && !datum.getDetails().get(j).getSpecialInstructions().equals("") && !datum.getDetails().get(j).getSpecialInstructions().equals("null")) {

                        spanny.append(" **" + datum.getDetails().get(j).getSpecialInstructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }


            }

            // printCustom(padLine("Food and drink total", "£" + Float.sum(datum.getTotalAmountWithFee(), datum.getDeliveryFees()>0?datum.getDeliveryFees():0) + "/=", 45) + "\n", 1, 0);

            printCustom(padLine("Delivery Fee", datum.getDeliveryFees() > 0 ? "£" + datum.getDeliveryFees() + "/=" : "   Free", 45) + "\n", 1, 0);
            if (datum.getDiscounted_amount() > 0) {

                printCustom(padLine("Discount", "- £" + datum.getDiscounted_amount() + "/=", 45) + "\n", 1, 0);


            }
            printCustom(padLine("Total", "£" + Float.sum(datum.getTotalAmountWithFee(), datum.getDeliveryFees() > 0 ? datum.getDeliveryFees() : 0) + "/=", 45) + "\n", 1, 0);
            printNewLine();
            printNewLine();
            if (!datum.getPayment().equalsIgnoreCase("cod")) {
                printCustom("Paid By :", 0, 0);
            }
            printCustom(padLine(datum.getPayment().equalsIgnoreCase("cod") ? (datum.getOrder_type().equalsIgnoreCase("Pickup")?"Cash on Collection":"Cash on Delivery"): "Card : xxxxxxxxxxxxxx", "£" + datum.getTotalAmountWithFee() + "/=", 45) + "\n", 1, 0);


            if (!datum.getPayment().equalsIgnoreCase("cod")) {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER HAS BEEN PAID", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                if (!datum.getPayment().equalsIgnoreCase("cod")) {
                    printCustom("PAID ONLINE ", 2, 1);

                }
            } else {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom((datum.getOrder_type().equalsIgnoreCase("Pickup")?"CASH ON COLLECTION":"CASH ON DELIVERY"), 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER NOT PAID", 2, 1);

            }
            printCustom("Please Check Notes for Instructions ", 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);

            printNewLine();
            printCustom("Customer ID: ", 0, 0);
            printCustom("" + datum.getUserId(), 2, 0);
            printNewLine();
            printCustom("Customer details: ", 0, 0);
            try {
                String[] splited = datum.getDeliveryAddress().split("\\s+");
                if (splited.length < 3) {
                    printCustom(datum.getDeliveryAddress(), 2, 0);
                    // printNewLine();
                } else if (splited.length < 4) {
                    printCustom(splited[0] + " " + splited[1] + " " + splited[2], 2, 0);
                    // printNewLine();
                } else if (splited.length < 5) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3], 2, 0);

                } else if (splited.length < 6) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    /// printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]), 2, 0);

                } else if (splited.length < 7) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();

                } else if (splited.length < 8) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6], 2, 0);

                } else if (splited.length < 9) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6].concat(" " + splited[7]), 2, 0);

                } else {

                }
            } catch (Exception exception) {
                printCustom(datum.getDeliveryAddress(), 2, 0);

            }
            printNewLine();
            printCustom("To Contact Customer Call: ", 0, 0);
            printCustom(datum.getPhone_number(), 2, 0);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();

            if (i == 1) {
                if (preferenceHelper.getIptwo() != null) {
                    Log.v("Printer Printing", "Printer two");

                    printSlip(preferenceHelper.getIptwo(), 2);
                } else {
                    return;
                }


            }
            if (i == 2) {
                if (preferenceHelper.getIpthree() != null) {
                    Log.v("Printer Printing", "Printer three");

                    printSlip(preferenceHelper.getIpthree(), 3);
                } else {
                    return;
                }


            }

            if (i == 3) {
                if (preferenceHelper.getIpfour() != null) {
                    Log.v("Printer Printing", "Printer four");

                    printSlip(preferenceHelper.getIpfour(), 4);
                } else {
                    return;
                }


            }
            if (i == 4) {
                if (preferenceHelper.getIpfive() != null) {
                    Log.v("Printer Printing", "Printer five");

                    printSlip(preferenceHelper.getIpfive(), -1);
                } else {
                    return;
                }


            }


//            if(i==-1){
//                return;
//            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }


    protected String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine) {
        if (partOne == null) {
            partOne = "";
        }
        if (partTwo == null) {
            partTwo = "";
        }
        String concat;
        if ((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + " " + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(" ", padding) + partTwo;
        }
        return concat;
    }

    /**
     * utility: string repeat
     */
    protected String repeat(String str, int i) {
        return new String(new char[i]).replace("\0", str);
    }

    private void print_image(String file) throws IOException {
        File fl = new File(file);
        if (fl.exists()) {
            Bitmap bmp = BitmapFactory.decodeFile(file);
            convertBitmap(bmp);
            outputStream.write(PrinterCommands.SET_LINE_SPACING_24);

            int offset = 0;
            while (offset < bmp.getHeight()) {
                outputStream.write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
                for (int x = 0; x < bmp.getWidth(); ++x) {

                    for (int k = 0; k < 3; ++k) {

                        byte slice = 0;
                        for (int b = 0; b < 8; ++b) {
                            int y = (((offset / 8) + k) * 8) + b;
                            int i = (y * bmp.getWidth()) + x;
                            boolean v = false;
                            if (i < dots.length()) {
                                v = dots.get(i);
                            }
                            slice |= (byte) ((v ? 1 : 0) << (7 - b));
                        }
                        outputStream.write(slice);
                    }
                }
                offset += 24;
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.write(PrinterCommands.FEED_LINE);
                outputStream.write(PrinterCommands.FEED_LINE);
            }
            outputStream.write(PrinterCommands.SET_LINE_SPACING_30);


        } else {
            Toast.makeText(mainActivity, "file doesn't exists", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private static OutputStream outputStream;
    byte FONT_TYPE;

    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes("Cp858"));
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    BitSet dots;

    private void convertArgbToGrayscale(Bitmap bmpOriginal, int width,
                                        int height) {
        int pixel;
        int k = 0;
        int B = 0, G = 0, R = 0;
        dots = new BitSet();
        try {

            for (int x = 0; x < height; x++) {
                for (int y = 0; y < width; y++) {
                    // get one pixel color
                    pixel = bmpOriginal.getPixel(y, x);

                    // retrieve color of all channels
                    R = Color.red(pixel);
                    G = Color.green(pixel);
                    B = Color.blue(pixel);
                    // take conversion up to one single value by calculating
                    // pixel intensity.
                    R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
                    // set bit into bitset, by calculating the pixel's luma
                    if (R < 55) {
                        dots.set(k);//this is the bitset that i'm printing
                    }
                    k++;

                }


            }


        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.toString());
        }
    }

    int mWidth, mHeight;
    String mStatus;

    public String convertBitmap(Bitmap inputBitmap) {

        mWidth = inputBitmap.getWidth();
        mHeight = inputBitmap.getHeight();

        convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
        mStatus = "ok";
        return mStatus;

    }

    //print photo
    public void printPhoto() {
        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ahop);
            if (bmp != null) {
                byte[] command = Utils.decodeBitmap(bmp);
                printText(command);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }


    //print unicode
    public void printUnicode() {
        try {
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void resetPrint() {
        try {
            outputStream.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            outputStream.write(PrinterCommands.FS_FONT_ALIGN);
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
            outputStream.write(PrinterCommands.ESC_CANCEL_BOLD);
            outputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print text
    private void printText(String msg) {
        try {
            // Print normal text
            outputStream.write(msg.getBytes("Cp858"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String leftRightAlign(String str1, String str2) {
        String ans = str1 + str2;
        if (ans.length() < 31) {
            int n = (31 - str1.length() + str2.length());
            ans = str1 + new String(new char[n]).replace("\0", " ") + str2;
        }
        return ans;
    }


    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime[] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        serviceHelper.enqueueCall(webService.updateOrder(datum.getId(), statuses[position]), AppConstant.UPDATE_ORDERS);


        //Toast.makeText(mainActivity,statuses[position] , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.UPDATE_ORDERS:
                //      Toast.makeText(mainActivity, "Order Updated Successfully", Toast.LENGTH_SHORT).show();
                //  serviceHelper.enqueueCall(webService.getAllOrders(), AppConstant.GET_ALL_ORDERS);
                break;


        }

    }


}
