package com.application.ordermanagement.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.DealsListingAdapter;
import com.application.ordermanagement.adapter.ProductListingAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.PrinterCommands;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.Utils;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.DealsResponse;
import com.application.ordermanagement.models.ProductDataResponse;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.BitSet;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.support.constraint.Constraints.TAG;

public  class DealsFragment extends BaseFragment implements DealsListingAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;
    DealsListingAdapter productListingAdapter;
    @BindView(R.id.rvProducts)
    RecyclerView rvProducts;

    boolean flag = false;
    private long mLastClickTime = -1;
    GridLayoutManager gridLayoutManager;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Deals");

    }

    public static DealsFragment newInstance() {
        Bundle args = new Bundle();
        DealsFragment fragment = new DealsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deals_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView

        productListingAdapter = new DealsListingAdapter(this, mainActivity, this, true);
        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (productListingAdapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return 2
                                ; //number of columns of the grid
                    default:
                        return -1;
                }
            }
        });
        rvProducts.setLayoutManager(gridLayoutManager);
        rvProducts.setAdapter(productListingAdapter);

        serviceHelper.enqueueCall(webService.getAllDeals(), AppConstant.ALL_DEALS);
        return view;
    }




    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.ALL_DEALS:
                DealsResponse productDataResponse = (DealsResponse) result;

                productListingAdapter.addAll(productDataResponse.getData());
                break;
            case AppConstant.UPDATE_DEALS:


                // serviceHelper.enqueueCall(webService.getAllproducts(),AppConstant.ALL_PRODUCTS);
                break;

        }

    }

    @Override
    public void retryPageLoad() {

    }

    @Override
    public void updatePrice(Long id, String price) {
        serviceHelper.enqueueCall(webService.updatedealproduct(id,price),AppConstant.UPDATE_DEALS);


    }

    @Override
    public void updateAvailibity(int id, String price, int status) {
       // serviceHelper.enqueueCall(webService.updateproductAvailibity(id,price,status),AppConstant.UPDATE_PRODUCTS);

    }

}