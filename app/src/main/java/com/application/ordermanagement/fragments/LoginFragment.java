package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.application.ordermanagement.R;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.BasePreferenceHelper;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.models.ProductDataResponse;
import com.application.ordermanagement.models.SignInResponse;
import com.application.ordermanagement.models.UserModel;
import com.google.firebase.analytics.FirebaseAnalytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LoginFragment extends BaseFragment {
    Unbinder unbinder;


    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    ;
    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.setVisibility(View.GONE);
    }

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signin, container, false);
        unbinder = ButterKnife.bind(this, view);

        btnSignIn.setOnClickListener(v -> {
            login();



        });
        return view;
    }
    private void login() {
       if (txtEmail.getText().toString().length() == 0) {
            UIHelper.showAlertDialog(mainActivity, getString(R.string.error), getString(R.string.enter_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches()) {
            UIHelper.showAlertDialog(mainActivity, getString(R.string.error), getString(R.string.valid_email));
        } else if (txtPassword.getText().toString().length() == 0) {
            UIHelper.showAlertDialog(mainActivity, getString(R.string.error), getString(R.string.enter_password));
        } else {

          serviceHelper.enqueueCall(webService.loginUser( txtEmail.getText().toString(), txtPassword.getText().toString(), AppConstant.DEVICE_TYPE,preferenceHelper.getDeviceToken()), AppConstant.USER_LOGIN);

        }
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.USER_LOGIN:
                SignInResponse signInResponse = (SignInResponse) result;

                if(signInResponse.getOpening_time()!=null){
                    preferenceHelper.setOpening_time(signInResponse.getOpening_time());
                }
                if(signInResponse.getClosing_time()!=null){
                    preferenceHelper.setClosing_time(signInResponse.getClosing_time());

                }
                preferenceHelper.setLoginStatus(true);
                UserModel userModel =new UserModel();
                userModel.setEmail(signInResponse.getData().getEmail());
                userModel.setFullName(signInResponse.getData().getName());
                userModel.setId(signInResponse.getData().getId());
                //userModel.setPrinter_ip("192.168.10.100");
                BasePreferenceHelper.setIPAddress(mainActivity,signInResponse.getPrinter_ip_1());
                BasePreferenceHelper.setIPAddresstwo(mainActivity,signInResponse.getPrinter_ip_2());
                BasePreferenceHelper.setIPAddressthree(mainActivity,signInResponse.getPrinter_ip_3());
                BasePreferenceHelper.setIPAddressfour(mainActivity,signInResponse.getPrinter_ip_4());
                BasePreferenceHelper.setIPAddressfive(mainActivity,signInResponse.getPrinter_ip_5());
                preferenceHelper.setIntegerPrefrence("shop_status",signInResponse.getShop_status());
                preferenceHelper.putUser(userModel);

                mainActivity.clearBackStack();
                mainActivity.addFragment(new HomeFragment(),true,false);
                UIHelper.showAlertDialog(mainActivity, getString(R.string.success), signInResponse.getMessage());
                UIHelper.hideSoftKeyboards(mainActivity);

                break;
        }

    }

}
