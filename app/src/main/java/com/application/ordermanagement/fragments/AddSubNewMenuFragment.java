package com.application.ordermanagement.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.UpdatePriceAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.helper.UIHelper;
import com.application.ordermanagement.interfaces.PaginationAdapterCallback;
import com.application.ordermanagement.model.AddProducttoCategory.AddProductToCategory;
import com.application.ordermanagement.models.MenuModule.MenuResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AddSubNewMenuFragment extends BaseFragment implements UpdatePriceAdapter.ISubCategoryProductAdapter, PaginationAdapterCallback {
    Unbinder unbinder;

    @BindView(R.id.submmit)
    Button submmit;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.addsize)
    Button addsize;
    @BindView(R.id.etname)
    EditText etname;
    @BindView(R.id.etprice)
    EditText etprice;
    @BindView(R.id.etfoodallergy)
    EditText etfoodallergy;
    @BindView(R.id.etdescription)
    EditText etdescription;
    @BindView(R.id.rvSizes)
    ListView rvSizes;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    ArrayList<String> items = new ArrayList<>();
    ArrayAdapter<String> itemsAdapter;
    AddProductToCategory addProductToCategory;
    String categoryId = "";
    MenuResponse.Product data;
    UpdatePriceAdapter updatePriceAdapter;
    @BindView(R.id.rvPrices)
    RecyclerView rvPrices;

    @Override
    protected void setTitle(Titlebar titlebar) {
        titlebar.showBackButton(mainActivity);
        titlebar.setTitle("Menu");

    }

    public void setdata(MenuResponse.Product data) {
        this.data = data;
    }

    public static AddSubNewMenuFragment newInstance() {
        Bundle args = new Bundle();
        AddSubNewMenuFragment fragment = new AddSubNewMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_sub_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        // Create an object of CustomAdapter and set Adapter to GirdView
        addProductToCategory = new AddProductToCategory();

        updatePriceAdapter = new UpdatePriceAdapter(this, mainActivity, this, true);
        rvPrices.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvPrices.setAdapter(updatePriceAdapter);

//        itemsAdapter =
//                new ArrayAdapter<String>(mainActivity, android.R.layout.simple_list_item_1, items);
//        rvSizes.setAdapter(itemsAdapter);

        submmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etname.getText().toString().length() > 0 && etprice.getText().toString().length() > 0
                        && etdescription.getText().toString().length() > 0) {

                    if (data != null) {
                        addProductToCategory.setIdCategory(Long.parseLong(categoryId));
                        addProductToCategory.setStatus(checkbox.isChecked() ? "1" : "0");
                        addProductToCategory.setPrice(etprice.getText().toString());
                        addProductToCategory.setName(etname.getText().toString());
                        addProductToCategory.setDescription(etdescription.getText().toString());
                        addProductToCategory.setFoodAllergy("" + etfoodallergy.getText().toString());
                        //if(inputs.size()>0){

                        addProductToCategory.setInputs(inputs);
                        serviceHelper.enqueueCall(webService.UpdateProduct(data.getId(), addProductToCategory), AppConstant.UPDATE_PRODUCTS);


                    } else {
                        addProductToCategory.setIdCategory(Long.parseLong(categoryId));
                        addProductToCategory.setStatus(checkbox.isChecked() ? "1" : "0");
                        addProductToCategory.setPrice(etprice.getText().toString());
                        addProductToCategory.setName(etname.getText().toString());
                        addProductToCategory.setDescription(etdescription.getText().toString());
                        addProductToCategory.setFoodAllergy("" + etfoodallergy.getText().toString());
                        //if(inputs.size()>0){

                        addProductToCategory.setInputs(inputs);
                        //}
                        serviceHelper.enqueueCall(webService.AddProduct(addProductToCategory), AppConstant.ADD_PRODUCTS);

                    }


                    //EventBus.getDefault().post(etname.getText().toString());

                } else {
                    Toast.makeText(mainActivity, "Fields Required", Toast.LENGTH_SHORT).show();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                mainActivity.popFragment();
            }
        });
        addsize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.hideSoftKeyboards(mainActivity);
                addSizePopup();
            }
        });
        if (data != null) {
            addProductToCategory.setIdCategory(data.getIdCategory());
            addProductToCategory.setStatus(String.valueOf(data.getStatus()));
            addProductToCategory.setName(data.getName());
            addProductToCategory.setDescription(data.getDescription());
            addProductToCategory.setFoodAllergy(data.getFoodAllergy() != null ? data.getFoodAllergy() : "");
            checkbox.setChecked(data.getStatus().equals("1") ? true : false);
            if (data.getSizes().size() > 0) {
                updatePriceAdapter.addAll(data.getSizes());
                inputs.addAll(data.getSizes());
                etprice.setText("0");

                /// itemsAdapter.add("Size : " + data.getSizes().get(i).getSize().toString() + " " + "Price : " + data.getSizes().get(i).getPrice());
                // AddProductToCategory.Input input = new AddProductToCategory.Input(data.getSizes().get(i).getPrice(),data.getSizes().get(i).getSize());
                //  inputs.add(input);

            } else {
                etprice.setText(data.getPrice());
                addProductToCategory.setPrice(data.getPrice());
            }
            etname.setText(data.getName());
            etdescription.setText(data.getDescription());
            etfoodallergy.setText(data.getFoodAllergy() != null ? data.getFoodAllergy() : "");
        }
        return view;
    }


    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case AppConstant.ADD_PRODUCTS:
                popFragment();

                break;
            case AppConstant.UPDATE_PRODUCTS:
                popFragment();

                break;

        }

    }

    ArrayList<MenuResponse.Size> inputs = new ArrayList<>();

    public void addSizePopup() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle("Sizes");
        //alertDialog.setMessage("Enter Size");
        alertDialog.setCancelable(false);
        LinearLayout layout = new LinearLayout(mainActivity);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText size = new EditText(mainActivity);
        size.setSingleLine(true);
        size.setHint("Enter Size");


        final EditText price = new EditText(mainActivity);
        price.setSingleLine(true);
        price.setHint("Enter Price");
        price.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);


        layout.addView(size);
        layout.addView(price);

        alertDialog.setPositiveButton("Add",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (size.getText().toString().trim().equals("") || price.getText().toString().trim().equals("")) {
                            UIHelper.showAlertDialog(mainActivity, getString(R.string.error), "Fields Required");

                        } else {
                            dialog.dismiss();


                            MenuResponse.Size size1 = new MenuResponse.Size();
                            size1.setPrice(price.getText().toString());
                            size1.setSize(size.getText().toString());
                            updatePriceAdapter.add(size1);
                            updatePriceAdapter.notifyDataSetChanged();
                            inputs.add(size1);
                            //itemsAdapter.add("Size : " + size.getText().toString() + " " + "Price : " + price.getText().toString());

//                            AddProductToCategory.Input input = new AddProductToCategory.Input(price.getText().toString(), size.getText().toString());
//                            inputs.add(input);
                        }

                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });
        alertDialog.setView(layout);
        alertDialog.show();


    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public void size(ArrayList<MenuResponse.Size> datum) {


//        AddProductToCategory.Input input = new AddProductToCategory.Input(datum.getText().toString(), size.getText().toString());
//        inputs.add(input);
    }

    @Override
    public void delete(MenuResponse.Size datum, int position) {
        inputs.remove(position);
        updatePriceAdapter.removeitem(position);
    }

    @Override
    public void onPriceUpdate(String price, int position) {
        MenuResponse.Size size = new MenuResponse.Size();
        size.setSize(inputs.get(position).getSize());
        size.setPrice(price);
        inputs.set(position,size);
    }

    @Override
    public void retryPageLoad() {

    }
}