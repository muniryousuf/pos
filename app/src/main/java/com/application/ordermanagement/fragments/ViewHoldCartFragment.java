package com.application.ordermanagement.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.ordermanagement.R;
import com.application.ordermanagement.adapter.POS.POSCartAdapter;
import com.application.ordermanagement.fragments.baseFragment.BaseFragment;
import com.application.ordermanagement.helper.Titlebar;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ViewHoldCartFragment extends BaseFragment implements  POSCartAdapter.onClickOptions {
    Unbinder unbinder;
    @BindView(R.id.rvCart)
    RecyclerView rvCart;
    @BindView(R.id.tvTaketoCrt)
    TextView tvTaketoCrt;
    POSCartAdapter posCartAdapter;
    OrderPlaceModel detailArrayList;

    public void setdata(OrderPlaceModel detailArrayList) {
        this.detailArrayList = detailArrayList;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_items, container, false);
        unbinder = ButterKnife.bind(this, view);

        posCartAdapter = new POSCartAdapter(mainActivity,this,false);
        rvCart.setLayoutManager(new LinearLayoutManager(mainActivity));
        rvCart.setAdapter(posCartAdapter);
        posCartAdapter.addAll(detailArrayList.getOrderDetails());
        tvTaketoCrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                POSFragment posFragment =new POSFragment();
                posFragment.setdata(detailArrayList);
                mainActivity.addFragment(posFragment,true,true);
            }
        });
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
    }

    @Override
    protected void setTitle(Titlebar titlebar) {

    }


    @Override
    public void onDeleteOption(int pos) {

    }

    @Override
    public void onIncrese() {

    }

    @Override
    public void onDecrese() {

    }

    @Override
    public void onClickOption(OrderPlaceModel.OrderDetail orderDetail,int  b) {

    }
}
