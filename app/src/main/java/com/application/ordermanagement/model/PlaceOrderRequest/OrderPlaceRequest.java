
package com.application.ordermanagement.model.PlaceOrderRequest;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderPlaceRequest {

    public OrderPlaceRequest( Long mUserId, UserData mUserData, Double mTotalAmountWithFee, String mPayment, String mOrderType, List<OrderDetail> mOrderDetails, Long mDiscountedAmount, String mDeliveryFees, String mDeliveryAddress, String mCvvNumber, String mCcExpiryYear, String mCcExpiryMonth,String mCardNo) {
        this.mCardNo = mCardNo;
        this.mCcExpiryMonth = mCcExpiryMonth;
        this.mCcExpiryYear = mCcExpiryYear;
        this.mCvvNumber = mCvvNumber;
        this.mDeliveryAddress = mDeliveryAddress;
        this.mDeliveryFees = mDeliveryFees;
        this.mDiscountedAmount = mDiscountedAmount;
        this.mOrderDetails = mOrderDetails;
        this.mOrderType = mOrderType;
        this.mPayment = mPayment;
        this.mTotalAmountWithFee = mTotalAmountWithFee;
        this.mUserData = mUserData;
        this.mUserId = mUserId;
    }

    @SerializedName("card_no")
    private String mCardNo;
    @SerializedName("ccExpiryMonth")
    private String mCcExpiryMonth;
    @SerializedName("ccExpiryYear")
    private String mCcExpiryYear;
    @SerializedName("cvvNumber")
    private String mCvvNumber;
    @SerializedName("delivery_address")
    private String mDeliveryAddress;
    @SerializedName("delivery_fees")
    private String mDeliveryFees;
    @SerializedName("discounted_amount")
    private Long mDiscountedAmount;
    @SerializedName("order_details")
    private List<OrderDetail> mOrderDetails;
    @SerializedName("order_type")
    private String mOrderType;
    @SerializedName("payment")
    private String mPayment;
    @SerializedName("total_amount_with_fee")
    private Double mTotalAmountWithFee;
    @SerializedName("user_data")
    private UserData mUserData;
    @SerializedName("user_id")
    private Long mUserId;

    public String getCardNo() {
        return mCardNo;
    }

    public void setCardNo(String cardNo) {
        mCardNo = cardNo;
    }

    public String getCcExpiryMonth() {
        return mCcExpiryMonth;
    }

    public void setCcExpiryMonth(String ccExpiryMonth) {
        mCcExpiryMonth = ccExpiryMonth;
    }

    public String getCcExpiryYear() {
        return mCcExpiryYear;
    }

    public void setCcExpiryYear(String ccExpiryYear) {
        mCcExpiryYear = ccExpiryYear;
    }

    public String getCvvNumber() {
        return mCvvNumber;
    }

    public void setCvvNumber(String cvvNumber) {
        mCvvNumber = cvvNumber;
    }

    public String getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public String getDeliveryFees() {
        return mDeliveryFees;
    }

    public void setDeliveryFees(String deliveryFees) {
        mDeliveryFees = deliveryFees;
    }

    public Long getDiscountedAmount() {
        return mDiscountedAmount;
    }

    public void setDiscountedAmount(Long discountedAmount) {
        mDiscountedAmount = discountedAmount;
    }

    public List<OrderDetail> getOrderDetails() {
        return mOrderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        mOrderDetails = orderDetails;
    }

    public String getOrderType() {
        return mOrderType;
    }

    public void setOrderType(String orderType) {
        mOrderType = orderType;
    }

    public String getPayment() {
        return mPayment;
    }

    public void setPayment(String payment) {
        mPayment = payment;
    }

    public Double getTotalAmountWithFee() {
        return mTotalAmountWithFee;
    }

    public void setTotalAmountWithFee(Double totalAmountWithFee) {
        mTotalAmountWithFee = totalAmountWithFee;
    }

    public UserData getUserData() {
        return mUserData;
    }

    public void setUserData(UserData userData) {
        mUserData = userData;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }
    public static class OrderDetail {

        @SerializedName("extras")
        private List<Extra> mExtras;
        @SerializedName("price")
        private Double mPrice;

        public OrderDetail( Double mSingleProductTotalAmount, Long mQuantity, String mProductType, String mProductName, Long mProductId, Double mPrice,List<Extra> mExtras) {
            this.mExtras = mExtras;
            this.mPrice = mPrice;
            this.mProductId = mProductId;
            this.mProductName = mProductName;
            this.mProductType = mProductType;
            this.mQuantity = mQuantity;
            this.mSingleProductTotalAmount = mSingleProductTotalAmount;
        }

        @SerializedName("product_id")
        private Long mProductId;
        @SerializedName("product_name")
        private String mProductName;
        @SerializedName("product_type")
        private String mProductType;
        @SerializedName("quantity")
        private Long mQuantity;
        @SerializedName("single_product_total_amount")
        private Double mSingleProductTotalAmount;

        public List<Extra> getExtras() {
            return mExtras;
        }

        public void setExtras(List<Extra> extras) {
            mExtras = extras;
        }

        public Double getPrice() {
            return mPrice;
        }

        public void setPrice(Double price) {
            mPrice = price;
        }

        public Long getProductId() {
            return mProductId;
        }

        public void setProductId(Long productId) {
            mProductId = productId;
        }

        public String getProductName() {
            return mProductName;
        }

        public void setProductName(String productName) {
            mProductName = productName;
        }

        public String getProductType() {
            return mProductType;
        }

        public void setProductType(String productType) {
            mProductType = productType;
        }

        public Long getQuantity() {
            return mQuantity;
        }

        public void setQuantity(Long quantity) {
            mQuantity = quantity;
        }

        public Double getSingleProductTotalAmount() {
            return mSingleProductTotalAmount;
        }

        public void setSingleProductTotalAmount(Double singleProductTotalAmount) {
            mSingleProductTotalAmount = singleProductTotalAmount;
        }

    }
    public static class Extra {

        @SerializedName("choice")
        private String mChoice;
        @SerializedName("group_name")
        private String mGroupName;

        public Extra(String mChoice, String mGroupName, String mPrice) {
            this.mChoice = mChoice;
            this.mGroupName = mGroupName;
            this.mPrice = mPrice;
        }

        @SerializedName("price")
        private String mPrice;

        public String getChoice() {
            return mChoice;
        }

        public void setChoice(String choice) {
            mChoice = choice;
        }

        public String getGroupName() {
            return mGroupName;
        }

        public void setGroupName(String groupName) {
            mGroupName = groupName;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

    }
    public class UserData {

        @SerializedName("address")
        private String mAddress;
        @SerializedName("asap")
        private String mAsap;
        @SerializedName("card_holder_name")
        private String mCardHolderName;
        @SerializedName("card_number")
        private String mCardNumber;
        @SerializedName("cvc")
        private String mCvc;
        @SerializedName("deliveryTime")
        private String mDeliveryTime;
        @SerializedName("email")
        private String mEmail;
        @SerializedName("expiration_month")
        private String mExpirationMonth;
        @SerializedName("expiration_year")
        private String mExpirationYear;
        @SerializedName("land_mark")
        private String mLandMark;
        @SerializedName("name")
        private String mName;
        @SerializedName("number")
        private String mNumber;
        @SerializedName("order_type")
        private String mOrderType;
        @SerializedName("payment_type")
        private String mPaymentType;
        @SerializedName("postal_code")
        private String mPostalCode;
        @SerializedName("street")
        private String mStreet;
        @SerializedName("town")
        private Object mTown;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public String getAsap() {
            return mAsap;
        }

        public void setAsap(String asap) {
            mAsap = asap;
        }

        public String getCardHolderName() {
            return mCardHolderName;
        }

        public void setCardHolderName(String cardHolderName) {
            mCardHolderName = cardHolderName;
        }

        public String getCardNumber() {
            return mCardNumber;
        }

        public void setCardNumber(String cardNumber) {
            mCardNumber = cardNumber;
        }

        public String getCvc() {
            return mCvc;
        }

        public void setCvc(String cvc) {
            mCvc = cvc;
        }

        public String getDeliveryTime() {
            return mDeliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            mDeliveryTime = deliveryTime;
        }

        public String getEmail() {
            return mEmail;
        }

        public void setEmail(String email) {
            mEmail = email;
        }

        public String getExpirationMonth() {
            return mExpirationMonth;
        }

        public void setExpirationMonth(String expirationMonth) {
            mExpirationMonth = expirationMonth;
        }

        public String getExpirationYear() {
            return mExpirationYear;
        }

        public void setExpirationYear(String expirationYear) {
            mExpirationYear = expirationYear;
        }

        public String getLandMark() {
            return mLandMark;
        }

        public void setLandMark(String landMark) {
            mLandMark = landMark;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String number) {
            mNumber = number;
        }

        public String getOrderType() {
            return mOrderType;
        }

        public void setOrderType(String orderType) {
            mOrderType = orderType;
        }

        public String getPaymentType() {
            return mPaymentType;
        }

        public void setPaymentType(String paymentType) {
            mPaymentType = paymentType;
        }

        public String getPostalCode() {
            return mPostalCode;
        }

        public void setPostalCode(String postalCode) {
            mPostalCode = postalCode;
        }

        public String getStreet() {
            return mStreet;
        }

        public void setStreet(String street) {
            mStreet = street;
        }

        public Object getTown() {
            return mTown;
        }

        public void setTown(Object town) {
            mTown = town;
        }

    }

}
