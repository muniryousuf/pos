
package com.application.ordermanagement.model.GetChoice;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetChoiceResponse {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("pagination")
    private Boolean mPagination;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getPagination() {
        return mPagination;
    }

    public void setPagination(Boolean pagination) {
        mPagination = pagination;
    }
    public class Datum {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_group")
        private Long mIdGroup;
        @SerializedName("name")
        private String mName;
        @SerializedName("preselect")
        private Long mPreselect;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdGroup() {
            return mIdGroup;
        }

        public void setIdGroup(Long idGroup) {
            mIdGroup = idGroup;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public Long getPreselect() {
            return mPreselect;
        }

        public void setPreselect(Long preselect) {
            mPreselect = preselect;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
