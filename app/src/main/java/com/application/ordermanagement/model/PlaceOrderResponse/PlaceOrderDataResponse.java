
package com.application.ordermanagement.model.PlaceOrderResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class PlaceOrderDataResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
    public class OrderDetail {

        @SerializedName("extras")
        private List<Extra> mExtras;
        @SerializedName("price")
        private Double mPrice;
        @SerializedName("product_id")
        private Long mProductId;
        @SerializedName("product_name")
        private String mProductName;
        @SerializedName("product_type")
        private String mProductType;
        @SerializedName("quantity")
        private Long mQuantity;
        @SerializedName("single_product_total_amount")
        private Double mSingleProductTotalAmount;

        public List<Extra> getExtras() {
            return mExtras;
        }

        public void setExtras(List<Extra> extras) {
            mExtras = extras;
        }

        public Double getPrice() {
            return mPrice;
        }

        public void setPrice(Double price) {
            mPrice = price;
        }

        public Long getProductId() {
            return mProductId;
        }

        public void setProductId(Long productId) {
            mProductId = productId;
        }

        public String getProductName() {
            return mProductName;
        }

        public void setProductName(String productName) {
            mProductName = productName;
        }

        public String getProductType() {
            return mProductType;
        }

        public void setProductType(String productType) {
            mProductType = productType;
        }

        public Long getQuantity() {
            return mQuantity;
        }

        public void setQuantity(Long quantity) {
            mQuantity = quantity;
        }

        public Double getSingleProductTotalAmount() {
            return mSingleProductTotalAmount;
        }

        public void setSingleProductTotalAmount(Double singleProductTotalAmount) {
            mSingleProductTotalAmount = singleProductTotalAmount;
        }

    }
    public class Extra {

        @SerializedName("choice")
        private String mChoice;
        @SerializedName("group_name")
        private String mGroupName;
        @SerializedName("price")
        private String mPrice;

        public String getChoice() {
            return mChoice;
        }

        public void setChoice(String choice) {
            mChoice = choice;
        }

        public String getGroupName() {
            return mGroupName;
        }

        public void setGroupName(String groupName) {
            mGroupName = groupName;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

    }
    public class Data {

        @SerializedName("card_no")
        private Object mCardNo;
        @SerializedName("ccExpiryMonth")
        private Object mCcExpiryMonth;
        @SerializedName("ccExpiryYear")
        private Object mCcExpiryYear;
        @SerializedName("cvvNumber")
        private Object mCvvNumber;
        @SerializedName("delivery_address")
        private String mDeliveryAddress;
        @SerializedName("delivery_fees")
        private String mDeliveryFees;
        @SerializedName("discounted_amount")
        private String mDiscountedAmount;
        @SerializedName("order_details")
        private List<OrderDetail> mOrderDetails;
        @SerializedName("order_type")
        private String mOrderType;
        @SerializedName("payment")
        private String mPayment;
        @SerializedName("phone_number")
        private String mPhoneNumber;
        @SerializedName("total_amount_with_fee")
        private Double mTotalAmountWithFee;
        @SerializedName("user_data")
        private UserData mUserData;
        @SerializedName("user_id")
        private Long mUserId;

        public Object getCardNo() {
            return mCardNo;
        }

        public void setCardNo(Object cardNo) {
            mCardNo = cardNo;
        }

        public Object getCcExpiryMonth() {
            return mCcExpiryMonth;
        }

        public void setCcExpiryMonth(Object ccExpiryMonth) {
            mCcExpiryMonth = ccExpiryMonth;
        }

        public Object getCcExpiryYear() {
            return mCcExpiryYear;
        }

        public void setCcExpiryYear(Object ccExpiryYear) {
            mCcExpiryYear = ccExpiryYear;
        }

        public Object getCvvNumber() {
            return mCvvNumber;
        }

        public void setCvvNumber(Object cvvNumber) {
            mCvvNumber = cvvNumber;
        }

        public String getDeliveryAddress() {
            return mDeliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            mDeliveryAddress = deliveryAddress;
        }

        public String getDeliveryFees() {
            return mDeliveryFees;
        }

        public void setDeliveryFees(String deliveryFees) {
            mDeliveryFees = deliveryFees;
        }

        public String getDiscountedAmount() {
            return mDiscountedAmount;
        }

        public void setDiscountedAmount(String discountedAmount) {
            mDiscountedAmount = discountedAmount;
        }

        public List<OrderDetail> getOrderDetails() {
            return mOrderDetails;
        }

        public void setOrderDetails(List<OrderDetail> orderDetails) {
            mOrderDetails = orderDetails;
        }

        public String getOrderType() {
            return mOrderType;
        }

        public void setOrderType(String orderType) {
            mOrderType = orderType;
        }

        public String getPayment() {
            return mPayment;
        }

        public void setPayment(String payment) {
            mPayment = payment;
        }

        public String getPhoneNumber() {
            return mPhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            mPhoneNumber = phoneNumber;
        }

        public Double getTotalAmountWithFee() {
            return mTotalAmountWithFee;
        }

        public void setTotalAmountWithFee(Double totalAmountWithFee) {
            mTotalAmountWithFee = totalAmountWithFee;
        }

        public UserData getUserData() {
            return mUserData;
        }

        public void setUserData(UserData userData) {
            mUserData = userData;
        }

        public Long getUserId() {
            return mUserId;
        }

        public void setUserId(Long userId) {
            mUserId = userId;
        }

    }
    public class UserData {

        @SerializedName("address")
        private String mAddress;
        @SerializedName("asap")
        private Object mAsap;
        @SerializedName("card_holder_name")
        private Object mCardHolderName;
        @SerializedName("card_number")
        private Object mCardNumber;
        @SerializedName("cvc")
        private Object mCvc;
        @SerializedName("deliveryTime")
        private String mDeliveryTime;
        @SerializedName("email")
        private String mEmail;
        @SerializedName("expiration_month")
        private Object mExpirationMonth;
        @SerializedName("expiration_year")
        private Object mExpirationYear;
        @SerializedName("land_mark")
        private Object mLandMark;
        @SerializedName("name")
        private String mName;
        @SerializedName("number")
        private String mNumber;
        @SerializedName("order_type")
        private String mOrderType;
        @SerializedName("payment_type")
        private String mPaymentType;
        @SerializedName("postal_code")
        private String mPostalCode;
        @SerializedName("street")
        private String mStreet;
        @SerializedName("town")
        private Object mTown;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public Object getAsap() {
            return mAsap;
        }

        public void setAsap(Object asap) {
            mAsap = asap;
        }

        public Object getCardHolderName() {
            return mCardHolderName;
        }

        public void setCardHolderName(Object cardHolderName) {
            mCardHolderName = cardHolderName;
        }

        public Object getCardNumber() {
            return mCardNumber;
        }

        public void setCardNumber(Object cardNumber) {
            mCardNumber = cardNumber;
        }

        public Object getCvc() {
            return mCvc;
        }

        public void setCvc(Object cvc) {
            mCvc = cvc;
        }

        public String getDeliveryTime() {
            return mDeliveryTime;
        }

        public void setDeliveryTime(String deliveryTime) {
            mDeliveryTime = deliveryTime;
        }

        public String getEmail() {
            return mEmail;
        }

        public void setEmail(String email) {
            mEmail = email;
        }

        public Object getExpirationMonth() {
            return mExpirationMonth;
        }

        public void setExpirationMonth(Object expirationMonth) {
            mExpirationMonth = expirationMonth;
        }

        public Object getExpirationYear() {
            return mExpirationYear;
        }

        public void setExpirationYear(Object expirationYear) {
            mExpirationYear = expirationYear;
        }

        public Object getLandMark() {
            return mLandMark;
        }

        public void setLandMark(Object landMark) {
            mLandMark = landMark;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String number) {
            mNumber = number;
        }

        public String getOrderType() {
            return mOrderType;
        }

        public void setOrderType(String orderType) {
            mOrderType = orderType;
        }

        public String getPaymentType() {
            return mPaymentType;
        }

        public void setPaymentType(String paymentType) {
            mPaymentType = paymentType;
        }

        public String getPostalCode() {
            return mPostalCode;
        }

        public void setPostalCode(String postalCode) {
            mPostalCode = postalCode;
        }

        public String getStreet() {
            return mStreet;
        }

        public void setStreet(String street) {
            mStreet = street;
        }

        public Object getTown() {
            return mTown;
        }

        public void setTown(Object town) {
            mTown = town;
        }

    }

}
