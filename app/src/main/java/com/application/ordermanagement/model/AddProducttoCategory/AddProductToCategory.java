
package com.application.ordermanagement.model.AddProducttoCategory;

import android.support.annotation.MenuRes;

import java.util.ArrayList;
import java.util.List;

import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddProductToCategory {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("food_allergy")
    private String mFoodAllergy;
    @SerializedName("id_category")
    private Long mIdCategory;
    @SerializedName("inputs")
    private ArrayList<MenuResponse.Size> mInputs;
    @SerializedName("name")
    private String mName;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("status")
    private String mStatus;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getFoodAllergy() {
        return mFoodAllergy;
    }

    public void setFoodAllergy(String foodAllergy) {
        mFoodAllergy = foodAllergy;
    }

    public Long getIdCategory() {
        return mIdCategory;
    }

    public void setIdCategory(Long idCategory) {
        mIdCategory = idCategory;
    }

    public ArrayList<MenuResponse.Size> getInputs() {
        return mInputs;
    }

    public void setInputs(ArrayList<MenuResponse.Size> inputs) {
        mInputs = inputs;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
    public static class Input {

        @SerializedName("price")
        private String mPrice;

        public Input(String mPrice, String mSize) {
            this.mPrice = mPrice;
            this.mSize = mSize;
        }

        @SerializedName("size")
        private String mSize;

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getSize() {
            return mSize;
        }

        public void setSize(String size) {
            mSize = size;
        }

    }
}
