
package com.application.ordermanagement.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResturantInfoBodyRequest {

    @SerializedName("address")
    private Address mAddress;
    @SerializedName("email")
    private Object mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("id_user")
    private Long mIdUser;
    @SerializedName("is_delivery")
    private Long mIsDelivery;
    @SerializedName("is_dine")
    private Long mIsDine;
    @SerializedName("is_pickup")
    private Long mIsPickup;
    @SerializedName("logo")
    private Object mLogo;
    @SerializedName("name")
    private String mName;
    @SerializedName("phone_number")
    private String mPhoneNumber;
    @SerializedName("website_url")
    private String mWebsiteUrl;

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address address) {
        mAddress = address;
    }

    public Object getEmail() {
        return mEmail;
    }

    public void setEmail(Object email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getIdUser() {
        return mIdUser;
    }

    public void setIdUser(Long idUser) {
        mIdUser = idUser;
    }

    public Long getIsDelivery() {
        return mIsDelivery;
    }

    public void setIsDelivery(Long isDelivery) {
        mIsDelivery = isDelivery;
    }

    public Long getIsDine() {
        return mIsDine;
    }

    public void setIsDine(Long isDine) {
        mIsDine = isDine;
    }

    public Long getIsPickup() {
        return mIsPickup;
    }

    public void setIsPickup(Long isPickup) {
        mIsPickup = isPickup;
    }

    public Object getLogo() {
        return mLogo;
    }

    public void setLogo(Object logo) {
        mLogo = logo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getWebsiteUrl() {
        return mWebsiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        mWebsiteUrl = websiteUrl;
    }
    public static class Address {

        @SerializedName("address")
        private String mAddress;
        @SerializedName("city")
        private String mCity;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_country")
        private Long mIdCountry;
        @SerializedName("id_restaurant")
        private Long mIdRestaurant;
        @SerializedName("zip_code")
        private String mZipCode;

        public String getAddress() {
            return mAddress;
        }

        public void setAddress(String address) {
            mAddress = address;
        }

        public String getCity() {
            return mCity;
        }

        public void setCity(String city) {
            mCity = city;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCountry() {
            return mIdCountry;
        }

        public void setIdCountry(Long idCountry) {
            mIdCountry = idCountry;
        }

        public Long getIdRestaurant() {
            return mIdRestaurant;
        }

        public void setIdRestaurant(Long idRestaurant) {
            mIdRestaurant = idRestaurant;
        }

        public String getZipCode() {
            return mZipCode;
        }

        public void setZipCode(String zipCode) {
            mZipCode = zipCode;
        }

    }

}
