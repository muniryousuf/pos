
package com.application.ordermanagement.model.deletecategoryresponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DeleteCategoryResponse {

    @SerializedName("data")
    private List<Object> mData;
    @SerializedName("deleted")
    private Boolean mDeleted;
    @SerializedName("status")
    private Boolean mStatus;

    public List<Object> getData() {
        return mData;
    }

    public void setData(List<Object> data) {
        mData = data;
    }

    public Boolean getDeleted() {
        return mDeleted;
    }

    public void setDeleted(Boolean deleted) {
        mDeleted = deleted;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
