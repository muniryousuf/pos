package com.application.ordermanagement.model.Printers;

import com.application.ordermanagement.models.CategoryResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrinterResponseList{
    public ArrayList<Datum> data;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public boolean pagination;
    public String message;
    public static class Datum{
        public int id;

        public int getIs_default() {
            return is_default;
        }

        public void setIs_default(int is_default) {
            this.is_default = is_default;
        }

        public int is_default;
        public String name;
        public String ip;
        public int status;
        public int print_count;
        public String created_at;
        public String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getPrint_count() {
            return print_count;
        }

        public void setPrint_count(int print_count) {
            this.print_count = print_count;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public List<CategoryResponse.Datum> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoryResponse.Datum> categories) {
            this.categories = categories;
        }

        public List<CategoryResponse.Datum> categories;
    }
}