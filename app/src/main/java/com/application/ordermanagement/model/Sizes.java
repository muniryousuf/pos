package com.application.ordermanagement.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class Sizes {


    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }
    @SerializedName("size")
    HashMap<String,String> map = new HashMap<>();
}
