package com.application.ordermanagement.model.TableResponseModel;

import java.util.ArrayList;

public class TablesResponse{
    public ArrayList<Datum> data;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public boolean isPagination() {
        return pagination;
    }

    public void setPagination(boolean pagination) {
        this.pagination = pagination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean pagination;
    public String message;
    public class Datum{
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getIs_reserve() {
            return is_reserve;
        }

        public void setIs_reserve(int is_reserve) {
            this.is_reserve = is_reserve;
        }

        public String getReservation_start_time() {
            return reservation_start_time;
        }

        public void setReservation_start_time(String reservation_start_time) {
            this.reservation_start_time = reservation_start_time;
        }

        public String getReservation_end_time() {
            return reservation_end_time;
        }

        public void setReservation_end_time(String reservation_end_time) {
            this.reservation_end_time = reservation_end_time;
        }

        public int id;
        public String name;
        public int is_reserve;
        public String reservation_start_time;
        public String reservation_end_time;

    }
}