
package com.application.ordermanagement.model.AdAdonProduct;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AdOnAddProductResponse {

    @SerializedName("created")
    private Boolean mCreated;
    @SerializedName("data")
    private Data mData;
    @SerializedName("status")
    private Boolean mStatus;

    public Boolean getCreated() {
        return mCreated;
    }

    public void setCreated(Boolean created) {
        mCreated = created;
    }

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }
    public class Data {

        @SerializedName("id")
        private Long mId;

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

    }
}
