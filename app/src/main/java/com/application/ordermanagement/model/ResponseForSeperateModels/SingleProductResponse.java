
package com.application.ordermanagement.model.ResponseForSeperateModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class SingleProductResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
    public class Size {
        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        boolean is_checked= false;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_product")
        private Long mIdProduct;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("size")
        private String mSize;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdProduct() {
            return mIdProduct;
        }

        public void setIdProduct(Long idProduct) {
            mIdProduct = idProduct;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getSize() {
            return mSize;
        }

        public void setSize(String size) {
            mSize = size;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class Group {

        @SerializedName("choices")
        private List<Choice> mChoices;
        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("display_type")
        private String mDisplayType;
        @SerializedName("id")
        private Long mId;
        @SerializedName("name")
        private String mName;
        @SerializedName("type")
        private String mType;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public List<Choice> getChoices() {
            return mChoices;
        }

        public void setChoices(List<Choice> choices) {
            mChoices = choices;
        }

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDisplayType() {
            return mDisplayType;
        }

        public void setDisplayType(String displayType) {
            mDisplayType = displayType;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getType() {
            return mType;
        }

        public void setType(String type) {
            mType = type;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public static class Data {

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("food_allergy")
        private Object mFoodAllergy;
        @SerializedName("groups")
        private List<Group> mGroups;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_category")
        private Long mIdCategory;
        @SerializedName("image")
        private Object mImage;
        @SerializedName("image_url")
        private String mImageUrl;
        @SerializedName("name")
        private String mName;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("sizes")
        private List<Size> mSizes;
        @SerializedName("status")
        private Long mStatus;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            mDescription = description;
        }

        public Object getFoodAllergy() {
            return mFoodAllergy;
        }

        public void setFoodAllergy(Object foodAllergy) {
            mFoodAllergy = foodAllergy;
        }

        public List<Group> getGroups() {
            return mGroups;
        }

        public void setGroups(List<Group> groups) {
            mGroups = groups;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdCategory() {
            return mIdCategory;
        }

        public void setIdCategory(Long idCategory) {
            mIdCategory = idCategory;
        }

        public Object getImage() {
            return mImage;
        }

        public void setImage(Object image) {
            mImage = image;
        }

        public String getImageUrl() {
            return mImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            mImageUrl = imageUrl;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public List<Size> getSizes() {
            return mSizes;
        }

        public void setSizes(List<Size> sizes) {
            mSizes = sizes;
        }

        public Long getStatus() {
            return mStatus;
        }

        public void setStatus(Long status) {
            mStatus = status;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }
    public class Choice {
        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        boolean is_checked= false;

        @SerializedName("created_at")
        private String mCreatedAt;
        @SerializedName("id")
        private Long mId;
        @SerializedName("id_group")
        private Long mIdGroup;
        @SerializedName("name")
        private String mName;
        @SerializedName("preselect")
        private Long mPreselect;
        @SerializedName("price")
        private String mPrice;
        @SerializedName("updated_at")
        private String mUpdatedAt;

        public String getCreatedAt() {
            return mCreatedAt;
        }

        public void setCreatedAt(String createdAt) {
            mCreatedAt = createdAt;
        }

        public Long getId() {
            return mId;
        }

        public void setId(Long id) {
            mId = id;
        }

        public Long getIdGroup() {
            return mIdGroup;
        }

        public void setIdGroup(Long idGroup) {
            mIdGroup = idGroup;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public Long getPreselect() {
            return mPreselect;
        }

        public void setPreselect(Long preselect) {
            mPreselect = preselect;
        }

        public String getPrice() {
            return mPrice;
        }

        public void setPrice(String price) {
            mPrice = price;
        }

        public String getUpdatedAt() {
            return mUpdatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            mUpdatedAt = updatedAt;
        }

    }

}
