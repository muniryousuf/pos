package com.application.ordermanagement.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.application.ordermanagement.BuildConfig;
import com.application.ordermanagement.R;
import com.application.ordermanagement.activity.MainActivity;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.helper.BasePreferenceHelper;
import com.application.ordermanagement.helper.PrinterCommands;
import com.application.ordermanagement.helper.Spanny;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;
    private NotificationChannel mChannel;
    private NotificationManager notifManager;
    BasePreferenceHelper basePreferenceHelper;

    @Override
    public void onNewToken(String s) {
//        super.onNewToken(s);
        BasePreferenceHelper.setDeviceToken(getApplicationContext(), s.trim());
        Log.d("Firebase Token", "Refreshed token: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        basePreferenceHelper = new BasePreferenceHelper(getApplicationContext());
        displayCustomNotificationForOrders(remoteMessage.getData().get("is_pos"), remoteMessage.getData().get("title")
                , remoteMessage.getData().get("body")
                , remoteMessage.getData().get("order_id")
                , remoteMessage.getData().get("total_amount")
                , remoteMessage.getData().get("reference")
                , remoteMessage.getData().get("order_type")
                , remoteMessage.getData().get("payment"));
        Log.i("testing", "onMessageReceived" + remoteMessage);
    }


    private void displayCustomNotificationForOrders(String is_pos, String title, String description, String id,
                                                    String total_amount, String reference, String order_type, String payment) {
        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService
                    (Context.NOTIFICATION_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder builder;
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            if (mChannel == null) {
                mChannel = new NotificationChannel
                        (BuildConfig.APPLICATION_ID, "Order Management System", importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, "0");

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(title)
                    .setSmallIcon(getNotificationIcon()) // required
                    .setContentText(description)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setLargeIcon(BitmapFactory.decodeResource
                            (getResources(), R.mipmap.ic_launcher))
                    .setBadgeIconType(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(RingtoneManager.getDefaultUri
                            (RingtoneManager.TYPE_NOTIFICATION));
            Notification notification = builder.build();
            notifManager.notify(0, notification);
        } else {

            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = null;

            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
            String chaneel_id = getString(R.string.default_notification_channel_id);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, chaneel_id)
                    .setContentTitle(title)
                    .setContentText(description)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
                    .setSound(defaultSoundUri)
                    .setSmallIcon(getNotificationIcon())
                    .setContentIntent(pendingIntent)
                    .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(description));

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1251, notificationBuilder.build());
        }
        //if (basePreferenceHelper.getAutoPrintStatus()) {
        //  callsuccessapi(is_pos,id, total_amount,  reference,  order_type,  payment);
        panelPrinters(id,total_amount,  reference,  order_type,  payment);
        //}

    }

    private void panelPrinters(String id, String total_amount, String reference, String order_type, String order_id) {
        details = new ArrayList<>();

        String url = AppConstant.BASE_URL + "update-order/" + id;


        RequestBody formBody = new FormBody.Builder()
                .add("status", "Accepted")
                .build();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .put(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                if (response.code() == 200) {
                    String responseString = response.body().string();
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(responseString);
                        JSONObject jsonObject = obj.getJSONObject("data");
                        this.id = jsonObject.getString("id");
                        user_id = jsonObject.getString("user_id");
                        payment = jsonObject.getString("payment");
                        delivery_address = jsonObject.getString("delivery_address");
                        phone_number = jsonObject.getString("phone_number");
                        total_amount_with_fee = Float.valueOf(jsonObject.getString("total_amount_with_fee"));
                        discounted_amount = Float.valueOf(jsonObject.getString("discounted_amount"));
                        delivery_fees = Float.valueOf(jsonObject.getString("delivery_fees"));
                        if (jsonObject.getString("order_type") != null) {
                            this.order_type = jsonObject.getString("order_type");

                        }
                        JSONArray jsonArray = jsonObject.getJSONArray("details");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            JSONObject product = jsonArray.getJSONObject(i).getJSONObject("product");
                            String id_category = product.getString("id_category");
                            String product_name = object.getString("product_name");
                            Float price = Float.valueOf(object.getString("price"));
                            String quantity = object.getString("quantity");
                            String extras = object.getString("extras");
                            String special_instruction = object.getString("special_instructions");
                            details.add(new ProductLisitng(id_category, product_name, price, quantity, extras, special_instruction != null ? special_instruction : ""));


                        }
                        for (int i = 0; i < basePreferenceHelper.getPrinters("printers").size(); i++) {
                           if(basePreferenceHelper.getPrinters("printers").get(i).getIs_default()==1 && basePreferenceHelper.getPrinters("printers").get(i).getStatus()==1){
                               printSlip(details,id,delivery_address,delivery_fees,total_amount_with_fee,payment,order_type,basePreferenceHelper.getPrinters("printers").get(i).getIp(),user_id,discounted_amount,phone_number);
                           }

                            ArrayList<ProductLisitng> lisitngs = new ArrayList<>();

                            for (int j = 0; j < details.size(); j++) {
                                for (int k = 0; k < basePreferenceHelper.getPrinters("printers").get(i).getCategories().size(); k++) {
                                    if(basePreferenceHelper.getPrinters("printers").get(i).getStatus()==1){
                                        if (details.get(j).getId_cateogory().equalsIgnoreCase(String.valueOf(basePreferenceHelper.getPrinters("printers").get(i).getCategories().get(k).getCategory().getId()))) {

                                            lisitngs.add(details.get(j));
                                        }
                                    }



                                }

                            }
                            if(lisitngs.size()>0){
                                for(int p =0;p< basePreferenceHelper.getPrinters("printers").get(i).getPrint_count();p++){
                                    printSlipForPanel(lisitngs, this.id, this.order_type, basePreferenceHelper.getPrinters("printers").get(i).getIp());

                                }
                            }
                            lisitngs.clear();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }


            response.body().close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }


    ArrayList<ProductLisitng> details;
    String id, delivery_address, phone_number, payment, order_type, user_id;
    Float total_amount_with_fee, delivery_fees;
    Float discounted_amount;

    private void callsuccessapi(String is_pos, String order_id, String total_amount, String reference, String order_typo, String paymento) {
        // mainActivity.showLoader();

        if (is_pos != null && is_pos.equalsIgnoreCase("true")) {
            details = new ArrayList<>();

            String url = AppConstant.BASE_URL + "update-order/" + order_id;


            RequestBody formBody = new FormBody.Builder()
                    .add("status", "Accepted")
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .put(formBody)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    if (response.code() == 200) {
                        String responseString = response.body().string();
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(responseString);
                            JSONObject jsonObject = obj.getJSONObject("data");
                            id = jsonObject.getString("id");
                            user_id = jsonObject.getString("user_id");
                            payment = jsonObject.getString("payment");
                            delivery_address = jsonObject.getString("delivery_address");
                            phone_number = jsonObject.getString("phone_number");
                            total_amount_with_fee = Float.valueOf(jsonObject.getString("total_amount_with_fee"));
                            discounted_amount = Float.valueOf(jsonObject.getString("discounted_amount"));
                            delivery_fees = Float.valueOf(jsonObject.getString("delivery_fees"));
                            if (jsonObject.getString("order_type") != null) {
                                order_type = jsonObject.getString("order_type");

                            }
                            JSONArray jsonArray = jsonObject.getJSONArray("details");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String product_name = object.getString("product_name");
                                Float price = Float.valueOf(object.getString("price"));
                                String quantity = object.getString("quantity");
                                String extras = object.getString("extras");
                                String special_instruction = object.getString("special_instructions");


                                details.add(new ProductLisitng("", product_name, price, quantity, extras, special_instruction != null ? special_instruction : ""));


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        //   OrderUpdateResponse orderUpdateResponse =(OrderUpdateResponse) response.body();
                        if (basePreferenceHelper.getAutoPrintStatus()) {
                            if (basePreferenceHelper.getIp() != null) {
                                Log.v("Printer Printing", "Printer one");
                                for (int j = 0; j < 2; j++) {
                                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIp(), user_id, discounted_amount, phone_number, 1);
                                }
                            }
                        }


                    }

                }


                response.body().close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            if (isAppOnForeground(getApplicationContext(), BuildConfig.APPLICATION_ID)) {
                Intent intent = new Intent(BuildConfig.APPLICATION_ID);
                intent.putExtra("id", order_id);
                intent.putExtra("total_amount", total_amount);
                intent.putExtra("reference", reference);
                intent.putExtra("order_typo", order_typo);
                intent.putExtra("paymento", paymento);
                sendBroadcast(intent);


            } else {
                details = new ArrayList<>();

                String url = AppConstant.BASE_URL + "update-order/" + order_id;


                RequestBody formBody = new FormBody.Builder()
                        .add("status", "Accepted")
                        .build();

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .put(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            String responseString = response.body().string();
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(responseString);
                                JSONObject jsonObject = obj.getJSONObject("data");
                                id = jsonObject.getString("id");
                                user_id = jsonObject.getString("user_id");
                                payment = jsonObject.getString("payment");
                                delivery_address = jsonObject.getString("delivery_address");
                                phone_number = jsonObject.getString("phone_number");
                                total_amount_with_fee = Float.valueOf(jsonObject.getString("total_amount_with_fee"));
                                discounted_amount = Float.valueOf(jsonObject.getString("discounted_amount"));
                                delivery_fees = Float.valueOf(jsonObject.getString("delivery_fees"));
                                if (jsonObject.getString("order_type") != null) {
                                    order_type = jsonObject.getString("order_type");

                                }
                                JSONArray jsonArray = jsonObject.getJSONArray("details");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String product_name = object.getString("product_name");
                                    Float price = Float.valueOf(object.getString("price"));
                                    String quantity = object.getString("quantity");
                                    String extras = object.getString("extras");
                                    String special_instruction = object.getString("special_instructions");


                                    details.add(new ProductLisitng("", product_name, price, quantity, extras, special_instruction != null ? special_instruction : ""));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            //   OrderUpdateResponse orderUpdateResponse =(OrderUpdateResponse) response.body();
                            if (basePreferenceHelper.getAutoPrintStatus()) {
                                if (basePreferenceHelper.getIp() != null) {
                                    Log.v("Printer Printing", "Printer one");
                                    for (int j = 0; j < 2; j++) {
                                        printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIp(), user_id, discounted_amount, phone_number, 1);
                                    }
                                }
                            }


                        }

                    }


                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public class ProductLisitng {
        public ProductLisitng(String id_cateogory, String productname, Float price, String quantity, String extra, String special_instructions) {
            this.id_cateogory = id_cateogory;
            this.productname = productname;
            this.extra = extra;
            this.price = price;
            this.quantity = quantity;
            this.special_instructions = special_instructions;
        }

        String productname;

        public String getId_cateogory() {
            return id_cateogory;
        }

        public void setId_cateogory(String id_cateogory) {
            this.id_cateogory = id_cateogory;
        }

        String id_cateogory;

        public String getExtra() {
            return extra;
        }

        public void setExtra(String extra) {
            this.extra = extra;
        }

        String extra;

        public String getSpecial_instructions() {
            return special_instructions;
        }

        public void setSpecial_instructions(String special_instructions) {
            this.special_instructions = special_instructions;
        }

        String special_instructions;
        Float price;

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        String quantity;

    }
    public void printSlip(ArrayList<MyFirebaseMessagingService.ProductLisitng> details,
                          String id, String delivery_address,
                          Float delivery_fees, Float total_amount_with_fee,
                          String pay, String order_type, String ip, String user_id, Float discounted_amount, String phone_number) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(basePreferenceHelper.getResturant().getName(), 3, 1);
            printNewLine();
            //printCustom("Pizza and Fish and Chips Specialists", 0, 1);
            printCustom(basePreferenceHelper.getResturant().getAddress().getAddress(), 0, 1);
            printCustom("Phone No : " + basePreferenceHelper.getResturant().getPhoneNumber(), 0, 1);
            printCustom("Post Code : " + basePreferenceHelper.getResturant().getAddress().getZipCode(), 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printCustom(order_type, 2, 1);
            printNewLine();
            if (order_type != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " /*+ dateTime[1]*/, 3, 1);
            }
            printCustom("Order Number : 000" + id/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            if (!order_type.equalsIgnoreCase("Pickup")) {
                printCustom("Restaurant Notes ", 0, 0);
                printCustom("Contact Free Delivery", 3, 0);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            }


            printNewLine();
            for (int j = 0; j < details.size(); j++) {
                printCustom(padLine(details.get(j).getQuantity() + " x " + details.get(j).getProductname(), " £ " + (Float.parseFloat(details.get(j).getQuantity()) * details.get(j).getPrice()), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(details.get(j).getExtra());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (details.get(j).getSpecial_instructions() != null && !details.get(j).getSpecial_instructions().equals("") && !details.get(j).getSpecial_instructions().equals("null")) {

                        spanny.append(" **" + details.get(j).getSpecial_instructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }


            }

            //printCustom(padLine("Food and drink total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);

            printCustom(padLine("Delivery Fee", delivery_fees > 0 ? "£" + delivery_fees + "/=" : "   Free", 45) + "\n", 1, 0);
            if (discounted_amount > 0) {

                printCustom(padLine("Discount", "- £" + discounted_amount + "/=", 45) + "\n", 1, 0);


            }
            printCustom(padLine("Total", "£" + Float.sum(total_amount_with_fee,/* delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);
            //printCustom(padLine("Total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);
            printNewLine();
            printNewLine();
            if (!payment.equalsIgnoreCase("cod")) {
                printCustom("Paid By :", 0, 0);
            }
            printCustom(padLine(payment.equalsIgnoreCase("cod") ? (order_type.equalsIgnoreCase("Pickup") ? "Cash on Collection" : "Cash on Delivery") : "Card : xxxxxxxxxxxxxx", "£" + Float.sum(total_amount_with_fee,/* delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);


            if (!payment.equalsIgnoreCase("cod")) {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER HAS BEEN PAID", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                if (!payment.equalsIgnoreCase("cod")) {
                    printCustom("PAID ONLINE ", 2, 1);

                }
            } else {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom((order_type.equalsIgnoreCase("Pickup") ? "CASH ON COLLECTION" : "CASH ON DELIVERY"), 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER NOT PAID", 2, 1);

            }
            printCustom("Please Check Notes for Instructions ", 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);

            printNewLine();
            printCustom("Customer ID: ", 0, 0);
            printCustom("" + user_id, 2, 0);
            printNewLine();
            printCustom("Customer details: ", 0, 0);
            try {
                String[] splited = delivery_address.split("\\s+");
                if (splited.length < 3) {
                    printCustom(delivery_address, 2, 0);
                    // printNewLine();
                } else if (splited.length < 4) {
                    printCustom(splited[0] + " " + splited[1] + " " + splited[2], 2, 0);
                    // printNewLine();
                } else if (splited.length < 5) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3], 2, 0);

                } else if (splited.length < 6) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    /// printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]), 2, 0);

                } else if (splited.length < 7) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();

                } else if (splited.length < 8) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6], 2, 0);

                } else if (splited.length < 9) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6].concat(" " + splited[7]), 2, 0);

                } else {

                }
            } catch (Exception exception) {
                printCustom(delivery_address, 2, 0);

            }
            printNewLine();
            printCustom("To Contact Customer Call: ", 0, 0);
            printCustom(phone_number, 2, 0);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();



        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }


    public void printSlip(ArrayList<MyFirebaseMessagingService.ProductLisitng> details,
                          String id, String delivery_address,
                          Float delivery_fees, Float total_amount_with_fee,
                          String pay, String order_type, String ip, String user_id, Float discounted_amount, String phone_number, int i) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(basePreferenceHelper.getResturant().getName(), 3, 1);
            printNewLine();
            //printCustom("Pizza and Fish and Chips Specialists", 0, 1);
            printCustom(basePreferenceHelper.getResturant().getAddress().getAddress(), 0, 1);
            printCustom("Phone No : " + basePreferenceHelper.getResturant().getPhoneNumber(), 0, 1);
            printCustom("Post Code : " + basePreferenceHelper.getResturant().getAddress().getZipCode(), 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printCustom(order_type, 2, 1);
            printNewLine();
            if (order_type != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " /*+ dateTime[1]*/, 3, 1);
            }
            printCustom("Order Number : 000" + id/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            if (!order_type.equalsIgnoreCase("Pickup")) {
                printCustom("Restaurant Notes ", 0, 0);
                printCustom("Contact Free Delivery", 3, 0);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            }


            printNewLine();
            for (int j = 0; j < details.size(); j++) {
                printCustom(padLine(details.get(j).getQuantity() + " x " + details.get(j).getProductname(), " £ " + (Float.parseFloat(details.get(j).getQuantity()) * details.get(j).getPrice()), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(details.get(j).getExtra());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (details.get(j).getSpecial_instructions() != null && !details.get(j).getSpecial_instructions().equals("") && !details.get(j).getSpecial_instructions().equals("null")) {

                        spanny.append(" **" + details.get(j).getSpecial_instructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }


            }

            //printCustom(padLine("Food and drink total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);

            printCustom(padLine("Delivery Fee", delivery_fees > 0 ? "£" + delivery_fees + "/=" : "   Free", 45) + "\n", 1, 0);
            if (discounted_amount > 0) {

                printCustom(padLine("Discount", "- £" + discounted_amount + "/=", 45) + "\n", 1, 0);


            }
            printCustom(padLine("Total", "£" + Float.sum(total_amount_with_fee,/* delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);
            //printCustom(padLine("Total", "£" + Float.sum(total_amount_with_fee, delivery_fees>0?delivery_fees:0) + "/=", 45) + "\n", 1, 0);
            printNewLine();
            printNewLine();
            if (!payment.equalsIgnoreCase("cod")) {
                printCustom("Paid By :", 0, 0);
            }
            printCustom(padLine(payment.equalsIgnoreCase("cod") ? (order_type.equalsIgnoreCase("Pickup") ? "Cash on Collection" : "Cash on Delivery") : "Card : xxxxxxxxxxxxxx", "£" + Float.sum(total_amount_with_fee,/* delivery_fees>0?delivery_fees:*/0) + "/=", 45) + "\n", 1, 0);


            if (!payment.equalsIgnoreCase("cod")) {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER HAS BEEN PAID", 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                if (!payment.equalsIgnoreCase("cod")) {
                    printCustom("PAID ONLINE ", 2, 1);

                }
            } else {
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom((order_type.equalsIgnoreCase("Pickup") ? "CASH ON COLLECTION" : "CASH ON DELIVERY"), 2, 1);
                printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
                printCustom("ORDER NOT PAID", 2, 1);

            }
            printCustom("Please Check Notes for Instructions ", 0, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);

            printNewLine();
            printCustom("Customer ID: ", 0, 0);
            printCustom("" + user_id, 2, 0);
            printNewLine();
            printCustom("Customer details: ", 0, 0);
            try {
                String[] splited = delivery_address.split("\\s+");
                if (splited.length < 3) {
                    printCustom(delivery_address, 2, 0);
                    // printNewLine();
                } else if (splited.length < 4) {
                    printCustom(splited[0] + " " + splited[1] + " " + splited[2], 2, 0);
                    // printNewLine();
                } else if (splited.length < 5) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3], 2, 0);

                } else if (splited.length < 6) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    /// printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]), 2, 0);

                } else if (splited.length < 7) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();

                } else if (splited.length < 8) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6], 2, 0);

                } else if (splited.length < 9) {
                    printCustom("" + splited[0].concat(" " + splited[1]).concat(" " + splited[2]), 2, 0);
                    //  printNewLine();
                    printCustom("" + splited[3].concat(" " + splited[4]).concat(" " + splited[5]), 2, 0);
                    // printNewLine();
                    printCustom("" + splited[6].concat(" " + splited[7]), 2, 0);

                } else {

                }
            } catch (Exception exception) {
                printCustom(delivery_address, 2, 0);

            }
            printNewLine();
            printCustom("To Contact Customer Call: ", 0, 0);
            printCustom(phone_number, 2, 0);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();

            if (i == 1) {
                if (basePreferenceHelper.getIptwo() != null) {
                    Log.v("Printer Printing", "Printer two");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIptwo(), user_id, discounted_amount, phone_number, 2);
                } else {
                    return;
                }


            }
            if (i == 2) {
                if (basePreferenceHelper.getIpthree() != null) {
                    Log.v("Printer Printing", "Printer three");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIpthree(), user_id, discounted_amount, phone_number, 3);
                } else {
                    return;
                }


            }

            if (i == 3) {
                if (basePreferenceHelper.getIpfour() != null) {
                    Log.v("Printer Printing", "Printer four");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIpfour(), user_id, discounted_amount, phone_number, 4);
                } else {
                    return;
                }


            }
            if (i == 4) {
                if (basePreferenceHelper.getIpfive() != null) {
                    Log.v("Printer Printing", "Printer five");

                    printSlip(details, id, delivery_address, delivery_fees, total_amount_with_fee, payment, order_type, basePreferenceHelper.getIpfive(), user_id, discounted_amount, phone_number, -1);
                } else {
                    return;
                }


            }


//            if(i==-1){
//                return;
//            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }


    public void printSlipForPanel(ArrayList<MyFirebaseMessagingService.ProductLisitng> details,
                                  String id, String order_type, String ip) {
        try {
            Socket sock = new Socket(ip, 9100);
            String dateTime[] = getDateTime();
            outputStream = sock.getOutputStream();
            printNewLine();
            printCustom(basePreferenceHelper.getResturant().getName(), 3, 1);
            printNewLine();
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            // printCustom(order_type, 2, 1);
            printNewLine();
            if (order_type != null) {
                printCustom("Due " + dateTime[0] + " AT ASAP " /*+ dateTime[1]*/, 3, 1);
            }
            printCustom("Order Number : 000" + id/*datum.getDetails().get(0).getOrderId()*/, 3, 1);
            printCustom(new String(new char[60]).replace("\0", "."), 0, 1);
            printNewLine();
            for (int j = 0; j < details.size(); j++) {
                printCustom(padLine(details.get(j).getQuantity() + " x " + details.get(j).getProductname(), " £ " + (Float.parseFloat(details.get(j).getQuantity()) * details.get(j).getPrice()), 45) + "", 1, 0);

                String choice = "";
                String group_name = "";
                String price = "";
                try {
                    JSONArray jsonArray = new JSONArray(details.get(j).getExtra());

                    Spanny spanny = new Spanny();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject currentObject = jsonArray.getJSONObject(k);

                        if (currentObject.getString("group_name") == null) {

                        } else {
                            group_name = currentObject.getString("group_name");
                        }
                        if (currentObject.getString("choice") == null) {

                        } else {
                            choice = currentObject.getString("choice");
                        }


                        if (currentObject.getString("price") == null) {

                        } else {
                            if (currentObject.getString("price").equals("0.00") || currentObject.getString("price").equals("0")) {
                                price = "";
                            } else {
                                price = currentObject.getString("price");
                            }

                        }

                        spanny.append(!choice.equals("") ? " + " + choice : "").append(price.equals("") ? "" + " " : " £ :" + price).append("\n")/*.append(!choice.equals("")?"   "+choice:"").append("\n ")*/;


                    }
                    if (details.get(j).getSpecial_instructions() != null && !details.get(j).getSpecial_instructions().equals("") && !details.get(j).getSpecial_instructions().equals("null")) {

                        spanny.append(" **" + details.get(j).getSpecial_instructions() + "**");
                    }
                    printCustom("  " + spanny.toString(), 1, 0);
                } catch (Exception e) {

                }

            }


            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            outputStream.flush();
            sock.close();


        } catch (UnknownHostException e) {
            e.printStackTrace();
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            // Toast.makeText(mainActivity, ""+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    protected String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine) {
        if (partOne == null) {
            partOne = "";
        }
        if (partTwo == null) {
            partTwo = "";
        }
        String concat;
        if ((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + " " + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(" ", padding) + partTwo;
        }
        return concat;
    }

    /**
     * utility: string repeat
     */
    protected String repeat(String str, int i) {
        return new String(new char[i]).replace("\0", str);
    }


    private static OutputStream outputStream;

    private void printCustom(String msg, int size, int align) throws IOException {
        //Print config "mode"
        //    byte[] arrayOfByte1 = { 27, 33, 0 };

        // outputStream.write(arrayOfByte1);

        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes("Cp858"));
            //outputStream.write(msg.getBytes(),0,msg.getBytes().length);

            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print photo

    //print unicode


    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //print text

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime[] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }


    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }

}

