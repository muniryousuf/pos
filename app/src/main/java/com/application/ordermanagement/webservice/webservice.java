package com.application.ordermanagement.webservice;


import com.application.ordermanagement.fragments.AddNewMenuFragment;
import com.application.ordermanagement.fragments.TabelReservationFragment;
import com.application.ordermanagement.helper.AppConstant;
import com.application.ordermanagement.model.AdAdonProduct.AdOnAddProductResponse;
import com.application.ordermanagement.model.AddProducttoCategory.AddProductToCategory;
import com.application.ordermanagement.model.DealsResponse;
import com.application.ordermanagement.model.GetChoice.GetChoiceResponse;
import com.application.ordermanagement.model.OrderPlace.OrderPlaceModel;
import com.application.ordermanagement.model.PlaceOrderResponse.PlaceOrderDataResponse;
import com.application.ordermanagement.model.Printers.PrinterResponseList;
import com.application.ordermanagement.model.Printers.PrintersResponse;
import com.application.ordermanagement.model.ResponseForSeperateModels.SingleProductResponse;
import com.application.ordermanagement.model.ResturantInfo.ResturantInfoResponse;
import com.application.ordermanagement.model.ResturantInfoBodyRequest;
import com.application.ordermanagement.model.TableResponseModel.TablesResponse;
import com.application.ordermanagement.model.addcategoryresponse.AddCategoryResponse;
import com.application.ordermanagement.model.deletecategoryresponse.DeleteCategoryResponse;
import com.application.ordermanagement.model.updatedealprice.UpdateDealResponse;
import com.application.ordermanagement.models.AddOns.AddOnsResponse;
import com.application.ordermanagement.models.CategoryResponse;
import com.application.ordermanagement.models.MenuModule.MenuResponse;
import com.application.ordermanagement.models.OrderUpdateResponse;
import com.application.ordermanagement.models.OrdersResponse;
import com.application.ordermanagement.models.ProductDataResponse;
import com.application.ordermanagement.models.ProductDataaResponseUpdate;
import com.application.ordermanagement.models.Resturant.ResturantResponse;
import com.application.ordermanagement.models.SalesResponse.SalesResponseClass;
import com.application.ordermanagement.models.SignInResponse;
import com.application.ordermanagement.models.shopstatus.ShopStatusResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by khanubaid on 12/28/2017.
 */

public interface webservice {


//    //Get Category
//    @GET(AppConstant.ALL_CATEGORYS)
//    Call<Api_Response<JsonObject>> getAllCategory();
//
//    //Sub Category
//    @GET(AppConstant.SUB_CATEGORYS)
//    Call<Api_Array_Response<JsonObject>> getSubCategory(@Query("category_id") int category_id);
//
//    @GET(AppConstant.SUB_CATEGORYS)
//    Call<Api_Response<ArrayList<AllCategory>>> getAllSubCategory(@Query("category_id") int category_id);
//
//    @GET(AppConstant.GET_BRANDS)
//    Call<Api_Response<ArrayList<Brand>>> GetAllBrands(@Query("category_id") int category_id);
//
//    @GET(AppConstant.GET_MAKE_MODELS)
//    Call<Api_Response<ArrayList<MakeModel>>> GetAllMakeModels(@Query("brand_id") int brand_id);
//
//    @GET(AppConstant.GET_PRODUCTS)
//    Call<Api_Response<ArrayList<ProductModelAPI>>> getAllProducts(@Query("country_id") String country_id, @Query("city_id") Integer city_id, @Query("sub_category_id") Integer sub_category_id, @Query("category_id") Integer category_id, @Query("brand_id") Integer brand_id, @Query("user_id") Integer user_id);
//
//    @FormUrlEncoded
////    @POST(AppConstant.ADD_PRODUCTS)
//    Call<Api_Response<JsonObject>> addProducts(@Field("body") String body, @Field("images") File images);
//
//    @Multipart
//    @POST(AppConstant.ADD_PRODUCTS)
//    Call<Api_Response> addProducts2(
//            @PartMap Map<String, RequestBody> CampaignBody,
//            @Part ArrayList<MultipartBody.Part> images
//    );
//
//    @Multipart
//    @POST(AppConstant.EDIT_PRODUCT)
//    Call<Api_Response> editProduct(
//            @PartMap Map<String, RequestBody> CampaignBody,
//            @Part ArrayList<MultipartBody.Part> images
//    );
//
//    @Multipart
//    @POST(AppConstant.USER_SIGNUP)
//    Call<Api_Response<UserModel>> registerUser(@PartMap Map<String, RequestBody> body,
//                                               @Part MultipartBody.Part image);
//
//    @FormUrlEncoded
//    @POST(AppConstant.USER_LOGIN)
//    Call<Api_Response<UserModel>> loginUser(@Field("email") String email,
//                                            @Field("password") String password,
//                                            @Field("role_id") String role_id,
//                                            @Field("device_type") String device_type,
//                                            @Field("device_id") String device_id);
//
//    @Multipart
//    @POST(AppConstant.USER_VERIFY_CODE)
//    Call<Api_Response<UserModel>> verifyUserCode(@PartMap Map<String, RequestBody> body);
//
//    @Multipart
//    @POST(AppConstant.USER_UPDATE)
//    Call<Api_Response<UserModel>> updateUser(@PartMap Map<String, RequestBody> body,
//                                             @Part MultipartBody.Part image);
//
//    @GET(AppConstant.GET_MY_PRODUCTS)
//  Call<Api_Response<ArrayList<ProductModelAPI>>> getUserProducts(@Query("user_id") Integer user_id);
//
//
//    @POST(AppConstant.USER_LOGOUT)
//    Call<Api_Response> logout(@Query("user_id") int user_id, @Query("device_id") String device_id);
//
//    @POST(AppConstant.FORGOT_PASSWORD)
//    Call<Api_Response<StringWarpper>> forgotPasswordEmail(@Query("email") String email);
//
//    @GET(AppConstant.GET_COUNTRIES)
//    Call<Api_Response<ArrayList<Country>>> getCountires();
//
//    @GET(AppConstant.GET_CITIES)
//    Call<Api_Response<ArrayList<City>>> getCities(@Query("country_id") String country_id);
//
//    @POST(AppConstant.DELETE_PRODUCT)
//    Call<Api_Response> deleteProduct(@Query("product_id") int product_id, @Query("user_id") int user_id);
//
//    @POST(AppConstant.RESEND_CODE)
//    Call<Api_Response> resendCode(@Query("email") String email);
//
//    @POST(AppConstant.UPDATE_PASSWORD)
//    Call<Api_Response> forgotChangePassword(@Query("email") String email,
//                                            @Query("reset_code") String reset_code,
//                                            @Query("password") String password);
//
//    @POST(AppConstant.USER_CHANGE_PASSWORD)
//    Call<Api_Response> changepassword(@Query("user_id") int user_id,
//                                      @Query("new_password") String new_password,
//                                      @Query("old_password") String old_password);
//
//    @POST(AppConstant.ADD_TO_FAV)
//    Call<Api_Response> addtoFavorite(@Query("product_id") String product_id, @Query("user_id") String user_id);
//
//    @POST(AppConstant.MARKVIEWED)
//    Call<Api_Response> MarkViewed(@Query("product_id") String product_id, @Query("user_id") String user_id, @Query("device_id") String device_id);
//
//    //   Call<Api_Response> MarkViewed(@Query("product_id") String product_id, @Query("user_id") String user_id);
//
//    @GET(AppConstant.GET_CART)
//    Call<Api_Response<ArrayList<CartProductMainClass>>> getCart(@Query("user_id") String user_id);
//
//    @POST(AppConstant.ADD_TO_CART)
//    Call<Api_Response> addProductToCart(@Query("product_id") String product_id, @Query("user_id") String user_id, @Query("device_id") String device_id, @Query("rent_type_id") String rent_type_id);
//
//
//    @POST(AppConstant.REMOVE_FROM_CART)
//    Call<Api_Response> removeProductfromCart(@Query("product_id") String product_id, @Query("user_id") String user_id, @Query("device_id") String device_id);
//
//    @POST(AppConstant.CART_CHECKOUT)
//    Call<Api_Response> checkout(@Query("product_id") String product_id,
//                                @Query("user_id") String user_id,
//                                @Query("quantity") String quantity,
//                                @Query("unit_price") String unit_price,
//                                @Query("rent_type_id") String rent_type_id,
//                                @Query("payment_method_id") String payment_method_id);
//
//    @GET(AppConstant.GET_ORDERS)
//    Call<Api_Response<ArrayList<Order>>> GetOrders(@Query("user_id") String user_id);
//
//    @POST(AppConstant.RATE_ORDER)
//    Call<Api_Response> RateOrder(
//            @Query("product_id") String product_id,
//            @Query("product_rating") String product_rating,
//            @Query("device_id") String device_id,
//            @Query("user_id") String user_id,
//            @Query("product_user_id") String product_user_id,
//            @Query("product_user_rating") String product_user_rating,
//            @Query("order_id") String order_id
//    );
//
//
//    @GET(AppConstant.GET_NOTIFICATIONS)
//    Call<Api_Response<ArrayList<Notification>>> GetNotifications(@Query("user_id") String user_id);
//
//
//    @POST(AppConstant.MARK_ORDER)
//    Call<Api_Response> acceptOrder(@Query("order_id") String order_id, @Query("status") int status, @Query("pickup_date") String pickup_date);

    @GET(AppConstant.ALL_PRODUCTS)
    Call<ProductDataResponse> getAllproducts();

    @GET(AppConstant.ALL_CATEGORYS)
    Call<CategoryResponse> getAllCategory();

    @GET(AppConstant.GET_ALL_ORDERS)
    Call<OrdersResponse> getAllOrders(@Query("from") String from, @Query("to") String to);

    @GET(AppConstant.GET_RESTURANT)
    Call<ResturantResponse> getResturant();

    @FormUrlEncoded
    @PUT(AppConstant.UPDATE_ORDERS + "/{id}")
    Call<Api_Response<OrderUpdateResponse>> updateOrder(@Path("id") int id,
                                                        @Field("status") String status);

    @FormUrlEncoded
    @PUT("products/{id}")
    Call<Api_Response<ProductDataaResponseUpdate>> updateproduct(@Path("id") int id,

                                                                 @Field("price") String price);

    @DELETE("products/{id}")
    Call<DeleteCategoryResponse> deleteProduct(@Path("id") Long id);


    @FormUrlEncoded
    @PUT("products/{id}")
    Call<Api_Response<ProductDataaResponseUpdate>> updateproduct(@Path("id") int id,
                                                                 @Field("size") Map<String, Object> size,
                                                                 @Field("price") Integer price);

    @FormUrlEncoded
    @PUT("products/{id}")
    Call<Api_Response<ProductDataaResponseUpdate>> updateproductAvailibity(@Path("id") int id,
                                                                           @Field("price") String price,
                                                                           @Field("status") int status);

    @FormUrlEncoded
    @PUT("products/{id}")
    Call<Api_Response<ProductDataaResponseUpdate>> updateproductAvailibity(@Path("id") int id,
                                                                           @Field("status") int status);

    @FormUrlEncoded
    @POST(AppConstant.USER_LOGIN)
    Call<SignInResponse> loginUser(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("device_type") String device_type,
                                   @Field("device_token") String device_id);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateShopstatus(@Field("shop_status") int shop_status);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateIp(@Field("printer_ip_1") String printer_ip_1);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateIptwo(@Field("printer_ip_2") String printer_ip_2);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateIpthree(@Field("printer_ip_3") String printer_ip_3);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateIpfour(@Field("printer_ip_4") String printer_ip_4);

    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> updateIpfive(@Field("printer_ip_5") String printer_ip_5);


    @FormUrlEncoded
    @POST(AppConstant.GET_TOTAL_SALES)
    Call<SalesResponseClass> getTotalSales(@Field("start_date") String start_date, @Field("end_date") String end_date);

    @GET(AppConstant.GET_ALL_MENUS)
    Call<MenuResponse> getMenu();

    @FormUrlEncoded
    @POST(AppConstant.GET_ALL_MENUS)
    Call<AddCategoryResponse> AddMenu(@Field("name") String name, @Field("description") String description);

    @FormUrlEncoded
    @PUT("categories/{id}")
    Call<AddCategoryResponse> UpdateMenu(@Path("id") Long id, @Field("name") String name, @Field("description") String description);


    @FormUrlEncoded
    @PUT("choices-group/{id}")
    Call<AddCategoryResponse> UpdateGroup(@Path("id") Long id, @Field("name") String name, @Field("type") String type, @Field("display_type") String display_type);


    @FormUrlEncoded
    @POST(AppConstant.ADD_GROUP)
    Call<AddCategoryResponse> AddGroup(@Field("name") String name, @Field("type") String type, @Field("display_type") String display_type);

    @FormUrlEncoded
    @POST("choices")
    Call<AddCategoryResponse> AddChoiceToGroup(@Field("name") String name, @Field("price") String price, @Field("preselect") int preselect, @Field("id_group") int id_group);


    @FormUrlEncoded
    @PUT("choices/{id}")
    Call<AddCategoryResponse> UpdateChoiceToGroup(@Path("id") Long id, @Field("name") String name, @Field("price") String price, @Field("preselect") int preselect, @Field("id_group") int id_group);

    @GET(AppConstant.ALL_DEALS)
    Call<DealsResponse> getAllDeals();

    @FormUrlEncoded
    @POST(AppConstant.ALL_DEALS)
    Call<DealsResponse> AddDeal();

    @POST("products")
    Call<AddCategoryResponse> AddProduct(@Body AddProductToCategory addProductToCategory);


    @POST("remove-group-to-product")
    Call<DeleteCategoryResponse> RemoveAdon(@Body AddNewMenuFragment.RemoveAdonRequest addProductToCategory);

    @PUT("products/{id}")
    Call<AddCategoryResponse> UpdateProduct(@Path("id") Long id, @Body AddProductToCategory addProductToCategory);


    @FormUrlEncoded
    @PUT("deals/{id}")
    Call<Api_Response<UpdateDealResponse>> updatedealproduct(@Path("id") Long id,
                                                             @Field("price") String price);

    @GET(AppConstant.GET_ADONS)
    Call<AddOnsResponse> getAdons();

    @DELETE("choices-group/{id}")
    Call<DeleteCategoryResponse> deleteGroup(@Path("id") Long id);


    @DELETE("choices/{id}")
    Call<DeleteCategoryResponse> deleteChoice(@Path("id") Long id);

    @GET("choices")
    Call<GetChoiceResponse> getChoice(@Query("id_group") Long id_group);


    @DELETE("categories/{id}")
    Call<DeleteCategoryResponse> deleteCategory(@Path("id") Long id);


    @POST("add-group-to-product")
    Call<AdOnAddProductResponse> addAdonToProduct(@Query("id_group") String id_group, @Query("id_product") String id_product);


    @GET("products/{id}")
    Call<MenuResponse.Datum> getProductData(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @POST("placeOrder")
    Call<PlaceOrderDataResponse> placeOrderResponse(@Body OrderPlaceModel body);


    @FormUrlEncoded
    @POST("update-general-setting")
    Call<ShopStatusResponse> timings(@Field("opening_time") String opening_time,@Field("closing_time") String closing_time);


    @POST("update-restaurant-info")
    Call<ResturantResponse> updateResturantInfo(@Body ResturantInfoBodyRequest body);

    @GET(AppConstant.GET_TABLES)
    Call<TablesResponse> getTables();

    @POST(AppConstant.GET_TABLES)
    Call<Api_Response> addTable(@Body TabelReservationFragment.Request s);

    @PUT("tableReservation/{id}")
    Call<Api_Response> changeStatusofTable(@Body TabelReservationFragment.Request s, @Path("id") int id);

    @GET(AppConstant.GET_PRINTERS)
    Call<PrinterResponseList> getPrinters();


    @POST(AppConstant.GET_PRINTERS)
    Call<Api_Response> addprinter(@Body PrintersResponse.Datum datum);

    @PUT("printers/{id}")
    Call<Api_Response> addprinterput(@Path("id") int id,@Body PrintersResponse.Datum datum);

    @DELETE("printers/{id}")
    Call<Api_Response> addprinterdelete(@Path("id") int id);
}




